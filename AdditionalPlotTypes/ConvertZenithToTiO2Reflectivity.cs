// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//


using Hiperscan.SGS.Benchtop;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class ConvertZenithToTiO2Reflectivity : Hiperscan.Extensions.PlotTypeInfo
    {
        public ConvertZenithToTiO2Reflectivity() : base(Catalog.GetString("Convert Zenith to TiO2 (Reflectivity)"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Reflectivity");
            this.LowerLimit = 0.0;
            this.UpperLimit = 1.5;
            this.StandardMin = 0.5;
            this.StandardMax = 1.1;
            this.NeedAbsorbance = true;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (spectrum.Reference == null)
                return null;

            DataSet ds = x_constraints.Apply(spectrum.Reference.Intensity);
            return spectrum.Intensity / Finder.ConvertWhiteReference(WhiteReferenceConversionMode.ZenithToTitaniumdioxide, ds);
        }
    }
}