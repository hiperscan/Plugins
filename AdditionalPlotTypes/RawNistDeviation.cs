// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//

using System;

using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class RawNistDeviation : Hiperscan.Extensions.PlotTypeInfo
    {
        private readonly double EPSILON = 1e-20;

        public RawNistDeviation() : base(Catalog.GetString("NIST Deviation (uncorrected)"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("NIST Deviation / nm");
            this.LowerLimit = -10.0;
            this.UpperLimit = 10.0;
            this.StandardMin = -2.0;
            this.StandardMax = 2.0;
            this.NeedAbsorbance = true;
        }

        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (spectrum.HasAbsorbance == false)
                return null;

            spectrum = spectrum.Clone();

            spectrum.WavelengthCorrection = null;
            spectrum.ApplyCorrectionsToRawData(CorrectionMask.All);
            WavelengthCorrection wlc;

            if (Math.Abs(spectrum.SpectralResolution - 15.0) < EPSILON)
                wlc = WavelengthCorrection.CreateFromExtremumDeviation(spectrum, WavelengthCorrection.ReferenceType.NIST15nm, false);
            else
                wlc = WavelengthCorrection.CreateFromExtremumDeviation(spectrum, WavelengthCorrection.ReferenceType.NIST10nm, true);

            DataSet ds = new DataSet(wlc.Wavelengths, wlc.Corrections)
            {
                IsContinuous = false
            };

            return ds;
        }

    }
}