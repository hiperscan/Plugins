// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//

using System.Collections.Generic;

using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class MeanNistDeviation : Hiperscan.Extensions.PlotTypeInfo
    {
        public MeanNistDeviation() : base(Catalog.GetString("Mean NIST Deviation"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Mean NIST Deviation / nm");
            this.LowerLimit = -10.0;
            this.UpperLimit = 10.0;
            this.StandardMin = -2.0;
            this.StandardMax = 2.0;
            this.NeedAbsorbance = true;
        }

        private static Dictionary<int,DataSet> cache = new Dictionary<int,DataSet>();

        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (spectrum.HasAbsorbance == false)
                return null;

            int hash = Plugin.host.CurrentSpectra.GetHashCode();

            if (MeanNistDeviation.cache.ContainsKey(hash))
                return MeanNistDeviation.cache[hash];

            DataSet ds = null;

            foreach (Spectrum s in Plugin.host.CurrentSpectra.Values)
            {
                Spectrum _spectrum = s.Clone();
                _spectrum.ApplyCorrectionsToRawData(CorrectionMask.All);
                WavelengthCorrection wlc;

                if (_spectrum.LimitBandwidth || _spectrum.SpectralResolution.Equals(15.0))
                    wlc = WavelengthCorrection.CreateFromExtremumDeviation(_spectrum, WavelengthCorrection.ReferenceType.NIST15nm, false);
                else
                    wlc = WavelengthCorrection.CreateFromExtremumDeviation(_spectrum, WavelengthCorrection.ReferenceType.NIST10nm, true);

                if (ds == null)
                    ds = new DataSet(wlc.Wavelengths, wlc.Corrections);
                else
                    ds += new DataSet(wlc.Wavelengths, wlc.Corrections);
            }

            ds /= (double)Plugin.host.CurrentSpectra.Count;

            ds.IsContinuous = false;

            MeanNistDeviation.cache.Add(hash, ds);

            return ds;
        }
    }
}