// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//


using Hiperscan.SGS.Benchtop;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class ConvertZenithToTiO2Intensity : Hiperscan.Extensions.PlotTypeInfo
    {
        public ConvertZenithToTiO2Intensity() : base(Catalog.GetString("Convert Zenith to TiO2 (Intensity)"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Intensity / a.u.");
            this.LowerLimit = -5.0;
            this.UpperLimit = 8.0;
            this.StandardMin = -0.5;
            this.StandardMax = 4.1;
            this.NeedAbsorbance = false;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            DataSet ds = x_constraints.Apply(spectrum.Intensity);
            return Finder.ConvertWhiteReference(WhiteReferenceConversionMode.ZenithToTitaniumdioxide, ds);
        }
    }
}