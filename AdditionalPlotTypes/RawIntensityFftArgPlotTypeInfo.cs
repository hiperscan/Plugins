// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//

using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class RawIntensityFFTArg : Hiperscan.Extensions.PlotTypeInfo
    {
        public RawIntensityFFTArg() : base(Catalog.GetString("FFT(RawIntensity) Arg"))
        {
            this.XLabel = Catalog.GetString("Spatial frequency / 1/nm");
            this.YLabel = Catalog.GetString("Arg(FFT(RawIntensity))");
            this.LowerLimit  = -4.0;
            this.UpperLimit  =  4.0;
            this.StandardMin = -4.0;
            this.StandardMax =  4.0;
            this.NeedAbsorbance = false;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            DataSet ds = spectrum.RawIntensity.FftShift().Phase;
            return ds;
        }
    }
}