// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//

using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class RawIntensity : Hiperscan.Extensions.PlotTypeInfo
    {
        public RawIntensity() : base(Catalog.GetString("RawIntensity"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("RawIntensity / a.u.");
            this.LowerLimit = -2.0;
            this.UpperLimit = 5.0;
            this.StandardMin = -0.1;
            this.StandardMax = 2.6;
            this.NeedAbsorbance = false;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            DataSet ds = new DataSet(spectrum.RawIntensity);
            return ds;
        }
    }
}