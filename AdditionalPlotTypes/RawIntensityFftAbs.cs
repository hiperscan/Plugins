// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//

using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class RawIntensityFftAbs : Hiperscan.Extensions.PlotTypeInfo
    {
        public RawIntensityFftAbs() : base(Catalog.GetString("FFT(RawIntensity) Abs"))
        {
            this.XLabel = Catalog.GetString("Spatial frequency / 1/nm");
            this.YLabel = Catalog.GetString("Abs(FFT(RawIntensity))");
            this.LowerLimit = -10.0;
            this.UpperLimit = 10.0;
            this.StandardMin = -5.0;
            this.StandardMax = 5.0;
            this.NeedAbsorbance = false;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            return spectrum.RawIntensity.FftShift().Amplitude;
        }
    }
}