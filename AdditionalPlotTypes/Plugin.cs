
using System;
using System.Reflection;

using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class Plugin : IPlugin
    {
        public static readonly string name        = Catalog.GetString("Additional Plots");
        public static readonly string version     = "1.0.0";
        public static readonly string vendor      = "Hiperscan GmbH";
        public static readonly string description = Catalog.GetString("Provides additional plot types");
        public static readonly Guid guid          = new Guid("cc68a870-9018-11df-a4ee-0800200c9a66");
        
        internal static IHost host;


        public Plugin()
        {
            this.PlotTypeInfos = new PlotTypeInfo[] 
            { 
                new Correction(),
                new RawIntensityFftAbs(),
                new RawIntensityFFTArg(),
                new LinearRegression(),
                new LinearRegressionCorrection(),
                new LinearRegressionCorrectionSnv(),
                new RawIntensity(),
                new ProbeError(),
                new ProbeErrorCorrection(),
                new ConvertZenithToTiO2Intensity(),
                new ConvertZenithToTiO2Reference(),
                new ConvertZenithToTiO2Reflectivity(),
                new ZenithApoIdent2FitQuadratic(),
                new ZenithApoIdent2FitLinear(),
                new NistDeviation(),
                new RawNistDeviation(),
                new MeanNistDeviation()
            };
        }
        
        public void ReplaceGui()
        {
        }

        public void Initialize(IHost host)
        {
            Plugin.host = host;
        }

        public PlotTypeInfo[] PlotTypeInfos { get; }

        public bool Active { get; set; } = true;

        public string Name                  => Plugin.name;
        public string Version               => Plugin.version;
        public string Vendor                => Plugin.vendor;
        public Assembly Assembly            => Assembly.GetExecutingAssembly();
        public string Description           => Plugin.description;
        public Guid Guid                    => Plugin.guid;
        public bool CanReplaceGui           => false;
        public MenuInfo[] MenuInfos         => null;
        public ToolbarInfo[] ToolbarInfos   => null;
        public FileTypeInfo[] FileTypeInfos => null;
    }
}