// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//

using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class Correction : Hiperscan.Extensions.PlotTypeInfo
    {
        public Correction() : base(Catalog.GetString("Wavelength correction"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Wavelength correction / nm");
            this.LowerLimit = -10.0;
            this.UpperLimit = 10.0;
            this.StandardMin = -5.0;
            this.StandardMax = 5.0;
            this.NeedAbsorbance = false;
        }
        
        public override DataSet ComputeDataSet (Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (spectrum.WavelengthCorrection == null)
                return null;
            DataSet ds = spectrum.WavelengthCorrection.FitFunction(1000.0, 1900.0, 10.0);
            return ds;
        }
    }
}