// AdditionalPlotTypes.cs created with MonoDevelop
// User: klose at 13:19 15.07.2010
// CVS release: $Id: AdditionalPlotTypes.cs,v 1.5 2011-04-19 13:13:02 klose Exp $
//

using Hiperscan.SGS.Benchtop.FinderSetup;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalPlotTypes
{

    public class ProbeError : Hiperscan.Extensions.PlotTypeInfo
    {
        public ProbeError() : base(Catalog.GetString("Probe Error"))
        {
            this.XLabel = Catalog.GetString("Wavelength / nm");
            this.YLabel = Catalog.GetString("Probe Error");
            this.LowerLimit = -5.0;
            this.UpperLimit = 8.0;
            this.StandardMin = -0.5;
            this.StandardMax = 4.1;
            this.NeedAbsorbance = true;
        }
        
        public override DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            if (spectrum.HasAbsorbance == false)
                return null;

            DataSet ds = x_constraints.Apply(spectrum.Absorbance);
            return ProbeErrorCharacteristic.GetCorrection(ds);
        }
    }
}