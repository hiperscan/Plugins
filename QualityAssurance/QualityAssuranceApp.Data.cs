using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Threading;
using System.Reflection;

using Mono.Unix;
using Gtk;

namespace Hiperscan.Extensions.QualityAssurance
{
    public enum DirectoryErrors
    {
        None,
        Idendity,
        NoActive,
        AppicationDirectoryNotExist,
        SolidSpectraDirectoryNotExist,
        FluidSpectraDirectoryNotExist,
        AllSpectraDirectoryNotExist,
    }
    
    public enum WatcherTypes
    {
        All,
        Fluid, 
        Solid,
    }

    
    public partial class QualityAssuranceApp : IDisposable
    {
        private static string DefaultPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        
        internal void InitializeConfigurations()
        {
            if (Settings.Current.Directories.ApplicationDirectory == String.Empty)
                Settings.Current.Directories.ApplicationDirectory = QualityAssuranceApp.DefaultPath;
            
            if (Settings.Current.Directories.SolidSpectraDirectory == String.Empty)
                Settings.Current.Directories.SolidSpectraDirectory = QualityAssuranceApp.DefaultPath;
                                    
            if (Settings.Current.Directories.FluidSpectraDirectory == String.Empty)
                Settings.Current.Directories.FluidSpectraDirectory = QualityAssuranceApp.DefaultPath;
            
            if (Settings.Current.Directories.AllSpectraDirectory == String.Empty)
                Settings.Current.Directories.AllSpectraDirectory = QualityAssuranceApp.DefaultPath;
        }
        
        public DirectoryErrors CheckConfigutations()
        {
            if (Directory.Exists(Settings.Current.Directories.ApplicationDirectory) == false)
                return DirectoryErrors.AppicationDirectoryNotExist;
            
            if (Settings.Current.Directories.ConfigurationKind == ConfigurationKind.Simple)
            {
                if (Directory.Exists(Settings.Current.Directories.AllSpectraDirectory) == false)
                    return DirectoryErrors.AllSpectraDirectoryNotExist;
            }
            else
            {
                if (Settings.Current.Directories.SolidSpectraDirectoryActivated == true && 
                    Settings.Current.Directories.FluidSpectraDirectoryActivated == true)
                {
                    if (    Settings.Current.Directories.SolidSpectraDirectory.Contains(Settings.Current.Directories.FluidSpectraDirectory)
                        ||     Settings.Current.Directories.FluidSpectraDirectory.Contains(Settings.Current.Directories.SolidSpectraDirectory))
                    {
                        return DirectoryErrors.Idendity;
                    }
                }
                
                if (Settings.Current.Directories.SolidSpectraDirectoryActivated == false && 
                    Settings.Current.Directories.FluidSpectraDirectoryActivated == false)
                {
                    return DirectoryErrors.NoActive;
                }
            
                if (Directory.Exists(Settings.Current.Directories.SolidSpectraDirectory) == false && 
                    Settings.Current.Directories.SolidSpectraDirectoryActivated == true)
                    return DirectoryErrors.SolidSpectraDirectoryNotExist;
            
                if (Directory.Exists(Settings.Current.Directories.FluidSpectraDirectory) == false && 
                    Settings.Current.Directories.FluidSpectraDirectoryActivated == true)
                    return DirectoryErrors.FluidSpectraDirectoryNotExist;
            }
            return DirectoryErrors.None;
        }
        
        public bool CheckForDirectoryErrors(Gtk.Window parent_window, string additinal_message)
        {
            bool is_ok = true;
            string message = String.Empty;
            
            switch(this.CheckConfigutations())
            {
            case DirectoryErrors.None:
                is_ok = true;
                break;
                
            case DirectoryErrors.Idendity:
                message = Catalog.GetString("Directories 'Solid spectra' and 'Fluid spectra' are identical or " +
                                            "one of the directories is containing the other one.");
                is_ok = false;
                break;
                
            case DirectoryErrors.NoActive:
                message = Catalog.GetString("At least one of the 'Spectra directories' must be activated.");
                is_ok = false;
                break;
                
            case DirectoryErrors.AppicationDirectoryNotExist:
                message = Catalog.GetString("The 'Application directory' does not exist.");
                is_ok = false;
                break;
                
            case DirectoryErrors.SolidSpectraDirectoryNotExist:
                message = Catalog.GetString("The 'Solid spectra directory' does not exist.");
                is_ok = false;
                break;
                                
            case DirectoryErrors.FluidSpectraDirectoryNotExist:
                message = Catalog.GetString("The 'Fluid spectra directory' does not exist.");
                is_ok = false;
                break;
                
            case DirectoryErrors.AllSpectraDirectoryNotExist:
                message = Catalog.GetString("The 'All spectra directory' does not exist.");
                is_ok = false;
                break;
            }         
            if (is_ok == false)
            {
                if (string.IsNullOrEmpty(additinal_message) == false)
                    message += "\n" + additinal_message;
                Gtk.MessageDialog error_dialog = new Gtk.MessageDialog(parent_window,
                                                                       Gtk.DialogFlags.Modal,
                                                                       Gtk.MessageType.Error,
                                                                       Gtk.ButtonsType.Close,
                                                                       message);
                error_dialog.Run();
                error_dialog.Destroy();                                                                                              
            }            
            return is_ok;
        }

        private FileSystemWatcher SolidWatcher     {get;set;}
        private FileSystemWatcher FluidWatcher     {get;set;}
        private FileSystemWatcher AllWatcher    {get;set;}
        
        private void ActivateFileWatcher(WatcherTypes watcher_type, string path)
        {
            string message = String.Format(Catalog.GetString("Spectra directory '{0}' "),path);
            
            try
            {
                FileSystemWatcher watcher = new FileSystemWatcher();
                watcher.Path = path;
                watcher.Filter = "*.csv";
                watcher.IncludeSubdirectories = true;
                watcher.Created += this.OnAddedFile;
                watcher.EnableRaisingEvents = true;
                switch (watcher_type)
                {
                case WatcherTypes.All:
                    this.AllWatcher = watcher;
                    break;
                    
                case WatcherTypes.Solid:
                    this.SolidWatcher = watcher;
                    break;
                    
                case WatcherTypes.Fluid:
                    this.FluidWatcher = watcher;
                    break;
                }
            }
            catch(Exception ex)
            {

                Console.WriteLine("{0}: {1}", message, ex.Message);
                
                message += Catalog.GetString("contains one or more inaccessible directories. " +
                                             "Please open the configuration and change the settings or " +
                                             "set the needed rights for selected directory.");
                Gtk.MessageDialog mdlg = new Gtk.MessageDialog(this.MainWindow, Gtk.DialogFlags.Modal,
                                                               Gtk.MessageType.Error, Gtk.ButtonsType.Close,
                                                               message);
                mdlg.Run();
                mdlg.Destroy();    
            }
        }
        
        private void DeactivateFileWatcher(FileSystemWatcher watcher)
        {
            if (watcher == null)
                return;
            
            watcher.EnableRaisingEvents = false;
            watcher.Created -= this.OnAddedFile;
            watcher.IncludeSubdirectories = false;
            watcher = null;
        }
        
        public void OnAddedFile (object sender, FileSystemEventArgs e)
        {        
            Thread.Sleep(100);
            Application.Invoke(sender, e, this.AddFile);                                             
        }
        
        public void AddFile(object sender, EventArgs e)
        {
            int tries = 3;
            FileSystemEventArgs args = e as FileSystemEventArgs;
            FileSystemWatcher watcher = sender as FileSystemWatcher;
            
            while(tries > 0)
            {
                try
                {
                    FileInfo  file = new FileInfo(args.FullPath);

                    CSVFileInformations file_informations_solid = null;
                    CSVFileInformations file_informations_fluid = null;
                    CSVFileInformations file_informations_all    = null;
                    if (Settings.Current.Directories.ConfigurationKind == ConfigurationKind.Simple)
                    {
                        if (watcher == this.AllWatcher)
                        {
                            file_informations_all = CSVFileInformations.CSVFileFilter(file, Spectroscopy.SpectrumType.Unknown);    
                        }
                    }
                    else
                    {
                        if (watcher == this.SolidWatcher)
                        {
                            file_informations_solid = CSVFileInformations.CSVFileFilter(file, Spectroscopy.SpectrumType.ProcessedSolidSpecimen);
                        }
                        else 
                        {
                            if (watcher == this.FluidWatcher)
                            {
                                file_informations_fluid = CSVFileInformations.CSVFileFilter(file, Spectroscopy.SpectrumType.ProcessedFluidSpecimen);
                            }
                        }    
                    }
                    
                    if (file_informations_all == null && file_informations_fluid == null && file_informations_solid == null)
                        return;

                    
                    CSVFileInformations tmp = null;
                    if (file_informations_all != null)
                    {
                        this.AllFiles.Add(file_informations_all);
                        tmp = file_informations_all;
                    }
                    else if (file_informations_fluid != null)
                    {
                        this.AllFiles.Add(file_informations_fluid);
                        tmp = file_informations_fluid;
                    }
                    else
                    {
                        this.AllFiles.Add(file_informations_solid);
                        tmp = file_informations_solid;
                    }
    
                    
                    this.AcquiredSpectraModel.RowInserted += this.OnModelRowInserted;
                    
                    if (this.NewestSpectrum == null || tmp.SpectrumCreationDate > this.NewestSpectrum.SpectrumCreationDate)
                    {
                        this.NewestSpectrum = tmp;
                        this.AcquiredSpectraModel.InsertWithValues(
                             0,
                             tmp.SpectrumType,
                             tmp.SpectrumFilename,
                             tmp.SpectrumCreationDate.ToString("yyyy-MM-dd  HH:mm:ss"),
                             tmp
                         );
                    }
                    else
                    {
                        this.AcquiredSpectraModel.InsertWithValues(
                             this.AcquiredSpectraModel.IterNChildren() - 1,
                             tmp.SpectrumType,
                             tmp.SpectrumFilename,
                             tmp.SpectrumCreationDate.ToString("yyyy-MM-dd  HH:mm:ss"),
                             tmp
                         );
                    }
                    break;
                }
                catch(System.IO.IOException ex)
                {
                    Console.WriteLine("Adding file: {0}", ex.Message);
                    tries--;
                    Thread.Sleep(200);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Adding file: {0}", ex.Message);
                    break;
                }
            }
            this.AcquiredSpectraModel.RowInserted -= this.OnModelRowInserted;;
        }
        
        private void OnModelRowInserted(object o, RowInsertedArgs args)
        {
            this.acquired_spectra_treeview.Selection.SelectIter(args.Iter);
        }
        

        private ApplicationDict Applications    {get;set;}
        private CSVFileInformationsList AllFiles{get;set;}
        
        public void UpdateAllDirectories()
        {
            this.main_alignment.Sensitive = false;

            if (this.main_window.GdkWindow != null)
                this.MainWindow.GdkWindow.Cursor = new Gdk.Cursor(Gdk.CursorType.Watch);
            
            this.Host.ProcessEvents();
            
            if (this.Applications == null)
                this.Applications = new ApplicationDict();
            
            if (this.AllFiles == null)
                this.AllFiles = new CSVFileInformationsList();

            this.AllFiles.Clear();
            this.Applications = this.LoadQAMFiles(new DirectoryInfo(Settings.Current.Directories.ApplicationDirectory));
            
            if ( Settings.Current.Directories.ConfigurationKind == ConfigurationKind.Simple)
            {
                this.AllFiles = this.LoadCSVFiles(new DirectoryInfo(Settings.Current.Directories.AllSpectraDirectory),
                                                      Spectroscopy.SpectrumType.Unknown);
                
                this.ActivateFileWatcher(WatcherTypes.All , Settings.Current.Directories.AllSpectraDirectory);
            }
            else
            {
                CSVFileInformationsList solid = null;
                CSVFileInformationsList fluid = null;
                if (Settings.Current.Directories.SolidSpectraDirectoryActivated == true)
                {
                    solid = this.LoadCSVFiles(new DirectoryInfo(Settings.Current.Directories.SolidSpectraDirectory),
                                              Spectroscopy.SpectrumType.ProcessedSolidSpecimen);
                    this.ActivateFileWatcher(WatcherTypes.Solid, Settings.Current.Directories.SolidSpectraDirectory);
                }
                if (Settings.Current.Directories.FluidSpectraDirectoryActivated == true)
                {
                    fluid = this.LoadCSVFiles(new DirectoryInfo(Settings.Current.Directories.FluidSpectraDirectory),
                                              Spectroscopy.SpectrumType.ProcessedFluidSpecimen);
                    this.ActivateFileWatcher(WatcherTypes.Fluid, Settings.Current.Directories.FluidSpectraDirectory);
                }
                
                if (solid != null && solid.Count > 0)
                    this.AllFiles.AddRange(solid);
                if (fluid != null && fluid.Count > 0)
                    this.AllFiles.AddRange(fluid);              
            }
            
            this.UpdateAcquiredSpectraTV();

            if (this.main_window.GdkWindow != null)
                this.MainWindow.GdkWindow.Cursor = new Gdk.Cursor(Gdk.CursorType.Arrow);
            
            this.main_alignment.Sensitive = true;
        }    
        
        private ApplicationDict LoadQAMFiles(DirectoryInfo dir_info)
        {
            ApplicationDict dict = new ApplicationDict();
            
            try
            {
                FileInfo[] file_info = dir_info.GetFiles("*.qam");
                
                foreach(FileInfo file in file_info)
                {
                    QAMFileInformations qam = QAMFileInformations.ReadQAM(file);
                    if (qam != null && dict.ContainsKey(qam.ApplicationID) == false)
                        dict.Add(qam.ApplicationID, qam);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Loading QAM-File: {0}", ex.Message);
            }
            
            return dict;
        }
        
        private bool LoadApplicationFromDLL(QAMFileInformations qam)
        {
            bool success = false;
            Assembly app_assembly = Assembly.LoadFrom(qam.DllFileName);
            
            foreach (Type type in app_assembly.GetTypes())
            {
                try
                {
                    if (type.IsPublic == false || (type.Attributes & TypeAttributes.Abstract) != 0)
                        continue;
                                
                    Type app_type = type.GetInterface("Hiperscan.Extensions.QualityAssurance.IApplication");            
                    if (app_type == null)
                        continue;
                                            
                    IApplication application = (IApplication)app_assembly.CreateInstance(type.FullName);
                    application.Initialize();
                    qam.Application = application;
                    success = true;
                    break;
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Loading Application: {0}", ex.Message);
                }            
            }
            
            return success;
        }
        

        
        private CSVFileInformationsList LoadCSVFiles(DirectoryInfo dir_info, Spectroscopy.SpectrumType type)
        {
            CSVFileInformationsList list = new CSVFileInformationsList();    
            try
            {                
                FileInfo[] file_info = dir_info.GetFiles("*.csv");
                foreach(FileInfo file in file_info)
                {
                    CSVFileInformations fi = CSVFileInformations.CSVFileFilter(file, type);
                    if (fi == null)
                        continue;
                    list.Add(fi);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("LoadCSVFiles: {0}", ex.Message);
            }
            
            try
            {
                foreach(DirectoryInfo recursive_dir in dir_info.GetDirectories())
                {

                    CSVFileInformationsList recursive_list = this.LoadCSVFiles(recursive_dir, type);
                    if (recursive_list.Count > 0)
                        list.AddRange(recursive_list);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("LoadCSVFiles: {0}", ex.Message);
            }
            return list;
        }
    

        #region IDisposable implementation
        public void Dispose ()
        {
            this.DeactivateFileWatcher(this.AllWatcher);
            this.DeactivateFileWatcher(this.SolidWatcher);
            this.DeactivateFileWatcher(this.FluidWatcher);
        }
        #endregion
    }
}

