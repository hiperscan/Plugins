using System;
using Gtk;
using Glade;
using Mono.Unix;
using System.IO;
using System.Threading;

namespace Hiperscan.Extensions.QualityAssurance
{
    public enum ModelColumns
    {
        SpectrumType,
        SpectrumFilename,
        SpectrumCreationDate,
        SpectrumID,
        SpectrumFullFilename,
        SpectrumEvaluation,
    }
    
    public partial class QualityAssuranceApp
    {
        [Widget] Gtk.Window main_window = null;
            
        [Widget] Gtk.Button configurations_button     = null;
        [Widget] Gtk.Button close_button             = null;

        [Widget] Gtk.TreeView acquired_spectra_treeview = null;
        [Widget] Gtk.Button first_button    = null;
        [Widget] Gtk.Button preview_button    = null;
        [Widget] Gtk.Button next_button        = null;
        [Widget] Gtk.Button last_button        = null;
        [Widget] Gtk.Entry index_entry        = null;
        
        [Widget] Gtk.Alignment traffic_alignment    = null;
        [Widget] Gtk.Alignment main_alignment         = null;
                
        
        public QualityAssuranceApp(IHost host)
        {
            this.Host = host;
            this.IsConfigWindowOpened = false;
            this.InitializeConfigurations();
            this.InitGUI();
            
            if (this.CheckForDirectoryErrors(this.MainWindow, "Please check the configurations.") == false)    
            {
                this.IsConfigWindowOpened = true;
                new Preferences(this);
            }

            while(this.IsConfigWindowOpened == true)
            {
                this.Host.ProcessEvents();
                Thread.Sleep(100);
            }
            this.Host.ProcessEvents();
            
            this.UpdateAllDirectories();
        }
        
        private void InitGUI()
        {
            Glade.XML gui;
            gui = new Glade.XML(null, "QualityAssurance.Resources.Glade.QualityAssuranceUI.glade", "main_window", null);
            gui.Autoconnect(this);
            
            this.MainWindow = this.main_window;
            
            this.MainWindow.Title         = "Quickstep - " + "Quality Assurance";
            this.MainWindow.IconList     = Plugin.Icons;
                            
            this.MainWindow.DeleteEvent += this.OnMainWindowDeleted;
            this.close_button.Clicked += this.OnMainWindowDeleted;
            this.configurations_button.Clicked += OnConfigurationButtonClicked;

            
            this.InitAllSpectraTreeView();
            
            this.TrafficLight = new TrafficLightArea();
            this.traffic_alignment.Child = this.TrafficLight;
            this.TrafficData = new TrafficLightData();
            
            
            this.first_button.Clicked += this.OnFirstButtonClicked;
            this.last_button.Clicked += this.OnLastButtonClicked;
            this.preview_button.Clicked += this.OnPreviewButtonClicked;
            this.next_button.Clicked += this.OnNextButtonClicked;
            this.index_entry.FocusOutEvent += this.OnIndexEntryFocusedOut;
            this.index_entry.Activated += this.OnIndexEntryActivated;

            this.MainWindow.ShowAll();    
    
        }
        
        private Gtk.ListStore AcquiredSpectraModel {get;set;}
        
        private void InitAllSpectraTreeView()
        {
            Gtk.TreeViewColumn type_col = new Gtk.TreeViewColumn();
            type_col.Title = Catalog.GetString("Spectrum type");
            type_col.Alignment = 0.5f;
            type_col.Expand = true;
            Gtk.CellRendererText type_cell = new Gtk.CellRendererText();
            type_cell.Xalign = 0.5f;
            type_cell.Xpad = 5;
            type_col.PackStart(type_cell, true);
            type_col.Clickable = true;
            type_col.Clicked += this.OnTypeColumnClicked;

            Gtk.TreeViewColumn file_col = new Gtk.TreeViewColumn();
            file_col.Title = Catalog.GetString("Filename");
            file_col.Expand = true;
            file_col.Alignment = 0.5f;            
            Gtk.CellRendererText file_cell = new Gtk.CellRendererText();
            file_cell.Xalign = 0.5f;
            file_cell.Xpad = 5;
            file_col.PackStart(file_cell, true);
            file_col.Clickable = true;
            file_col.Clicked += this.OnFileColumnClicked;

            Gtk.TreeViewColumn date_col = new Gtk.TreeViewColumn();
            date_col.Title = Catalog.GetString("Date");
            date_col.Expand = true;
            date_col.Alignment = 0.5f;            
            Gtk.CellRendererText date_cell = new Gtk.CellRendererText();
            date_cell.Xalign = 0.5f;
            date_cell.Xpad = 5;
            date_col.PackStart(date_cell, true);
            date_col.Clickable = true;
            date_col.Clicked += this.OnDateColumnClicked;
            
                    
            this.acquired_spectra_treeview.AppendColumn(type_col);
            this.acquired_spectra_treeview.AppendColumn(file_col);
            this.acquired_spectra_treeview.AppendColumn(date_col);

            this.AcquiredSpectraModel = new ListStore
                (
                 typeof(string),            //Spectrum type
                 typeof(string),            //Filename
                 typeof(string),            //Creation date
                 typeof(CSVFileInformations)//Reference to List-object
                 );
            
            this.acquired_spectra_treeview.Model = this.AcquiredSpectraModel;
            
            type_col.AddAttribute(type_cell, "text", 0);
            file_col.AddAttribute(file_cell, "text", 1);
            date_col.AddAttribute(date_cell, "text", 2);

            this.AcquiredSpectraModel.SetSortFunc(0, this.CompareSpectrumTypes);
            this.AcquiredSpectraModel.SetSortFunc(1, this.CompareSpectrumFiles);
            this.AcquiredSpectraModel.SetSortFunc(2, this.CompareSpectrumDates);

            this.LastCol = date_col;
            date_col.SortOrder = SortType.Descending;
            this.AcquiredSpectraModel.SetSortColumnId(2, SortType.Descending);
            date_col.SortIndicator = true;

            this.acquired_spectra_treeview.Selection.Changed += this.OnAllSpectraSelectionChanged;
        }

        private Gtk.TreeViewColumn LastCol { get; set; }
        private void OnTypeColumnClicked (object sender, EventArgs e)
        {
            Gtk.TreeViewColumn col =  this.acquired_spectra_treeview.Columns[0];
            if(this.LastCol == col)
            {
                if (col.SortOrder == SortType.Ascending)
                    col.SortOrder = SortType.Descending;
                else
                    col.SortOrder = SortType.Ascending;
            }
            else
            {
                col.SortOrder = SortType.Ascending;
                this.LastCol.SortIndicator = false;
                this.LastCol = col;
                col.SortIndicator = true;
            }
            this.AcquiredSpectraModel.SetSortColumnId(0, col.SortOrder);
            this.UpdateLeftSide();
        }

        private void OnFileColumnClicked (object sender, EventArgs e)
        {
            Gtk.TreeViewColumn col =  this.acquired_spectra_treeview.Columns[1];
            if(this.LastCol == col)
            {
                if (col.SortOrder == SortType.Ascending)
                    col.SortOrder = SortType.Descending;
                else
                    col.SortOrder = SortType.Ascending;
            }
            else
            {
                col.SortOrder = SortType.Ascending;
                this.LastCol.SortIndicator = false;
                this.LastCol = col;
                col.SortIndicator = true;
            }
            this.AcquiredSpectraModel.SetSortColumnId(1, col.SortOrder);
            this.UpdateLeftSide();
        }

        private void OnDateColumnClicked (object sender, EventArgs e)
        {
            Gtk.TreeViewColumn col =  this.acquired_spectra_treeview.Columns[2];
            if(this.LastCol == col)
            {
                if (col.SortOrder == SortType.Ascending)
                    col.SortOrder = SortType.Descending;
                else
                    col.SortOrder = SortType.Ascending;

            }
            else
            {
                col.SortOrder = SortType.Descending;
                this.LastCol.SortIndicator = false;
                this.LastCol = col;
                col.SortIndicator = true;
            }
            this.AcquiredSpectraModel.SetSortColumnId(2, col.SortOrder);
            this.UpdateLeftSide();
        }



        private int CompareSpectrumTypes(TreeModel model, TreeIter a, TreeIter b)
        {
            string s1 = (string)model.GetValue(a, 0);
            string s2 = (string)model.GetValue(b, 0);
            return string.Compare(s1, s2);
        }

        private int CompareSpectrumFiles(TreeModel model, TreeIter a, TreeIter b)
        {
            string s1 = (string)model.GetValue(a, 1);
            string s2 = (string)model.GetValue(b, 1);
            return string.Compare(s1, s2);
        }

        private int CompareSpectrumDates(TreeModel model, TreeIter a, TreeIter b)
        {
            string s1 = (string)model.GetValue(a, 2);
            string s2 = (string)model.GetValue(b, 2);
            return string.Compare(s1, s2);
        }

        private CSVFileInformations NewestSpectrum{ get; set; }
        
        private void UpdateAcquiredSpectraTV()
        {
            this.NewestSpectrum = null;
            DateTime min_newest_date = DateTime.MinValue;
            this.AcquiredSpectraModel.Clear();
            foreach(CSVFileInformations file_informations in this.AllFiles)
            {
                if (min_newest_date < file_informations.SpectrumCreationDate)
                {
                    min_newest_date = file_informations.SpectrumCreationDate;
                    this.NewestSpectrum = file_informations;
                }

                this.AcquiredSpectraModel.AppendValues
                    (
                     file_informations.SpectrumType,
                     file_informations.SpectrumFilename,
                     file_informations.SpectrumCreationDate.ToString("yyyy-MM-dd  HH:mm:ss"),
                     file_informations
                     );
            }
            if (this.AcquiredSpectraModel.IterNChildren() > 0)
                this.OnFirstButtonClicked(null, null);
            else
                this.UpdateGUI();
        }
                        
        private void OnMainWindowDeleted(object sender, EventArgs e)
        {
            if (Plugin.replace_gui)
                this.Host.Quit();
            this.MainWindow.Destroy();
        }
        
        private void OnConfigurationButtonClicked(object sender, EventArgs e)
        {
            this.IsConfigWindowOpened = true;
            new Preferences(this);
            
            while(this.IsConfigWindowOpened == true)
            {
                this.Host.ProcessEvents();
                Thread.Sleep(100);
            }
            this.UpdateAllDirectories();
        }

        private int GetCurrentRowIndex ()
        {
            int index = -1;
            TreeIter iter;
            if (this.acquired_spectra_treeview.Selection.GetSelected(out iter) == true)
            {
                this.AcquiredSpectraModel.GetPath(iter);
                TreePath treepath = this.AcquiredSpectraModel.GetPath(iter);
                index = treepath.Indices[0];
            }
            return index;
        }
        
        private void OnFirstButtonClicked(object sender, EventArgs e)
        {
            int count = this.AcquiredSpectraModel.IterNChildren();
            if (count > 0)
            {
                TreeIter iter;
                this.AcquiredSpectraModel.IterNthChild(out iter, 0);
                this.acquired_spectra_treeview.Selection.SelectIter(iter);
            }
        }
        
        private void OnLastButtonClicked(object sender, EventArgs e)
        {
            int count = this.AcquiredSpectraModel.IterNChildren();
            if (count > 0)
            {
                TreeIter iter;
                this.AcquiredSpectraModel.IterNthChild(out iter, count - 1);
                this.acquired_spectra_treeview.Selection.SelectIter(iter);
            }
        }
        
        private void OnPreviewButtonClicked(object sender, EventArgs e)
        {
            int count = this.AcquiredSpectraModel.IterNChildren();
            if (count > 0)
            {
                if (this.GetCurrentRowIndex () > 0)
                {
                    TreeIter iter;
                    this.AcquiredSpectraModel.IterNthChild(out iter, this.GetCurrentRowIndex () - 1);
                    this.acquired_spectra_treeview.Selection.SelectIter(iter);
                }
            }
        }
        
        private void OnNextButtonClicked(object sender, EventArgs e)
        {
            int count = this.AcquiredSpectraModel.IterNChildren();
            if (count > 0)
            {
                
                if (this.GetCurrentRowIndex () < count - 1)
                {
                    TreeIter iter;
                    this.AcquiredSpectraModel.IterNthChild(out iter, this.GetCurrentRowIndex () + 1);
                    this.acquired_spectra_treeview.Selection.SelectIter(iter);
                }
            }
        }
        
        private void OnIndexEntryFocusedOut(object sender, EventArgs e)
        {
            try
            {
                int index = Int32.Parse(this.index_entry.Text.Trim()) - 1;
                int count = this.AcquiredSpectraModel.IterNChildren();
                if (count > 0)
                {
                    if (index > -1 && index < count)
                    {                        TreeIter iter;
                        this.AcquiredSpectraModel.IterNthChild(out iter, index);
                        this.acquired_spectra_treeview.Selection.SelectIter(iter);
                    }
                    else
                    {
                        this.UpdateLeftSide();
                    }
                }    
            }
            catch(Exception){}    
        }
        
        private void OnIndexEntryActivated(object sender, EventArgs e)
        {
            try
            {
                int index = Int32.Parse(this.index_entry.Text.Trim()) - 1;
                int count = this.AcquiredSpectraModel.IterNChildren();
                if (count > 0)
                {
                    if (index > -1 && index < count)
                    {                        TreeIter iter;
                        this.AcquiredSpectraModel.IterNthChild(out iter, index);
                        this.acquired_spectra_treeview.Selection.SelectIter(iter);
                    }
                    else
                    {
                        this.UpdateLeftSide();
                    }
                }    
            }
            catch(Exception){}
            this.index_entry.Position = this.index_entry.Text.Length;
        }


        private void OnAllSpectraSelectionChanged(object sender, EventArgs e)
        {
            this.UpdateGUI();
        }

        private void UpdateNavigationElements()
        {
            int index = this.GetCurrentRowIndex();
            if (index == -1)
            {
                this.first_button.Sensitive = false;
                this.last_button.Sensitive = false;
                this.next_button.Sensitive = false;
                this.preview_button.Sensitive = false;
                this.index_entry.Text = String.Empty;
                this.index_entry.Sensitive = false;        
            }
            else
            {            
                this.first_button.Sensitive = true;
                this.last_button.Sensitive = true;
                this.next_button.Sensitive = true;
                this.preview_button.Sensitive = true;
                this.index_entry.Text = (index + 1).ToString();
                this.index_entry.Sensitive = true;
                if (index <= 0)
                {
                    this.first_button.Sensitive = false;
                    this.preview_button.Sensitive = false;
                }
                
                if (index == this.AcquiredSpectraModel.IterNChildren() - 1)
                {
                    this.last_button.Sensitive = false;
                    this.next_button.Sensitive = false;
                }
            }
        }
        
        private object GetCurrentRowValues(ModelColumns column)
        {
            object ret_val = null;
            TreeIter iter;
            if (this.acquired_spectra_treeview.Selection.GetSelected(out iter) == true)
            {             
                CSVFileInformations file_informations = (CSVFileInformations)this.AcquiredSpectraModel.GetValue(iter, 3);
                switch(column)
                {
                case ModelColumns.SpectrumCreationDate:
                    ret_val = file_informations.SpectrumCreationDate;
                    break;
                    
                case ModelColumns.SpectrumFilename:
                    ret_val = file_informations.SpectrumFilename;
                    break;
                    
                case ModelColumns.SpectrumFullFilename:
                    ret_val = Path.Combine(file_informations.SpectrumPath, file_informations.SpectrumFilename);
                    break;
                    
                case ModelColumns.SpectrumID:
                    ret_val = file_informations.SpectrumId;
                    break;
                    
                case ModelColumns.SpectrumType:
                    ret_val = file_informations.SpectrumType;
                    break;

                case ModelColumns.SpectrumEvaluation:
                    ret_val = file_informations.Evaluation;
                    break;

                }
            }
            return ret_val;
        }

        private void SetCurrentRowValues(ModelColumns column, object value)
        {
            TreeIter iter;
            if (this.acquired_spectra_treeview.Selection.GetSelected(out iter) == true)
            {             
                CSVFileInformations file_informations = (CSVFileInformations)this.AcquiredSpectraModel.GetValue(iter, 3);

                switch (column)
                {
                
                case ModelColumns.SpectrumEvaluation:
                    file_informations.Evaluation = (SpectrumEvaluation)value;
                    break;

                default:
                    break;

                }
            }
        }

        private void UpdateLeftSide()
        {
            this.UpdateNavigationElements();
            TreeIter iter;
            if (this.acquired_spectra_treeview.Selection.GetSelected(out iter) == true)
            {
                TreePath path = this.AcquiredSpectraModel.GetPath(iter);
                this.acquired_spectra_treeview.ScrollToCell(path, this.acquired_spectra_treeview.Columns[0], false, 0.5f, 0.5f);
            }
        }
        
        private void EvaluateSpectrum()
        {
            if (this.GetCurrentRowIndex() == -1)
            {
                this.TrafficData.TrafficState = TrafficLightStates.NoSpectrumLoaded;
                return;
            }
            
            Spectroscopy.Spectrum spectrum = null;
            string file_name = (string)this.GetCurrentRowValues(ModelColumns.SpectrumFullFilename);
            try
            {
                spectrum = Hiperscan.Spectroscopy.IO.CSV.Read(file_name);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Evaluation of spectrum: {0}", ex.Message);
                this.TrafficData.TrafficState = TrafficLightStates.CannotOpenSpectrum;
                return;
            }
            
            string spectrum_id = (string)this.GetCurrentRowValues(ModelColumns.SpectrumID);
            if (this.Applications.ContainsKey(spectrum_id) == false)
            {
                this.TrafficData.TrafficState = TrafficLightStates.NoApplicationAvailable;
                return;
            }
            
            if (this.Applications[spectrum_id].Application == null)
            {
                try
                {
                    if (this.LoadApplicationFromDLL(this.Applications[spectrum_id]) == false)
                    {
                        this.TrafficData.TrafficState = TrafficLightStates.CannotOpenApplication;
                        return;
                    }
        
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Evaluation of spectrum: {0}", ex.Message);
                    this.TrafficData.TrafficState = TrafficLightStates.CannotOpenApplication;
                    return;
                }
            }
            
            try
            {
                // evaluate spectrum every time (no caching)
//                SpectrumEvaluation eval = (SpectrumEvaluation)this.GetCurrentRowValues(ModelColumns.SpectrumEvaluation);
//                this.TrafficData.LightWithAnimation = true;
//                if (eval == SpectrumEvaluation.None)
//                {
//                    SpectrumEvaluation eval = this.Applications[spectrum_id].Application.Evaluate(spectrum);
//                    this.SetCurrentRowValues(ModelColumns.SpectrumEvaluation, eval);
//                }
        
                SpectrumEvaluation eval = this.Applications[spectrum_id].Application.Evaluate(spectrum);
                this.SetCurrentRowValues(ModelColumns.SpectrumEvaluation, eval);

                switch(eval)
                {

                case SpectrumEvaluation.Red:
                    this.TrafficData.TrafficState = TrafficLightStates.RedEvaluatedSpectrum;
                    break;
                    
                case SpectrumEvaluation.Yellow:
                    this.TrafficData.TrafficState = TrafficLightStates.YellowEvaluatedSpectrum;
                    break;
                    
                case SpectrumEvaluation.Green:
                    this.TrafficData.TrafficState = TrafficLightStates.GreenEvaluatedSpectrum;
                    break;

                }
                this.TrafficData.SpectrumFileName = (string)this.GetCurrentRowValues(ModelColumns.SpectrumFilename);
                this.TrafficData.ApplicationName = this.Applications[spectrum_id].Application.Name;
                this.TrafficData.ApplicationDescription = this.Applications[spectrum_id].Application.Description;
                
                string path_newest_spectrum = Path.Combine(this.NewestSpectrum.SpectrumPath, this.NewestSpectrum.SpectrumFilename);
                this.TrafficData.NewestSpectrum = path_newest_spectrum == (string) this.GetCurrentRowValues(ModelColumns.SpectrumFullFilename);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Evaluation of spectrum {0} failed: {1}", spectrum.Label, ex.ToString());
                this.TrafficData.TrafficState = TrafficLightStates.CannotEvaluateSpectrum;
            }
        }
        
        private void UpdateGUI()
        { 
            //Updates left side of GUI
            this.UpdateLeftSide();
            
            // Evaluation of selected spectrum
            this.EvaluateSpectrum();
            
            //this.UpdateOneSpectrumSection();
            this.TrafficLight.Update(this.TrafficData);
        }
        
        public IHost Host                    {get; private set;}
        public Gtk.Window MainWindow         {get; private set;}
        public bool IsConfigWindowOpened         {get;set;}
        private TrafficLightArea TrafficLight    {get;set;}
        private TrafficLightData TrafficData    {get;set;}
    }
}

