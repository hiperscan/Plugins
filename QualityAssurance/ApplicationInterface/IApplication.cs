using System;
using Hiperscan.Spectroscopy;


namespace Hiperscan.Extensions.QualityAssurance
{
    public enum SpectrumEvaluation
    {
        None,
        Red,
        Yellow,
        Green,
    }
    
    public interface IApplication
    {
        //
        //Constructor with file_name
        //
        
        //App_ID == first group of CSV-files (spearated by '_')
        string AppID {get;}
        
        //Name of application
        string Name {get;}
        
        //Short description of application
        string Description {get;}
        
        //Evaluate the spectrum and returns the result in form of green till red
        SpectrumEvaluation Evaluate(Spectrum spectrum);    
        
        //Initializing the application
        void Initialize();
    }
}

