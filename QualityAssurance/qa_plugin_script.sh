#!/bin/bash

set -e

QA_ROOT_DIR=$(pwd)
QA_ROOT_DIR=$(readlink -e "$QA_ROOT_DIR/../../")
QA_ROOT_DIR="$QA_ROOT_DIR/QAPlugins"

if [ ! -d "$QA_ROOT_DIR" ]
then
 	exit
fi


PROFILE=$1

if [ -z "$PROFILE" ]
then
	echo "Cannot create applications without configuration profile"
 	exit
fi


APPLICATION_NAME="Applications"
APPLICATION_DIR="$QA_ROOT_DIR/$APPLICATION_NAME"

rm -Rf $APPLICATION_DIR
mkdir $APPLICATION_DIR


for APP in $(ls "$QA_ROOT_DIR")
do
	if [ ! -d "$QA_ROOT_DIR/$APP" ]
	then
		continue
	fi

	if [ "$APP" == "$APPLICATION_NAME" ]
	then
		continue
	fi

	if [ "$APP" == "test_data" ]
	then
		continue
	fi

	echo "Found QA_Plugin $APP ..."

	
	BIN_DIR="$QA_ROOT_DIR/$APP/bin/$PROFILE"
	if [ ! -d "$BIN_DIR" ]
	then
		echo "Cannot find binaries for this app"
		echo
		echo
		continue
	fi


	QAM_FILE=$(find "$BIN_DIR" -name *.qam | head -n 1)
	if [ -z "$QAM_FILE" ]
	then
		echo "Cannot find binaries for this app"
		echo
		echo
		continue
	fi


	QAM_FILE=$(basename "$QAM_FILE")
	QAM_DIR=$(echo "${QAM_FILE%.*}")
	echo "Found QAM -_file $QAM_FILE $QAM_DIR   $BIN_DIR ..."
	echo 
	echo


	SOURCE="$BIN_DIR/$QAM_FILE"
	DESTINATION="$APPLICATION_DIR/$QAM_FILE"
	ln -s "$SOURCE" "$DESTINATION"  

	SOURCE="$BIN_DIR"
	DESTINATION="$APPLICATION_DIR/$QAM_DIR"
	ln -s "$SOURCE" "$DESTINATION"  
done
