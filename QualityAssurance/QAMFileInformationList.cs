using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Hiperscan.Spectroscopy;
using System.Text;

using Hiperscan.Spectroscopy.IO;
using Mono.Unix;

namespace Hiperscan.Extensions.QualityAssurance
{
    public class QAMFileInformations
    {
        private static readonly string Separator = "=";
        
        public string ApplicationID             {get;private set;}
        public string DllFileName                 {get;private set;}
        public IApplication Application            {get; set;}
        
        public QAMFileInformations()
        {
            this.Application = null;
            this.DllFileName = String.Empty;
            this.ApplicationID = String.Empty;
        }
        

        
        public static QAMFileInformations ReadQAM(FileInfo file)
        {
            QAMFileInformations qam = new QAMFileInformations();
            string pattern = @"^(\d)+$";
            Regex expression = new Regex(pattern);
            using(StreamReader sr = new StreamReader(file.FullName, Encoding.UTF8))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] line_data = line.Split(new string[]{Separator}, StringSplitOptions.RemoveEmptyEntries);
                    if (line_data.Length != 2)
                        continue;
                    
                    switch(line_data[0].Trim().ToLower())
                    {
                    case "applicationid":
                        string id = line_data[1].Trim();
                        if (expression.Match(id).Success == true)
                            qam.ApplicationID = id;
                        break;
                        
                    default:
                        break;
                    }
                }
            }
            if (String.IsNullOrEmpty(qam.ApplicationID) == true)
                return null;
            
            string application_name = Path.GetFileNameWithoutExtension(file.Name);
            qam.DllFileName = Path.Combine(file.DirectoryName, application_name);
            qam.DllFileName = Path.Combine(qam.DllFileName, application_name + ".dll");
            
            return qam;
        }
    }
    
    public class ApplicationDict : Dictionary<string, QAMFileInformations>{}
}

