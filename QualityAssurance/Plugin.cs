using System;
using Hiperscan.Extensions;
using System.Reflection;
using Mono.Unix;

using Gtk;

namespace Hiperscan.Extensions.QualityAssurance
{

    public class Plugin : IPlugin
    {
        private bool active = true;
        
        public static IHost Host             {get;set;}
        public static Gdk.Pixbuf[] Icons    {get;set;}
        public static readonly string PluginGUID = "ec40c527-135e-40f6-9291-5584eddf5472";
        public static readonly  string PluginName = Catalog.GetString("Quality-Assurance");

        public static bool replace_gui = false;
        
        private MenuInfo[] PluginMenu        {get;set;}
        private ToolbarInfo[] PluginToolbar    {get;set;}
        
        
        public Plugin()
        {
            
            MenuInfo menu_info1 = new MenuInfo(Catalog.GetString("Quality Assurance"));
            menu_info1.Invoked += (sender, e) => 
            {
                new QualityAssuranceApp(Plugin.Host);
            };            
            this.PluginMenu = new MenuInfo[] {menu_info1};
            
            string quality_assurance_logo_fname = "QualityAssurance.Resources.Images.qa128x128.png";
            Plugin.Icons = new Gdk.Pixbuf[5];
            
            Plugin.Icons[0] = new Gdk.Pixbuf(null, quality_assurance_logo_fname, 16, 16);
            Plugin.Icons[1] = new Gdk.Pixbuf(null, quality_assurance_logo_fname, 32, 32);
            Plugin.Icons[2] = new Gdk.Pixbuf(null, quality_assurance_logo_fname, 48, 48);
            Plugin.Icons[3] = new Gdk.Pixbuf(null, quality_assurance_logo_fname, 64, 64);
            Plugin.Icons[4] = new Gdk.Pixbuf(null, quality_assurance_logo_fname, 128, 128);
            
            IconSet icon_set = new IconSet();
            foreach (Gdk.Pixbuf pixbuf in Plugin.Icons)
            {
                IconSource icon_source = new IconSource();
                icon_source.Pixbuf = pixbuf;
                icon_set.AddSource(icon_source);
            }
            IconFactory icon_factory = new IconFactory();
            icon_factory.Add("hiperscan.qualityAssurance", icon_set);
            icon_factory.AddDefault();
            
            ToolButton b1 = new ToolButton("hiperscan.qualityAssurance");
            ToolbarInfo toolbar_info1 = new ToolbarInfo(b1, 
                                                        Catalog.GetString("Quality Assurance"), 
                                                        Catalog.GetString("Start Quality Assurance"));
            
            toolbar_info1.Invoked += delegate(object sender, EventArgs e) 
            {
                new QualityAssuranceApp(Plugin.Host);
            };
            
            this.PluginToolbar = new ToolbarInfo[] {toolbar_info1};

        }
        
        
        #region IPlugin implementation
        public void Initialize(IHost host)
        {
            Plugin.Host = host;
            Settings.Init();
        }

        public void ReplaceGui()
        {
            Plugin.replace_gui = true;
            new QualityAssuranceApp(Plugin.Host);
        }

        public MenuInfo[] MenuInfos 
        {
            get 
            {
                return this.PluginMenu;
            }
        }

        public ToolbarInfo[] ToolbarInfos 
        {
            get 
            {
                return this.PluginToolbar;
            }
        }

        public PlotTypeInfo[] PlotTypeInfos 
        {
            get 
            {
                return null;
            }
        }

        public FileTypeInfo[] FileTypeInfos 
        {
            get 
            {
                return null;
            }
        }

        public string Name 
        {
            get 
            {
                return Plugin.PluginName;
            }
        }

        public string Version 
        {
            get 
            {
                string[] temp = Assembly.GetExecutingAssembly().GetName().Version.ToString().Split(new char[]{'.'});
                return String.Join(".", new string[]{temp[0], temp[1]});
            }
        }

        public string Vendor 
        {
            get 
            {
                return "Hiperscan GmbH";
            }
        }

        public Assembly Assembly 
        {
            get 
            {
                return Assembly.GetExecutingAssembly();
            }
        }

        public string Description 
        {
            get 
            {
                return Catalog.GetString("Quality assurance plugin for QuickStep");
            }
        }

        public Guid Guid 
        {
            get 
            {
                return new Guid(Plugin.PluginGUID);
            }
        }

        public bool Active 
        {
            get 
            {
                return this.active;
            }
            set 
            {
                this.active = value;
            }
        }

        public bool CanReplaceGui 
        {
            get 
            {
                return true;
            }
        }
        #endregion    
    }
}

