using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Hiperscan.Spectroscopy;

using Hiperscan.Spectroscopy.IO;
using Mono.Unix;

namespace Hiperscan.Extensions.QualityAssurance
{
    public class CSVFileInformations
    {
        public CSVFileInformations(FileInfo file, Spectrum spectrum)
        {
            this.SpectrumFilename = file.Name;
            this.SpectrumPath = file.DirectoryName;
            this.SpectrumCreationDate = spectrum.Timestamp;
            switch(spectrum.SpectrumType)
            {
            case Hiperscan.Spectroscopy.SpectrumType.ProcessedSolidSpecimen:
                this.SpectrumType = Catalog.GetString("Solid");
                break;
                
            case Hiperscan.Spectroscopy.SpectrumType.ProcessedFluidSpecimen:
                this.SpectrumType = Catalog.GetString("Fluid");
                break;
                
            default:
                this.SpectrumType = String.Empty;
                break;
            }
            
            string[] data = file.Name.Split(new char[]{'_'}, StringSplitOptions.RemoveEmptyEntries);
            Regex regex = new Regex(@"^(\d)+$");
            if (regex.Match(data[0]).Success == true)
            {
                long id = Convert.ToInt64(data[0]);
//                this.SpectrumId = (id % 100).ToString("00");
                this.SpectrumId = id.ToString();
            }
            else
                this.SpectrumId = string.Empty;

            this.Evaluation = SpectrumEvaluation.None;
        }
        
        public string SpectrumType                 {get; private set;}
        public string SpectrumFilename            {get; private set;}
        public DateTime SpectrumCreationDate    {get; private set;}
        public string SpectrumId                {get; private set;}
        public string SpectrumPath                {get; private set;}
        public SpectrumEvaluation Evaluation    {get; set;}
        
        public static CSVFileInformations CSVFileFilter(FileInfo file, Spectroscopy.SpectrumType type)
        {
            Regex expression = new Regex(@"^(\d)+_([A-Z])+(_G(\d)+)?_(\d)+\.csv$");
            Regex white_expr = new Regex(@"^(\d)+_(\d){4}[A-Z]{1,2}-(\d){3}.*\.csv$");

            if (expression.IsMatch(file.Name) == false && white_expr.IsMatch(file.Name) == false)
                return null;
            
            if (System.IO.File.Exists(file.FullName) == false)
                return null;
            CSVFileInformations tmp = null;
            try
            {
                Spectrum spectrum = CSV.Read(file.FullName, 25);
                switch(type)
                {
                case Spectroscopy.SpectrumType.Unknown:
                    if (spectrum.SpectrumType == Hiperscan.Spectroscopy.SpectrumType.ProcessedSolidSpecimen ||
                        spectrum.SpectrumType == Hiperscan.Spectroscopy.SpectrumType.ProcessedFluidSpecimen)
                        tmp = new CSVFileInformations(file, spectrum);
                    break;
                    
                case Spectroscopy.SpectrumType.ProcessedSolidSpecimen:
                    if (spectrum.SpectrumType == Hiperscan.Spectroscopy.SpectrumType.ProcessedSolidSpecimen)
                        tmp = new CSVFileInformations(file, spectrum);
                    break;
                    
                case Spectroscopy.SpectrumType.ProcessedFluidSpecimen:
                    if (spectrum.SpectrumType == Hiperscan.Spectroscopy.SpectrumType.ProcessedFluidSpecimen)
                        tmp = new CSVFileInformations(file, spectrum);
                    break;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("CSVFileFilter: {0}", ex.Message);
            }
            return tmp;
        }
    }
    
    public class CSVFileInformationsList : List<CSVFileInformations>{}
}


