using System;
using Gtk;
using Hiperscan.GtkExtensions.Compiler;
using System.Drawing;
using Mono.Unix;
using System.Reflection;


namespace Hiperscan.Extensions.QualityAssurance
{
    public enum ShortOSVersion
    {
        Windows,
        Linux,
    }
    
    public enum TrafficLightStates
    {
        None,
        NoSpectrumLoaded,
        NoApplicationAvailable,
        CannotOpenSpectrum,
        CannotEvaluateSpectrum,
        CannotOpenApplication,
        RedEvaluatedSpectrum,
        YellowEvaluatedSpectrum,
        GreenEvaluatedSpectrum,
    }
    
    public class TrafficLightData
    {
        public string SpectrumFileName            {get;set;}
        public string ApplicationName             {get;set;}
        public string ApplicationDescription     {get;set;}
        public bool LightWithAnimation             {get;set;}
        public TrafficLightStates TrafficState     {get;set;}    
        public bool NewestSpectrum                 {get;set;}
    }
    
    public class TrafficLightArea : Gtk.DrawingArea
    {
        private MeasureTextDelegate MeasureText{get;set;}
        
        public TrafficLightArea ()
        {
            switch(Environment.OSVersion.Platform)
            {
            case PlatformID.Win32NT:
            case PlatformID.Win32S:
            case PlatformID.Win32Windows:
            case PlatformID.WinCE:
            case PlatformID.Xbox:
                this.current_os_version = ShortOSVersion.Windows;
                    this.MeasureText = SystemDrawingGraphicCompiler.MeasureGraphicsText;
                break;
                
            default:
                this.current_os_version = ShortOSVersion.Linux;
                    this.MeasureText = MonoCairoGraphicCompiler.MeasureGraphicsText;
                break;
            }
            this.BorderColor = new GraphicsColor(1,1,1,1);                        
            this.InitVisualTreeRoot();    
            this.InitError();
            this.InitStreetLight();
        }
        
        

        
        protected override bool OnExposeEvent (Gdk.EventExpose evnt)
        {
            GenericGraphicCompiler translator = null;
            if (this.CurrentOSVersion == ShortOSVersion.Windows)
                translator = new SystemDrawingGraphicCompiler(Gtk.DotNet.Graphics.FromDrawable(evnt.Window, true));
            else
                translator = new MonoCairoGraphicCompiler(Gdk.CairoHelper.Create(evnt.Window));
            
            double border_width = 20;
            
            double scale_x = this.Width / this.ReferenceSize.Width;
            double scale_y = this.Height / this.ReferenceSize.Height;
            
            double scale = scale_x < scale_y ? scale_x : scale_y;
            
            double translate_x = (this.Width - this.ReferenceSize.Width * scale) / 2.0;
            double translate_y = (this.Height - this.ReferenceSize.Height * scale) / 2.0;
            
            this.LeftBorder.Coordinates.X = 0;
            this.LeftBorder.Coordinates.Y = 0;
            this.LeftBorder.Coordinates.Width = border_width;
            this.LeftBorder.Coordinates.Height = this.Height;
            
            this.TopBorder.Coordinates.X = 0;
            this.TopBorder.Coordinates.Y = 0;
            this.TopBorder.Coordinates.Width = this.Width;
            this.TopBorder.Coordinates.Height = border_width;
            
            this.BottomBorder.Coordinates.X = 0;
            this.BottomBorder.Coordinates.Y = this.Height - border_width;
            this.BottomBorder.Coordinates.Width = this.Width;
            this.BottomBorder.Coordinates.Height = border_width;
            
            this.RightBorder.Coordinates.X = this.Width - border_width;
            this.RightBorder.Coordinates.Y = 0;
            this.RightBorder.Coordinates.Width = border_width;
            this.RightBorder.Coordinates.Height = this.Height;

            this.Background.Coordinates.Width = this.Width;
            this.Background.Coordinates.Height = this.Height;
            
            this.TranslateDrawingGroup.XOffset = translate_x;
            this.TranslateDrawingGroup.YOffset = translate_y;            
            this.ScaleDrawingGroup.XScale = scale;
            this.ScaleDrawingGroup.YScale = scale;
            
            GraphicsCompiler graphic_compiler = new GraphicsCompiler(this.VisualTreeRoot, translator);
            graphic_compiler.TraverseVisualTree();    
            translator.Dispose();
            
            return true;
        }
        
        private GraphicsGroup VisualTreeRoot         {get;set;}
        private GraphicsShapeRectangle Background     {get;set;}
        private GraphicsShapeRectangle LeftBorder     {get;set;}
        private GraphicsShapeRectangle TopBorder     {get;set;}
        private GraphicsShapeRectangle BottomBorder {get;set;}
        private GraphicsShapeRectangle RightBorder     {get;set;}
        private GraphicsColor BorderColor             {get;set;}
        
        private GraphicsGroup DrawingGroup                 {get;set;}    
        private GraphicsTranslate TranslateDrawingGroup {get;set;}
        private GraphicsScale ScaleDrawingGroup            {get;set;}
        
        private GraphicsGroup ErrorRoot                                {get;set;}
        private GraphicsText ErrorLine1                                {get;set;}
        private GraphicsText ErrorLine2                                {get;set;}
        private GraphicsGroup TrafficLightRoot                         {get;set;}    
        private GraphicsText SpectrumFileNameText                     {get;set;}
        private GraphicsText ApplicationNameText                     {get;set;}
        private GraphicsTextParagraph ApplicationDescriptionText    {get;set;}        
        private GraphicsShapeRectangle Light                        {get;set;}
        
        private void InitVisualTreeRoot()
        {
            this.VisualTreeRoot = new  GraphicsGroup();
            
            this.Background = new GraphicsShapeRectangle();
            this.Background.Coordinates = new GraphicsRectangle();
            this.Background.Brush = new GraphicsSolidColorBrush(GraphicsColors.White);
            this.Background.IsClosed = true;
            this.VisualTreeRoot.Children.Add(this.Background);
            
            GraphicsColorStop stop1 = new GraphicsColorStop();
            stop1.Offset = 0;
            stop1.Color = this.BorderColor;
            
            GraphicsColorStop stop2 = new GraphicsColorStop();
            stop2.Offset = 1;
            stop2.Color = new GraphicsColor(1, 1, 1, 0);
            
            this.LeftBorder = new  GraphicsShapeRectangle();
            GraphicsLinearGradientenBrush left_brush = new GraphicsLinearGradientenBrush();
            left_brush.Start = new GraphicsPoint(0, 1);
            left_brush.End = new GraphicsPoint(1, 1);
            left_brush.ColorStop1 = stop1;
            left_brush.ColorStop2 = stop2;    
            this.LeftBorder.Brush = left_brush;
            
            this.TopBorder = new  GraphicsShapeRectangle();
            GraphicsLinearGradientenBrush top_brush = new GraphicsLinearGradientenBrush();
            top_brush.Start = new GraphicsPoint(0, 0);
            top_brush.End = new GraphicsPoint(0, 1);
            top_brush.ColorStop1 = stop1;
            top_brush.ColorStop2 = stop2;        
            this.TopBorder.Brush = top_brush;
            
            this.BottomBorder = new  GraphicsShapeRectangle();
            GraphicsLinearGradientenBrush bottom_brush = new GraphicsLinearGradientenBrush();
            bottom_brush.Start = new GraphicsPoint(0, 1);
            bottom_brush.End = new GraphicsPoint(0, 0);
            bottom_brush.ColorStop1 = stop1;
            bottom_brush.ColorStop2 = stop2;            
            this.BottomBorder.Brush = bottom_brush;
            
            this.RightBorder = new  GraphicsShapeRectangle();
            GraphicsLinearGradientenBrush right_brush = new GraphicsLinearGradientenBrush();
            right_brush.Start = new GraphicsPoint(1, 1);
            right_brush.End = new GraphicsPoint(0, 1);
            right_brush.ColorStop1 = stop1;
            right_brush.ColorStop2 = stop2;    
            this.RightBorder.Brush = right_brush;
                        
            this.VisualTreeRoot.Children.Add(this.LeftBorder);
            this.VisualTreeRoot.Children.Add(this.TopBorder);
            this.VisualTreeRoot.Children.Add(this.BottomBorder);
            this.VisualTreeRoot.Children.Add(this.RightBorder);
            
            
            this.DrawingGroup = new GraphicsGroup();
            this.TranslateDrawingGroup = new GraphicsTranslate();
            this.ScaleDrawingGroup = new GraphicsScale();
            this.DrawingGroup.TransformGroup.Add(this.TranslateDrawingGroup);
            this.DrawingGroup.TransformGroup.Add(this.ScaleDrawingGroup);
            this.VisualTreeRoot.Children.Add(this.DrawingGroup);
            
        }
        
        private void InitError()
        {
            this.ErrorRoot = new GraphicsGroup();
            
            double radius = 0.4 * this.reference_size.Width;
            double center_x = this.ReferenceSize.Width / 2;
            double center_y = this.ReferenceSize.Height / 2;            
            GraphicsPoint[] points = new GraphicsPoint[8];
            for (int i = 0; i < 8; i++)
            {
                double angle = Math.PI / 4 * i + Math.PI / 8;
                double x = radius * Math.Cos(angle) + center_x;
                double y = radius * Math.Sin(angle) + center_y;
                points[i] = new GraphicsPoint(x,y);
            }
            
            GraphicsShapePolygone polygone = new GraphicsShapePolygone(points);
            polygone.IsClosed = true;
            polygone.Brush = new GraphicsSolidColorBrush(GraphicsColors.Red);
            polygone.Pen = new GraphicsPen();
            polygone.Pen.Color = new GraphicsColor(1, 0.5, 0.5);
            polygone.Pen.LineWidth = 40;
            this.ErrorRoot.Children.Add(polygone);
            
            GraphicsFont font = new GraphicsFont();
            font.FontSize = 130;
            font.FontWeight = GraphicsFontWeights.Bold;
            
            this.ErrorLine1 = new GraphicsText();
            this.ErrorLine1.Brush = new GraphicsSolidColorBrush(GraphicsColors.White);
            this.ErrorLine1.Font = font;
            this.ErrorRoot.Children.Add(this.ErrorLine1);
            
            this.ErrorLine2 = new GraphicsText();
            this.ErrorLine2.Brush = new GraphicsSolidColorBrush(GraphicsColors.White);
            this.ErrorLine2.Font = font;
            this.ErrorRoot.Children.Add(this.ErrorLine2);
        }
        
        private void CalculateErrorText(string line1, string line2)
        {
            double radius = 0.4 * this.reference_size.Width;
            this.ErrorLine1.Font.FontSize = 1330;
            
            this.ErrorLine1.Text = line1;
            this.ErrorLine2.Text = line2;
            GraphicsSize size1 = this.MeasureText(this.ErrorLine1);
            GraphicsSize size2 = this.MeasureText(this.ErrorLine2);
            
            GraphicsSize size = null;
            GraphicsText text_shape = null;
            if (size1.Width > size2.Width)
            {
                size = size1;
                text_shape = this.ErrorLine1;
            }
            else
            {
                size = size2;
                text_shape = this.ErrorLine2;
            }
                
            for(;;)
            {
                if (size.Width <= 0.7 * this.ReferenceSize.Width)
                    break;
                
                text_shape.Font.FontSize -= 5;
                size = this.MeasureText(text_shape);    
            }
            
            size1 = this.MeasureText(this.ErrorLine1);
            size2 = this.MeasureText(this.ErrorLine2);
            
            this.ErrorLine1.Position.X = (this.reference_size.Width - size1.Width) / 2;
            this.ErrorLine1.Position.Y = radius * Math.Sin(-Math.PI / 8) + this.ReferenceSize.Height / 2;
            
            this.ErrorLine2.Position.X = (this.reference_size.Width - size2.Width) / 2;
            this.ErrorLine2.Position.Y = radius * Math.Sin(Math.PI / 8) + this.ReferenceSize.Height / 2 - size2.Height;            
        }
        
        private void InitStreetLight()
        {
            this.TrafficLightRoot = new GraphicsGroup();
            
            GraphicsFont bold_font = new GraphicsFont();
            bold_font.FontWeight = GraphicsFontWeights.Bold;
            bold_font.FontSize = 40;
            
            GraphicsText text1 = new GraphicsText();
            text1.Font = bold_font;
            text1.Text = Catalog.GetString("Spectrum file:");
            text1.Position = new GraphicsPoint(30, 100);        
            this.TrafficLightRoot.Children.Add(text1);
            
            GraphicsText text2 = new GraphicsText();
            text2.Font = bold_font;
            text2.Text = Catalog.GetString("Application name:");
            text2.Position = new GraphicsPoint(30, 300);
            this.TrafficLightRoot.Children.Add(text2);
            
            GraphicsText text3 = new GraphicsText();
            text3.Font = bold_font;
            text3.Text = Catalog.GetString("Application description:");
            text3.Position = new GraphicsPoint(30, 500);
            this.TrafficLightRoot.Children.Add(text3);
                                           
            GraphicsFont normal_font = new GraphicsFont();
            normal_font.FontWeight = GraphicsFontWeights.Normal;
            normal_font.FontSize = 40;
            
            this.SpectrumFileNameText = new GraphicsText();
            this.SpectrumFileNameText.Font = normal_font;
            this.SpectrumFileNameText.Position = new GraphicsPoint(60, 150);
            this.SpectrumFileNameText.MaxWidth = 530;
            this.SpectrumFileNameText.Text = String.Empty;
            this.TrafficLightRoot.Children.Add(this.SpectrumFileNameText);
                        
            this.ApplicationNameText = new GraphicsText();
            this.ApplicationNameText.Font = normal_font;
            this.ApplicationNameText.Position = new GraphicsPoint(60, 350);
            this.ApplicationNameText.MaxWidth = 530;
            this.ApplicationNameText.Text = String.Empty;
            this.TrafficLightRoot.Children.Add(this.ApplicationNameText);
            
            this.ApplicationDescriptionText = new GraphicsTextParagraph();
            this.ApplicationDescriptionText.Font = normal_font;
            this.ApplicationDescriptionText.Coordinates = new GraphicsRectangle(60, 550, 530, 400);
            this.ApplicationDescriptionText.Text = String.Empty;
            this.TrafficLightRoot.Children.Add(this.ApplicationDescriptionText);
            
            
            GraphicsImage street_light_image = new GraphicsImage();
            street_light_image.Coordinates = new GraphicsRectangle();
            string file = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);            
            file = System.IO.Path.Combine(file, "Resources");
            file = System.IO.Path.Combine(file, "Images");
            file = System.IO.Path.Combine(file, "street_light.png");
            street_light_image.FileName = file;
            street_light_image.Coordinates.Width = street_light_image.ImageWidth / 1.4;
            street_light_image.Coordinates.Height =street_light_image.ImageHeight / 1.4;
            street_light_image.Coordinates.Y = (this.ReferenceSize.Height - street_light_image.Coordinates.Height) / 2;
            street_light_image.Coordinates.X = this.ReferenceSize.Width - street_light_image.Coordinates.Width - 30;
            this.TrafficLightRoot.Children.Add(street_light_image);
            
            this.Light = new GraphicsShapeRectangle();
            this.Light.Coordinates.X = 635;
            this.Light.Coordinates.Width = 300;
            this.Light.Coordinates.Height = 300;
            GraphicsRadialGradientenBrush rgb = new GraphicsRadialGradientenBrush();
            rgb.ColorStop1.Color = new GraphicsColor(1, 0, 0, 0);
            rgb.ColorStop2.Offset = 1;
            rgb.ColorStop2.Color = new GraphicsColor(0, 0, 0, 0);
            this.Light.Brush = rgb;
            this.TrafficLightRoot.Children.Add(this.Light);            
        }
        
        
        private double Width 
        {
            get
            {
                return this.Allocation.Width;
            }
        }
        
        private double Height
        {
            get
            {
                return this.Allocation.Height;
            }
        }
        
        private ShortOSVersion current_os_version = ShortOSVersion.Windows;
        private ShortOSVersion CurrentOSVersion
        {
            get
            {
                return this.current_os_version;
            }
        }
            
        
        private GraphicsSize reference_size = new GraphicsSize(1000, 1000);
        private GraphicsSize ReferenceSize
        {
            get 
            { 
                return this.reference_size;
            }
        }
        
        public void Update(TrafficLightData data)
        {
            this.DrawingGroup.Children.Clear();
            
            string cannot_evaluate_spectrum = Catalog.GetString("CANNOT EVALUATE\nSPECTRUM");
            string cannot_open_spectrum = Catalog.GetString("CANNOT OPEN\nSPECTRUM");
            string cannot_open_application = Catalog.GetString("CANNOT OPEN\nAPPLICATION");
            string no_application_available = Catalog.GetString("NO APPLICATION\nAVAILABLE");
            string no_spectrum_available = Catalog.GetString("NO SPECTRUM\nAVAILABLE");
            
            switch(data.TrafficState)
            {
            case TrafficLightStates.NoSpectrumLoaded:
            case TrafficLightStates.NoApplicationAvailable:
            case TrafficLightStates.CannotOpenApplication:
            case TrafficLightStates.CannotOpenSpectrum:
            case TrafficLightStates.CannotEvaluateSpectrum:
                this.BorderColor.R = 0.8;
                this.BorderColor.G = 0;
                this.BorderColor.B = 0;
                string[] error_data = new string[2]{String.Empty, String.Empty};
                switch( data.TrafficState)
                {
                case TrafficLightStates.CannotEvaluateSpectrum:
                    error_data = cannot_evaluate_spectrum.Split(new char[]{'\n'}, StringSplitOptions.RemoveEmptyEntries);
                    break;
                    
                case TrafficLightStates.CannotOpenApplication:
                    error_data = cannot_open_application.Split(new char[]{'\n'}, StringSplitOptions.RemoveEmptyEntries);
                    break;
                    
                case TrafficLightStates.CannotOpenSpectrum:
                    error_data = cannot_open_spectrum.Split(new char[]{'\n'}, StringSplitOptions.RemoveEmptyEntries);
                    break;
                    
                case TrafficLightStates.NoApplicationAvailable:
                    error_data = no_application_available.Split(new char[]{'\n'}, StringSplitOptions.RemoveEmptyEntries);
                    break;
                    
                case TrafficLightStates.NoSpectrumLoaded:
                    error_data = no_spectrum_available.Split(new char[]{'\n'}, StringSplitOptions.RemoveEmptyEntries);
                    break;
                }
                this.CalculateErrorText(error_data[0], error_data[1]);
                this.DrawingGroup.Children.Add(this.ErrorRoot);
                break;
                

                
            case TrafficLightStates.GreenEvaluatedSpectrum:
            case TrafficLightStates.YellowEvaluatedSpectrum:
            case TrafficLightStates.RedEvaluatedSpectrum:
                if (data.NewestSpectrum == true)
                {
                    this.BorderColor.R = 0;
                    this.BorderColor.G = 0.6;
                    this.BorderColor.B = 0;
                }
                else
                {
                    this.BorderColor.R = 0.6;
                    this.BorderColor.G = 0.6;
                    this.BorderColor.B = 0;
                }
                
                this.SpectrumFileNameText.Text             = data.SpectrumFileName;
                this.ApplicationNameText.Text             = data.ApplicationName;
                this.ApplicationDescriptionText.Text    = data.ApplicationDescription;
                
                GraphicsRadialGradientenBrush rgb = this.Light.Brush as GraphicsRadialGradientenBrush;
                rgb.ColorStop1.Offset = 0;
                rgb.ColorStop2.Offset = 0;
                
                switch(data.TrafficState)
                {
                case TrafficLightStates.GreenEvaluatedSpectrum:
                    this.Light.Coordinates.Y = 595;
                    rgb.ColorStop1.Color = new GraphicsColor(0,0.7,0,0.9);
                    break;
                    
                case TrafficLightStates.YellowEvaluatedSpectrum:
                    this.Light.Coordinates.Y = 355;
                    rgb.ColorStop1.Color = new GraphicsColor(0.8,0.8,0, 0.9);
                    break;
                    
                case TrafficLightStates.RedEvaluatedSpectrum:
                    rgb.ColorStop1.Color = new GraphicsColor(0.8,0,0, 0.8);
                    this.Light.Coordinates.Y = 115;
                    break;
                }
                this.DrawingGroup.Children.Add(this.TrafficLightRoot);
                this.DrawLightAnimated(data.LightWithAnimation, rgb);
                break;
    
            default:
                this.BorderColor.R = 1;
                this.BorderColor.G = 1;
                this.BorderColor.B = 1;
                break;
            }
            this.QueueDraw();
        }
        
        private void DrawLightAnimated(bool with_animation, GraphicsRadialGradientenBrush rgb)
        {
            if (with_animation == false)
            {
                rgb.ColorStop1.Offset = 0.7;
                rgb.ColorStop2.Offset = 1;
                return;
            }
            rgb.ColorStop1.Offset = 0;
            rgb.ColorStop2.Offset = 0;
            
            uint steps = 15;
            uint frame_time = 200 / steps;
            
            double step_offset1 = 0.7 / steps;
            double step_offset2 = 1.0 /steps;
            
            GLib.Timeout.Add(frame_time, 
                delegate()
                {
                    this.QueueDraw();
                    rgb.ColorStop1.Offset += step_offset1;
                    rgb.ColorStop2.Offset += step_offset2;
                                return rgb.ColorStop2.Offset < 0.99;
                });
        }
    }
}

