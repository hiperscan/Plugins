using System;
using System.Configuration;


namespace Hiperscan.Extensions.QualityAssurance
{   
    public enum ConfigurationKind
    {
        Simple,
        Advanced,
    }
    
    public class WorkingDirectories : ConfigurationElement
    {
        [ConfigurationProperty("ApplicationDirectory")]
        public string ApplicationDirectory
        {
            get { return (string)this["ApplicationDirectory"];     }
            set { this["ApplicationDirectory"] = value;          }
        }
        
        [ConfigurationProperty("SolidSpectraDirectory")]
        public string SolidSpectraDirectory
        {
            get { return (string)this["SolidSpectraDirectory"]; }
            set { this["SolidSpectraDirectory"] = value;          }
        }
        
        [ConfigurationProperty("FluidSpectraDirectory")]
        public string FluidSpectraDirectory
        {
            get { return (string)this["FluidSpectraDirectory"]; }
            set { this["FluidSpectraDirectory"] = value;          }
        }
        
        [ConfigurationProperty("AllSpectraDirectory")]
        public string AllSpectraDirectory
        {
            get { return (string)this["AllSpectraDirectory"];     }
            set { this["AllSpectraDirectory"] = value;          }
        }
        
        [ConfigurationProperty("ConfigurationKind")]
        public ConfigurationKind ConfigurationKind
        {
            get { return (ConfigurationKind)this["ConfigurationKind"];    }
            set { this["ConfigurationKind"] = value;                      }
        }
        
        
        [ConfigurationProperty("SolidSpectraDirectoryActivated")]
        public bool SolidSpectraDirectoryActivated
        {
            get { return (bool)this["SolidSpectraDirectoryActivated"];     }
            set { this["SolidSpectraDirectoryActivated"] = value;          }
        }
        
        [ConfigurationProperty("FluidSpectraDirectoryActivated")]
        public bool FluidSpectraDirectoryActivated
        {
            get { return (bool)this["FluidSpectraDirectoryActivated"];     }
            set { this["FluidSpectraDirectoryActivated"] = value;          }
        }
    }
    
    public class QsGui : ConfigurationElement
    {
    }
    
    public class QualityAssuranceSettings : ConfigurationSection
    {
        [ConfigurationProperty("Directories")]
        public  WorkingDirectories Directories
        {
            get { return (WorkingDirectories)this["Directories"]; }
            set { this["Directories"] = value;          }
        }
    }
    
    public static class Settings
    {    

        public static void Init()
        {
            Plugin.Host.InitConfigurationSection(Plugin.PluginGUID, new QualityAssuranceSettings());            
        }
        
        
        public static QualityAssuranceSettings Current
        {
            get { return Plugin.Host.GetConfigurationSection(Plugin.PluginGUID) as QualityAssuranceSettings; }
        }
    }
}
