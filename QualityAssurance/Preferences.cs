using System;
using System.IO;
using Mono.Unix;

using Gtk;
using Glade;

namespace Hiperscan.Extensions.QualityAssurance
{        
    public class Preferences
    {
        [Widget] Gtk.Window preferences_window                    = null;
        
        [Widget] Gtk.FileChooserButton application_filechooser     = null;
        [Widget] Gtk.FileChooserButton solid_filechooser         = null;
        [Widget] Gtk.FileChooserButton fluid_filechooser         = null;    
        [Widget] Gtk.FileChooserButton all_filechooser             = null;    
        
        [Widget] Gtk.CheckButton solid_checkbutton                = null;
        [Widget] Gtk.CheckButton fluid_checkbutton                = null;
        
        [Widget] Gtk.Button save_button                            = null;
        [Widget] Gtk.Button cancel_button                        = null;
        
        [Widget] Gtk.Label application_label                     = null;
        [Widget] Gtk.Label all_spectra_label                     = null;
        [Widget] Gtk.Label solid_spectra_label                     = null;
        [Widget] Gtk.Label fluid_spectra_label                     = null;
        
        [Widget] Gtk.Alignment simple_align                         = null;
        [Widget] Gtk.Alignment advanced_align                     = null;
        
        [Widget] Gtk.RadioButton simple_radiobutton             = null;
        [Widget] Gtk.RadioButton advanced_radiobutton             = null;
        
            
        public Preferences (QualityAssuranceApp parent_app)
        {
            this.ParentApp = parent_app;
            this.InitGUI();
        }
        
        private void InitGUI()
        {            
            Glade.XML gui;
            gui = new Glade.XML(null, "QualityAssurance.Resources.Glade.QualityAssuranceUI.glade", "preferences_window", null);
            gui.Autoconnect(this);
            
            this.preferences_window.Title         = Catalog.GetString("Preferences");
            this.preferences_window.IconList     = Plugin.Icons;
            
            this.application_filechooser.SetCurrentFolder(Settings.Current.Directories.ApplicationDirectory);
            this.solid_filechooser.SetCurrentFolder(Settings.Current.Directories.SolidSpectraDirectory);
            this.fluid_filechooser.SetCurrentFolder(Settings.Current.Directories.FluidSpectraDirectory);
            this.all_filechooser.SetCurrentFolder(Settings.Current.Directories.AllSpectraDirectory);
            this.solid_checkbutton.Active    = Settings.Current.Directories.SolidSpectraDirectoryActivated;
            this.fluid_checkbutton.Active    = Settings.Current.Directories.FluidSpectraDirectoryActivated;
            
            this.advanced_radiobutton.Group = this.simple_radiobutton.Group;
            
            if (Settings.Current.Directories.ConfigurationKind == ConfigurationKind.Simple)
            {
                this.simple_radiobutton.Active         = true;
                this.simple_align.Sensitive         = true;
                this.advanced_align.Sensitive         = false;
            }
            else
            {
                this.simple_align.Sensitive         = false;
                this.advanced_radiobutton.Active     = true;
                this.advanced_align.Sensitive         = true;
            }
            
            this.preferences_window.DeleteEvent += this.OnDeleteEvent;
            this.cancel_button.Clicked += this.OnCancelButtonClicked;
            this.save_button.Clicked += this.OnSaveButtonClicked;
            this.simple_radiobutton.Clicked += this.OnConfigurationKindChanged;
            this.preferences_window.Shown += this.OnPreferencesWindowShown;
            this.preferences_window.ShowAll();
        }
        
        private void OnPreferencesWindowShown(object sender, EventArgs e)
        {
            int left_alignment = Int32.MinValue;
            if (this.application_filechooser.Allocation.X > left_alignment)
                left_alignment = this.application_filechooser.Allocation.X;
            if (this.all_filechooser.Allocation.X > left_alignment)
                left_alignment = this.all_filechooser.Allocation.X;
            if (this.solid_filechooser.Allocation.X > left_alignment)
                left_alignment = this.solid_filechooser.Allocation.X;
            if (this.fluid_filechooser.Allocation.X > left_alignment)
                left_alignment = this.fluid_filechooser.Allocation.X;
            
            this.application_label.WidthRequest     = left_alignment - this.application_label.Allocation.X;
            this.all_spectra_label.WidthRequest     = left_alignment - this.all_spectra_label.Allocation.X;
            this.solid_spectra_label.WidthRequest     = left_alignment - this.solid_spectra_label.Allocation.X;
            this.fluid_spectra_label.WidthRequest     = left_alignment - this.fluid_spectra_label.Allocation.X;
            
            int width = Int32.MaxValue;
            if (this.application_filechooser.Allocation.Width < width)
                width = this.application_filechooser.Allocation.Width;
            if (this.all_filechooser.Allocation.Width < width)
                width = this.all_filechooser.Allocation.Width;
            if (this.solid_filechooser.Allocation.Width < width)
                width = this.solid_filechooser.Allocation.Width;
            if (this.fluid_filechooser.Allocation.Width < width)
                width = this.fluid_filechooser.Allocation.Width;
            
            this.application_filechooser.WidthRequest     = width;
            this.all_filechooser.WidthRequest             = width;
            this.solid_filechooser.WidthRequest         = width;
            this.fluid_filechooser.WidthRequest         = width;
        }
        
        private void OnDeleteEvent(object sender, DeleteEventArgs e)
        {        
            e.RetVal = true;
            this.OnCancelButtonClicked(null, null);            
        }
        
        private void OnConfigurationKindChanged(object sender, EventArgs e)
        {
            Gtk.RadioButton button = sender as RadioButton;
            bool simple_active = false;
            if (button == this.simple_radiobutton && button.Active == true)
                simple_active = true;
            

            if (simple_active == true)
            {
                this.simple_align.Sensitive         = true;
                this.advanced_align.Sensitive         = false;
            }
            else
            {
                this.simple_align.Sensitive         = false;
                this.advanced_align.Sensitive         = true;
            }
        }
        
        private void OnCancelButtonClicked(object sender, EventArgs e)
        {
            this.application_filechooser.SetFilename(Settings.Current.Directories.ApplicationDirectory);
            this.solid_filechooser.SetFilename(Settings.Current.Directories.SolidSpectraDirectory);
            this.fluid_filechooser.SetFilename(Settings.Current.Directories.FluidSpectraDirectory);
            this.all_filechooser.SetFilename(Settings.Current.Directories.AllSpectraDirectory);
            this.solid_checkbutton.Active    = Settings.Current.Directories.FluidSpectraDirectoryActivated;
            this.fluid_checkbutton.Active    = Settings.Current.Directories.FluidSpectraDirectoryActivated;
            if (Settings.Current.Directories.ConfigurationKind == ConfigurationKind.Simple)
                this.simple_radiobutton.Active = true;
            else
                this.advanced_radiobutton.Active = true;
            this.DestroyPreferences();            
        }
        
        private void OnSaveButtonClicked(object sender, EventArgs e)
        {
            Settings.Current.Directories.ApplicationDirectory             = this.application_filechooser.Filename;
            Settings.Current.Directories.SolidSpectraDirectory             = this.solid_filechooser.Filename;
            Settings.Current.Directories.FluidSpectraDirectory             = this.fluid_filechooser.Filename;
            Settings.Current.Directories.AllSpectraDirectory            = this.all_filechooser.Filename;
            Settings.Current.Directories.SolidSpectraDirectoryActivated = this.solid_checkbutton.Active;
            Settings.Current.Directories.FluidSpectraDirectoryActivated = this.fluid_checkbutton.Active;
            if (this.simple_radiobutton.Active == true)
                Settings.Current.Directories.ConfigurationKind = ConfigurationKind.Simple;
            else
                Settings.Current.Directories.ConfigurationKind = ConfigurationKind.Advanced;
            
            this.DestroyPreferences();
        }
        
        private void DestroyPreferences()
        {
            if (this.ParentApp.CheckForDirectoryErrors(this.preferences_window, null) == true)
            {
                this.preferences_window.Destroy();
                this.ParentApp.IsConfigWindowOpened = false;
            }
        }
                
        private QualityAssuranceApp ParentApp     {get;set;}

    }
}

