
using System;
using System.Reflection;

using Gtk;

using Hiperscan.Unix;


namespace Hiperscan.Extensions.Postprocessing
{

    public class Plugin : IPlugin
    {
        public static readonly string name        = Catalog.GetString("Postprocessing");
        public static readonly string version     = "1.0.0";
        public static readonly string vendor      = "Hiperscan GmbH";
        public static readonly string description = Catalog.GetString("Process acquired spectra");
        public static readonly string guid        = "b8855f90-8f48-11df-a4ee-0800200c9a66";
        
        internal static IHost host;

        public Plugin()
        {
            ToolButton b = new ToolButton(Stock.Convert);
            MenuInfo menu_info = new MenuInfo(Catalog.GetString("Postprocessing"));
            ToolbarInfo toolbar_info = new ToolbarInfo(b, 
                                                       Catalog.GetString("Postprocessing"), 
                                                       Catalog.GetString("Show postprocessing window"));

            menu_info.Invoked += (sender, e) => new PostprocessingDialog();
            this.MenuInfos = new MenuInfo[] { menu_info };

            toolbar_info.Invoked += (sender, e) => new PostprocessingDialog();
            this.ToolbarInfos = new ToolbarInfo[] { toolbar_info };
        }
        
        public void ReplaceGui()
        {
        }

        public void Initialize(IHost host)
        {
            Plugin.host = host;
            Settings.Init();
        }

        public MenuInfo[]    MenuInfos    { get; }
        public ToolbarInfo[] ToolbarInfos { get; }

        public bool Active { get; set; } = true;

        public PlotTypeInfo[] PlotTypeInfos => null;
        public FileTypeInfo[] FileTypeInfos => null;
        public string Name                  => Plugin.name;
        public string Version               => Plugin.version;
        public string Vendor                => Plugin.vendor;
        public Assembly Assembly            => Assembly.GetExecutingAssembly();
        public string Description           => Plugin.description;
        public Guid Guid                    => new Guid(Plugin.guid);
        public bool CanReplaceGui           => false;
    }
}