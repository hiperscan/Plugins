// PostprocessingDialog.cs created with MonoDevelop
// User: klose at 17:06 14.04.2009
// CVS release: $Id: PostprocessingDialog.cs,v 1.7 2011-06-14 12:34:22 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;

using Glade;

using Gtk;

using Hiperscan.NPlotEngine;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.Postprocessing
{

    public class PostprocessingDialog
    {
        [Widget] Gtk.Dialog postprocessing_dialog = null;
        
        [Widget] Gtk.ComboBox post_spectrum_combo       = null;
        [Widget] Gtk.ComboBox post_type_combo           = null;
        [Widget] Gtk.ComboBox post_output_culture_combo = null;
        
        [Widget] Gtk.FileChooserButton post_result_filechooser = null;
        [Widget] Gtk.Entry post_filename_entry = null;

        [Widget] Gtk.SpinButton post_window_size_spin  = null;
        [Widget] Gtk.SpinButton post_noise_limit_spin  = null;
        [Widget] Gtk.SpinButton post_cutoff_limit_spin = null;
        [Widget] Gtk.SpinButton post_epsilon_spin      = null;
        
        [Widget] Gtk.SpinButton post_lower_limit_spin = null;
        [Widget] Gtk.SpinButton post_upper_limit_spin = null;
        [Widget] Gtk.SpinButton post_max_count_spin   = null;
        
        [Widget] Gtk.Frame post_result_preview_frame = null;
        
        [Widget] Gtk.ToggleButton post_save_button = null;

        private readonly IPlotEngine plot;
        
        private ICurrentSpectra current_spectra;

        private Spectrum current_spectrum;
        private DataSet  current_extrema;
        
        private static ulong instances = 0;

        private int WindowSize    => (int)this.post_window_size_spin.Value;
        private double NoiseLimit => this.post_noise_limit_spin.Value;
        private double Cutoff     => this.post_cutoff_limit_spin.Value;
        private int Epsilon       => (int)this.post_epsilon_spin.Value;
        private int LowerLimit    => (int)this.post_lower_limit_spin.Value;
        private int UpperLimit    => (int)this.post_upper_limit_spin.Value;
        private int MaxCount      => (int)this.post_max_count_spin.Value;

        private readonly List<string> current_labels = new List<string>();
        
        private bool updating = false;

        
        public PostprocessingDialog()
        {
            Glade.XML gui;

            try
            {
                gui = new Glade.XML(null, "Postprocessing.Postprocessing.glade", "postprocessing_dialog", "Hiperscan");
            }
            catch
            {
                Console.WriteLine("UserInterface: No embedded resource for GUI found. Trying to read external Glade file."); 
                gui = new Glade.XML("Postprocessing.glade", "postprocessing_dialog", null);
            }
            gui.Autoconnect(this);

            this.postprocessing_dialog.DefaultWidth = 800;
            ++PostprocessingDialog.instances;

            this.plot = new NPlotEngine.NPlotEngine(PlotEngineType.Synchronous)
            {
                XLabel = Catalog.GetString("Wavelength / nm"),
                YLabel = Catalog.GetString("Intensity / a.u.")
            };
            this.post_result_preview_frame.Add(this.plot.GtkWidget);

            this.current_spectra = Plugin.host.CurrentSpectra;
            current_spectra.Changed += this.Update;
            this.Update();
            
            this.post_result_filechooser.FileSet += this.OnResultDirSet;

            if (string.IsNullOrEmpty(Settings.Current.SavePostprocessingResultFilePath) == false)
                this.post_result_filechooser.SetCurrentFolder(Settings.Current.SavePostprocessingResultFilePath);
            else
                this.post_result_filechooser.SetCurrentFolder(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            this.post_filename_entry.Text = Settings.Current.PostprocessingResultFile;
            
            FileFilter file_filter = new FileFilter();
            file_filter.AddPattern("*.csv");
            file_filter.Name = "Postprocessing result file";            

            this.post_result_filechooser.AddFilter(file_filter);
            
            this.post_output_culture_combo.Active = 1;
            this.postprocessing_dialog.ShowAll();
            this.postprocessing_dialog.DeleteEvent += this.OnDeleteEvent;        
            this.post_type_combo.Active = 0;
        }
        
        ~PostprocessingDialog()
        {
            ((IDisposable)this.plot).Dispose();
        }
        
        private void Update(ICurrentSpectra cs, bool ds_updated)
        {
            this.Update();
        }
        
        private static readonly double[] WAVELENGTH_REFERENCES_NIST10NM = 
        {
            1132.9,
            1261.8, 
            1320.2, 
            1534.6,
            1681.4,
            1757.6,
            1847.3
        };

        private static readonly double[] WAVELENGTH_REFERENCES_NIST15NM = 
        {
            1262.58,
            1321.16,
            1536.07,
            1681.33,
            1757.46,
            1847.25
        };

        private void Update()
        {
            if (this.updating)
                return;
            
            this.updating = true;
            if (this.postprocessing_dialog.GdkWindow != null)
                this.postprocessing_dialog.GdkWindow.Cursor = new Gdk.Cursor(Gdk.CursorType.Watch);

            Plugin.host.ProcessEvents();
            
            // update combo box
            bool changed = false;
            var last_labels = new List<string>(this.current_labels);
            lock (this.current_spectra)
            {
                foreach (Spectrum spectrum in this.current_spectra.Values)
                {
                    if (last_labels.Contains(spectrum.Label))
                        last_labels.Remove(spectrum.Label);
                    else
                        changed = true;
                }
                if (last_labels.Count > 0)
                    changed = true;
            }
            
            if (changed)
            {
                ListStore store = new ListStore(typeof(string));
                CellRendererText text = new CellRendererText();
                
                this.post_spectrum_combo.Clear();
                this.current_labels.Clear();
                this.post_spectrum_combo.PackStart(text, false);
                this.post_spectrum_combo.SetAttributes(text, "text", 0);
                this.post_spectrum_combo.Model = store;
                this.post_spectrum_combo.AppendText("None");
            }
            
            int spectrum_ix = 0;
            int ix = 0;
                
            lock (this.current_spectra)
            {
                foreach (Spectrum spectrum in this.current_spectra.Values)
                {
                    ++ix;

                    if (changed)
                    {
                        this.post_spectrum_combo.AppendText(spectrum.Label);
                        this.current_labels.Add(spectrum.Label);
                    }
                    
                    if (spectrum == this.current_spectrum)
                    {
                        spectrum_ix = ix;
                    }
                    else if (this.current_spectrum != null && 
                             spectrum.Id == this.current_spectrum.Id)
                    {
                        this.current_spectrum = spectrum;
                        spectrum_ix = ix;
                    }    
                }
                
                this.post_spectrum_combo.Active = spectrum_ix;
            }
            
            if (this.post_spectrum_combo.Active == 0)
            {
                this.current_spectrum = null;
                this.current_extrema  = null;
                this.plot.Clear();
                this.plot.ProcessChanges();
            }
            
            this.post_result_preview_frame.Sensitive = false;
            
            if (this.current_spectrum != null && (this.post_type_combo.Active !=0 || this.current_spectrum.HasAbsorbance))
            {
                DataSet absorbance;
                if (this.post_type_combo.Active == 0)
                    absorbance = new DataSet(this.current_spectrum.Absorbance);
                else
                    absorbance = new DataSet(this.current_spectrum.Intensity);
                
                absorbance.Id    = "2_current_spectrum";
                absorbance.Label = this.current_spectrum.Label;
                this.plot.AddDataSet(absorbance);
                this.plot.FitView();
                this.plot.ProcessChanges();
                    
                if (this.post_type_combo.Active == 0)
                {
                    try
                    {
                        DataSet.Extrema extrema = absorbance.FindExtremaSG(this.Epsilon, this.WindowSize, 
                                                                           this.NoiseLimit,
                                                                           this.Cutoff);
                        var x_max = new List<double>();
                        var y_max = new List<double>();
                        for (int jx=0; jx < WAVELENGTH_REFERENCES_NIST10NM.Length; ++jx)
                        {
                            double[] maxx = extrema.Maxima.FindNearest(WAVELENGTH_REFERENCES_NIST10NM[jx]);
                            x_max.Add(maxx[0]);
                            y_max.Add(maxx[1]);
                        }
                        this.current_extrema = new DataSet(x_max, y_max);
                        
                        List<int> indices;
                        
                        indices = this.current_extrema.Find('x', '<', this.LowerLimit);
                        this.current_extrema.RemoveAt(indices);
                        
                        indices = this.current_extrema.Find('x', '>', this.UpperLimit);
                        this.current_extrema.RemoveAt(indices);
                        
                        int diff = this.current_extrema.Count - this.MaxCount;
                        if (diff > 0)
                            this.current_extrema.RemoveRange(this.MaxCount, diff);
                        
                        
                        this.current_extrema.Id    = "3_current_extrema";
                        this.current_extrema.Label = $"{this.current_spectrum.Label} extrema";
                        this.current_extrema.Color = Color.Green;
                        this.plot.AddDataSet(this.current_extrema);
                        
                        this.post_result_preview_frame.Sensitive = true;
                        
                        if (this.post_save_button.Active)
                        {
                            string fname = this.post_result_filechooser.Filename + 
                                System.IO.Path.DirectorySeparatorChar + 
                                    this.post_filename_entry.Text;
                            this.AppendResult(fname, this.current_spectrum, this.current_extrema);
                        }
                    }
                    catch (Exception ex)
                    {
                        Plugin.host.ShowInfoMessage(this.postprocessing_dialog,
                                                    Catalog.GetString("Cannot calculate extrema: {0}"),
                                                    ex.Message);
                        Console.WriteLine(ex);
                        this.post_spectrum_combo.Active = 0;
                        this.current_extrema  = null;
                    }
                
                    try
                    {
                        if (this.current_spectrum != null && this.current_spectrum.HasAbsorbance)
                        {
                            IPlotEngine plot = this.plot;
                            
                            DataSet ds = this.current_spectrum.Absorbance;
                            
                            switch (ds.DataType)
                            {
                                
                            case DataType.Intensity:
                                ds.XLabel = Catalog.GetString("Wavelength / nm");
                                ds.YLabel = Catalog.GetString("Intensity / a.u.");
                                break;
                                
                            case DataType.Absorbance:
                                ds.XLabel = Catalog.GetString("Wavelength / nm");
                                ds.YLabel = Catalog.GetString("Absorbance");                    
                                break;
                                
                            case DataType.Reference:
                                ds.XLabel = Catalog.GetString("Wavelength / nm");
                                ds.YLabel = Catalog.GetString("Background intensity / a.u.");
                                break;
                                
                            case DataType.Preview:
                                ds.Label  = Catalog.GetString("Preview");
                                ds.XLabel = Catalog.GetString("Wavelength / nm");
                                ds.YLabel = Catalog.GetString("Intensity / a.u.");
                                break;
                                
                            default:
                                throw new Exception("Unknown DataType");
                                
                            }
                            
                            ds.Id = "2_current_spectrum";
                            ds.Label = this.current_spectrum.Label;
                            ds.Color = Color.Blue;
                            
                            plot.DataSets.Clear();
                            plot.AddDataSet(ds);
                            plot.XLabel = ds.XLabel;
                            plot.YLabel = ds.YLabel;
                            
                            plot.AddDataSet(this.current_extrema);

                            plot.FitView();
                            this.plot.ProcessChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Plugin.host.ShowInfoMessage(this.postprocessing_dialog,
                                                    Catalog.GetString("Cannot display extrema: {0}"),
                                                    ex.Message);
                        this.plot.Clear();
                        this.post_spectrum_combo.Active = 0;
                    }
                }
                else
                {
                    DataSet.FwhmResult fwhm = new DataSet.FwhmResult();
                    
                    try
                    {
                        fwhm = absorbance.FWHM();

                        if (this.post_save_button.Active)
                        {
                            string fname = Path.Combine(this.post_result_filechooser.Filename,
                                                        this.post_filename_entry.Text);
                            this.AppendResult(fname, this.current_spectrum, fwhm);
                        }
                    }
                    catch (Exception ex)
                    {
                        Plugin.host.ShowInfoMessage(this.postprocessing_dialog,
                                                    Catalog.GetString("Cannot calculate FHWM: {0}"),
                                                    ex);
                        Console.WriteLine(ex);
                        this.post_spectrum_combo.Active = 0;
                    }
                
                    try
                    {
                        if (this.current_spectrum != null)
                        {
                            IPlotEngine plot = this.plot;
                            
                            DataSet ds = this.current_spectrum.Intensity;
                            
                            switch (ds.DataType)
                            {
                                
                            case DataType.Intensity:
                                ds.XLabel = Catalog.GetString("Wavelength / nm");
                                ds.YLabel = Catalog.GetString("Intensity / a.u.");
                                break;
                                
                            case DataType.Absorbance:
                                ds.XLabel = Catalog.GetString("Wavelength / nm");
                                ds.YLabel = Catalog.GetString("Absorbance");                    
                                break;
                                
                            case DataType.Reference:
                                ds.XLabel = Catalog.GetString("Wavelength / nm");
                                ds.YLabel = Catalog.GetString("Reference intensity / a.u.");
                                break;
                                
                            case DataType.Preview:
                                ds.Label  = "Preview";
                                ds.XLabel = Catalog.GetString("Wavelength / nm");
                                ds.YLabel = Catalog.GetString("Intensity / a.u.");
                                break;
                                
                            default:
                                throw new Exception("Unknown DataType");
                                
                            }
                            
                            DataSet fwhm_ds = new DataSet(ds);
                            fwhm_ds.Label += $"FWHM={fwhm.FWHM.ToString("f2")}nm";
                            Console.WriteLine(fwhm_ds.Label);
                            plot.DataSets.Clear();
                            plot.AddDataSet(fwhm_ds);
                            plot.XLabel = ds.XLabel;
                            plot.YLabel = ds.YLabel;
                            plot.FitView();
                            plot.ProcessChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Plugin.host.ShowInfoMessage(this.postprocessing_dialog,
                                                    Catalog.GetString("Cannot display FWHM: {0}"),
                                                    ex.Message);
                        this.plot.Clear();
                        this.post_spectrum_combo.Active = 0;
                    }
                }
            }

            if (this.postprocessing_dialog.GdkWindow != null)
                this.postprocessing_dialog.GdkWindow.Cursor = new Gdk.Cursor(Gdk.CursorType.Arrow);
            
            this.updating = false;
        }

        private void AppendResult(string fname, Spectrum spectrum, DataSet.FwhmResult fwhm)
        {
            fname = fname.Replace("%serial%", spectrum.Serial);
            
            Console.WriteLine($"Appending postprocessing result to '{fname}'");
            
            CultureInfo culture;
            
            switch (this.post_output_culture_combo.Active)
            {

            case 0:
                culture = new CultureInfo("en-US");
                break;
                
            case 1:
                culture = new CultureInfo("de-DE");
                break;
                
            default:
                culture = CultureInfo.CurrentCulture;
                break;

            }
            
            using (StreamWriter stream = new StreamWriter(fname, true))
            {
                stream.Write("{1}{0}{2}{0}",
                             culture.TextInfo.ListSeparator,
                             spectrum.Timestamp.ToString(culture),
                             spectrum.Serial);
                
                stream.WriteLine("{1}{0}{2}{0}{3}",
                                 culture.TextInfo.ListSeparator,
                                 fwhm.XPeak.ToString(culture), 
                                 fwhm.YPeak.ToString(culture), 
                                 fwhm.FWHM.ToString(culture));
            }
        }
        
        private void AppendResult(string fname, Spectrum spectrum, DataSet extrema)
        {
            fname = fname.Replace("%serial%", spectrum.Serial);
            
            Console.WriteLine($"Appending postprocessing result to '{fname}'");
            
            CultureInfo culture;
            
            switch (this.post_output_culture_combo.Active)
            {

            case 0:
                culture = new CultureInfo("en-US");
                break;
                
            case 1:
                culture = new CultureInfo("de-DE");
                break;
                
            default:
                culture = CultureInfo.CurrentCulture;
                break;

            }

            using (StreamWriter stream = new StreamWriter(fname, true))
            {
                stream.Write("{1}{0}{2}{0}",
                             culture.TextInfo.ListSeparator,
                             spectrum.Timestamp.ToString(),
                             spectrum.Serial);

                foreach (double lambda in extrema.X)
                {
                    stream.Write(lambda.ToString("f16", culture) + culture.TextInfo.ListSeparator);
                }

                foreach (double intensity in extrema.Y)
                {
                    stream.Write(intensity.ToString("f16", culture) + culture.TextInfo.ListSeparator);
                }

                stream.Write("\n");
            }
        }
        
        internal void OnCloseButtonClicked(object o, System.EventArgs e)
        {
            --PostprocessingDialog.instances;
            this.current_spectra.Changed -= this.Update;
            this.postprocessing_dialog.Destroy();
        }
        
        private void OnDeleteEvent(object o, DeleteEventArgs e)
        {
            this.OnCloseButtonClicked(o, e);
            e.RetVal = true;
        }
        
        private void OnResultDirSet(object o, EventArgs e)
        {
            FileChooserButton chooser = o as FileChooserButton;
            
            if (chooser.Filename == null)
            {
                // canceled
                return;
            }

            Settings.Current.SavePostprocessingResultFilePath = chooser.Filename;
        }

        internal void OnPostTypeComboChanged(object o, EventArgs e)
        {
            this.Update();
        }
        
        internal void OnPostSpectrumComboChanged(object o, System.EventArgs e)
        {
            ComboBox c = o as ComboBox;
            
            if (c.Active == 0)
            {
                this.current_spectrum = null;
                this.Update();
                return;
            }
            
            string choice = c.ActiveText;

            if (this.current_spectra.ContainsLabel(choice))
                this.current_spectrum = this.current_spectra.GetByLabel(choice);
            else
                this.current_spectrum = null;

            this.Update();
        }

        internal void OnSaveButtonToggled(object o, EventArgs e)
        {
            ToggleButton b = o as ToggleButton;
            
            if (b.Active)
                this.Update();
        }
        
        internal void OnFitZoomButtonClicked(object o, System.EventArgs e)
        {
            this.plot.FitView();
            this.plot.ProcessChanges();
        }

        internal void OnPostWindowSizeSpinChanged(object o, EventArgs e)  => this.Update();
        internal void OnPostNoiseLimitSpinChanged(object o, EventArgs e)  => this.Update();
        internal void OnPostCutoffLimitSpinChanged(object o, EventArgs e) => this.Update();
        internal void OnPostEpsilonSpinChanged(object o, EventArgs e)     => this.Update();
        internal void OnPostLowerLimitSpinChanged(object o, EventArgs e)  => this.Update();
        internal void OnPostUpperLimitSpinChanged(object o, EventArgs e)  => this.Update();
        internal void OnPostMaxCountSpinChanged(object o, EventArgs e)    => this.Update();
    }
}