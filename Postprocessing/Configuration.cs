// Configuration.cs created with MonoDevelop
// User: klose at 15:23 14.07.2010
// CVS release: $Id: Configuration.cs,v 1.1 2010-07-14 15:27:20 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//

using System.Configuration;


namespace Hiperscan.Extensions.Postprocessing
{

    internal class PluginSettings : System.Configuration.ConfigurationSection
    {
        
        public PluginSettings() {}
        
        [ConfigurationProperty("SectionExists")]
        public bool SectionExists
        {
            get { return (bool)this["SectionExists"]; }
            set { this["SectionExists"] = value;      }
        }
        
        [ConfigurationProperty("SavePostprocessingResultFilePath")]
        public string SavePostprocessingResultFilePath
        {
            get { return (string)this["SavePostprocessingResultFilePath"]; }
            set { this["SavePostprocessingResultFilePath"] = value;        }
        }
        
        [ConfigurationProperty("PostprocessingResultFile")]
        public string PostprocessingResultFile
        {
            get
            {
                if ((string)this["PostprocessingResultFile"] == "")
                    return "postprocessing_results.csv";
                else
                    return (string)this["PostprocessingResultFile"];
            }
            set { this["PostprocessingResultFile"] = value; }
        }
    }

    internal static class Settings
    {
        public static void Init()
        {
            Plugin.host.InitConfigurationSection(Plugin.guid, new PluginSettings());
        }

        public static PluginSettings Current => Plugin.host.GetConfigurationSection(Plugin.guid) as PluginSettings;
    }
}