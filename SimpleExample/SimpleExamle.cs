// UI.Toolbar.cs created with MonoDevelop
// User: klose at 10:34 15.01.2009
// CVS release: $Id: UI.Toolbar.cs,v 1.75 2011-06-24 13:48:23 klose Exp $
//
//    SimpleExample: Demo application for implementation of plugin for QuickStep
//    Copyright (C) 2012  Thomas Klose, Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

// Needed references:
// - System 
//        for fundamental types of the runtime
//
// - Hiperscan.Extensions
//        contains interface descriptions for IHost and IPlugin
//
// - Hiperscan.SGS
//         contains the device class
//
// - Hiperscan.Spectroscopy
//        contains the spectrum class
//
// - gtk-sharp
//        needed for GUI applications
//
// - Mono.Posix
//        needed for "Internationalizations", that means the shown text
//        depends on the language settings on the running computer.

using System;

using Hiperscan.SGS;
using Hiperscan.SGS.Benchtop;
using Hiperscan.SGS.Common;
using Hiperscan.SGS.SerialInterface;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.SimpleExample
{
    // The class SimplePlugin implements the interface IPlugin.
    // This is needed because this interface descriptions are used from QuickStep in order to bind our plugin
    // in the QuickStep Gui
    public class SimplePlugin : Hiperscan.Extensions.IPlugin
    {
        #region user defined fields

        // menu_infos is an array, which contains the menu items of this plugin.
        // this will be shown on the QuickStep application under the menu item "Extensions" -> "Plugin name" -> "Item name"
        private MenuInfo[] menu_infos = null;

        // toolbar_infos is an array, which contains the toolbuttons or other widgets like checkbuttons.
        // spinbuttons or custom defined gui elements.
        // This toolbuttons will be shown on the QuickStep application in the toolbar.
        private ToolbarInfo[] toolbar_infos = null;

        // active is used for activation of this plugin. If the value is false, so mthe plugin will not be loaded
        // by starting of the QuickStep application. In other case the plugin will be loaded and the menu items and 
        // toolbar items will be integrated on the QuickStep sureface.
        private bool active = true;

        #endregion



        // This Constructor creates three menu items with simple functions and
        // two toolbar items. One of them is a normal toolbutton ant the other one is a checkbox, whichis displayed
        // on the QuickStep application in the toolbar.
        public SimplePlugin()
        {

            // The first menu item is named "Hello World".
            // If this menu item is clicked  a simple windowis shown, which displays "Hello world" and a button for 
            // closing the window.
            //
            // Note:     The string is called with the Catalog.GetString method. This allows us to internationalize our project.
            //            That means we can create a catalog (*.po file) , which contains the origin and translated strings 
            //            (here English and German)
            //            It is recommended to use a tool that can create and edit such po-files.
            MenuInfo menu_info1 = new MenuInfo(Catalog.GetString("Hello world"));
            menu_info1.Invoked += (sender, e) =>
            {
                new HelloWorld(this);
            };


            // The second menu item is named "Active, if at least a device is selected and device state is Idle".
            // This menu item can only be activated if at least one device is selected on the QuickStep surface and 
            // the device state is "Idle". This option is set with "NeedIdleDevice".
            // If this menu item is clicked  a simple window is shown, which displays two buttons. One button
            // acquires the spectra of all selected and "Idle" devices and displays these spectra in a diagram.
            // The other button closes the window.
            MenuInfo menu_info2 = new MenuInfo(Catalog.GetString("Active, if at least a device is selected and device state is \"Idle\""));
            menu_info2.NeedIdleDevice = true;
            menu_info2.Invoked += (sender, e) =>
            {
                new AtLeastOneDevice(this);
            };

            // The third menu item is named "Active, if excatly one device is selected and device state is Idle".
            // This menu item can only be activated if exactly one device is selected on the QuickStep surface and 
            // the device state is "Idle". This option is set with "NeedIdleDevice" in connection with "NeedSingleDevice".
            // If this menu item is clicked  a simple window is shown, which displays two buttons. One button
            // acquires the spectrum of the selected and "Idle" device and displays this spectrum in a diagram.
            // The other button closes the window.
            MenuInfo menu_info3 = new MenuInfo(Catalog.GetString("Active, if excatly one device is selected and device state is \"Idle\""));
            menu_info3.NeedSingleDevice = true;
            menu_info3.NeedIdleDevice = true;
            menu_info3.Invoked += (sender, e) =>
            {
                new ExactlyOneDevice(this);
            };


            // Here the array for the menu items will be created, which is dispayed in the menu und "Extensions -> plugin-name -> item-names 
            // of the QuickStep sureface.
            //
            //
            // Note: If the array has only one item, then wi find it in the QuickStep menu under "Extensions -> item-name" 
            menu_infos = new MenuInfo[]
            {
                menu_info1,
                menu_info2,
                menu_info3,
            };


            // The first toolbutton is an ordinary toolbutton, which by clicking the simple Hello-World application invokes.
            Gtk.ToolButton tool_button = new Gtk.ToolButton(Gtk.Stock.SelectColor);
            ToolbarInfo toolbar_info1 = new ToolbarInfo(tool_button,
                                                        Catalog.GetString("Example"),                       // Label text
                                                       Catalog.GetString("Start About Example Plugin"));    // Tooltip text
            toolbar_info1.Invoked += (sender, e) =>
            {
                new HelloWorld(this);
            };


            // The second toolbutton item is in this exampöe a checkbotton. 
            // That means we can also define other forms of toolbuttons.
            // If we define own toolbutton types, we can not use the standard event handler Invoked like at the first toolbutton.
            // We have to implement then the type specific events, in our case is that the click event.
            // This event only changes the label for the checkbutton between "On" and "Off"
            Gtk.CheckButton check_button = new Gtk.CheckButton(Catalog.GetString("Off"));
            check_button.Clicked += (sender, e) =>
            {
                if (check_button.Active == true)
                    check_button.Label = Catalog.GetString("On");
                else
                    check_button.Label = Catalog.GetString("Off");
            };
            Gtk.ToolItem tool_item = new Gtk.ToolItem();
            tool_item.Child = check_button;
            ToolbarInfo toolbar_info2 = new ToolbarInfo(tool_item,
                                                        Catalog.GetString("Example"),
                                                       Catalog.GetString("Example Demo Checkbutton"));



            // Here the array for the toolbar items is created, which is dispayed in the toolbar of 
            // the QuickStep sureface.
            this.toolbar_infos = new ToolbarInfo[] { toolbar_info1, toolbar_info2 };
        }





        #region IPlugin implementation

        // In this method initializings are set depending on the solution, we implement.
        // In our case we need only the Host property. Host is from type IHost.
        // IHost is a Interface which is implemented in QuickStep, so we have access to members of
        // our QuickStep application.
        public void Initialize(IHost host)
        {
            this.Host = host;
            this.IsGuiReplaced = false;
        }

        // If the QuickStep application is called with parameters "QickStep.GUI.exe -r Simple-Plugin" and this 
        // plugin is able to replace the GUI then this method is invoked during the start
        // of the QuickStep application.
        // The parameter "Simple-Example" is defined in the name property and the ability to replace
        // is defined in the CanReplaceGUI property of the SimplePlugin implementation.
        // 
        // Additional the IsGuiReplaced property is set in order to save that the application was started 
        // with replacing the QuickStep GUI. 
        // In our example the Hello-World project is shown instead of the QuickStep Gui.
        // The constructor of Hello-World gets a parameter a with reference of our SimplePlugin, so we can use the members
        // of this plugin, especally the members Host and IsGuiReplaced are interesting.
        public void ReplaceGui()
        {
            this.IsGuiReplaced = true;
            new HelloWorld(this);
        }

        // This property contains the menu items which are added to the menu of the QuickStep application.
        // If the array contains only one item, then one item is added under the menu "Extension". In the other
        // case one item with the name of this plugin isadded which contains the menu items of this aray as sub menu
        // items
        //
        // Example one:
        //     - only one menu item with a label "Hello World"
        //    - the menu under "Extensions" of QuickStep contains a menu item with the label "Hello World"
        //
        // Example two:
        //     - several menu items labels "label one", "label two", ...
        //    - the menu under "Extensions" of QuickStep contains a menu item with the label "SimplePlugin"
        //       which has the defined menu items "label one", "label two", ... as sub menus.
        public MenuInfo[] MenuInfos
        {
            get
            {
                return this.menu_infos;
            }
        }

        // This property contains the toolbar items which are added to the toolbar of the QuickStep application
        public ToolbarInfo[] ToolbarInfos
        {
            get
            {
                return this.toolbar_infos;
            }
        }

        //TODO
        public PlotTypeInfo[] PlotTypeInfos
        {
            get
            {
                return null;
            }
        }

        //TODO
        public FileTypeInfo[] FileTypeInfos
        {
            get
            {
                return null;
            }
        }

        // This property returns the name of the plugin. This name is used for creating menu items under "Extensions" 
        // of the QuickStep application (see description MenuInfos property) and for the parameterized call
        // "QuickStep.GUI.exe -r Simple-Plugin", if the QuickStep GUI should be replaced.
        public string Name
        {
            get
            {
                return Catalog.GetString("Simple-Plugin");
            }
        }

        // This property returns the version of this plugin.
        public string Version
        {
            get
            {
                return Catalog.GetString("0.0.0.0");
            }
        }

        // This property returns the vendor of this plugin.
        public string Vendor
        {
            get
            {
                return Catalog.GetString("Simple Ltd.");
            }
        }

        // This property returns the assembly of this plugin. The assembly contains information,
        // which are needed from the calling QuickStep application.
        // 
        // Note:     In order to call the plugin application, the executing directory of the QuickStep application must contain
        //            a directory named "Plugins". This directory must contain a directory with the name of the
        //            name of executing plugin assemply.
        //            In our case is the path: ./Plugins/SimpleExample/SimpleExample.dll
        //            !!! Not: ./Plugins/SimplePlugin/SimplePlugin.dll !!!
        public System.Reflection.Assembly Assembly
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly();
            }
        }

        // This property returns the description of this plugin.
        public string Description
        {
            get
            {
                return Catalog.GetString("This is a simple example.");
            }
        }

        // This property returns the Guid of this plugin. The Guid is supposed
        // a global unique identifaction key.
        public Guid Guid
        {
            get
            {
                return new Guid("7ca3df1d-942d-4817-84e6-fe76f204651e");
            }
        }

        // This property controls the loading of this plugin. If the value false, this plugin is not loaded
        // from QuickStep.
        public bool Active
        {
            get { return this.active; }
            set { this.active = value; }
        }

        // This property describes the ability to replace the QuickStep GUI through a user defined application GUI.
        // If the return value is true and the QuickStep invokation is parameterized like "QuickStep.GUI.exe -r Simple-Plugin"
        // then the ReplaceGUI method of this plugin is called.
        // In our case we start then our Hello World Gui instead of the QuickStep Gui.
        public bool CanReplaceGui
        {
            get
            {
                return true;
            }
        }
        #endregion


        #region user defined properties

        // This property stores an instance of the interface IHost. So we can use the methods of
        // the QickStep application, which this interface implements.
        public IHost Host { get; private set; }


        // this property stores if a pplication is started with replacing GUI.
        // (It is used for example in our Hello-World project.)
        public bool IsGuiReplaced { get; private set; }

        #endregion
    }

    public class MessageBox
    {
        public static void Show(string Msg)
        {
            Gtk.MessageDialog md = new Gtk.MessageDialog(null, Gtk.DialogFlags.Modal, Gtk.MessageType.Info, Gtk.ButtonsType.Ok, Msg);
            md.Run();
            md.Destroy();
        }
    }

    // This class implements our little Hello-World project.
    // We can start this application in two ways:
    //         - we replace the QuickStep Gui with the Gui of the our Hello-World application
    //         - We show both Gui's ( QuickStep and Hello-World)
    public class HelloWorld
    {
        public HelloWorld(SimplePlugin plugin)
        {
            // Saving a reference of our plugin, because we need some properties of
            // our plugin (Host and IsGuiReplaced)
            this.Plugin = plugin;

            foreach (IDevice dev in this.Plugin.Host.ActiveDevices.Idle)
            {
                // FinderTemperature
                Double dd = dev.ReadFinderTemperature();

                // FinderTemperatureGradient
                DeviceInfo devinf = dev.Info;

                // FinderStatus
                Spectrometer.State devst = dev.GetState();

                // FinderWheelposition
                FinderWheelPosition devwp = dev.FinderWheelPosition;

                // SerialNumber
                String ss = dev.ReadSerialNumber();

                //MessageBox.Show( "TDev= " + dd.ToString() );

                MessageBox.Show("SN= " + ss);
                // First we acquire the spectrum from the device.
                // Spectrum spectrum = dev.Single();

                // Second the spectrum is added to the diagram surface of the QuickStep application.
                // this.Plugin.Host.AddToCurrentPlot(spectrum);
            }

            Plugin.Host.ShowInfoMessage(Plugin.Host.MainWindow, true, "Hello, World!\n" +
                                        "<a href=\"http://www.hiperscan.com\" title=\"Hiperscan Homepage\">Click Me!</a>");

            if (true)
            {
                // Creating a label with internationalized text (Catalog.GetString - method)
                Gtk.Label label = new Gtk.Label(Catalog.GetString("My first GTK# application.  "));

                // Creating a button for closing this application with
                // the corresponding click event
                Gtk.Button button = new Gtk.Button();
                button.Label = Catalog.GetString(Catalog.GetString("Close"));
                button.Clicked += this.OnDelete;

                // Now we need a container for our two created widgets (label + button).
                // This container type is a horizontal box, that means the widgets (buttons) are placed
                // in line horizontally.
                Gtk.HBox hbox = new Gtk.HBox();
                hbox.PackStart(label);
                hbox.PackStart(button);

                // Now we create the displaying window for our application.
                // The window gets some properties set like border width.
                // A delete event for closing this application is added.
                // Finally tthe created widgtes are conntected with the window and shown.
                this.Window = new Gtk.Window(Catalog.GetString("Hello World"));
                this.Window.BorderWidth = 20;
                this.Window.WindowPosition = Gtk.WindowPosition.Center;
                this.Window.DeleteEvent += this.OnDelete;
                this.Window.Child = hbox;
                this.Window.ShowAll();
            }
        }

        // This method handles the closing event for this application.
        // As we said, there are two ways to start the Hello-World project:
        //        - with replacing the QuickStep Gui
        //         - without replacing  the QuickStep Gui
        //
        // If the QuickStep Gui is replaced, so both applications - QuickStep and Hello World -
        // are closed. In other case, only our Hello-World application is closed
        private void OnDelete(object sender, EventArgs e)
        {
            if (this.Plugin.IsGuiReplaced == true)
                this.Plugin.Host.Quit();
            else
                this.Window.Destroy();
        }

        // This property saves a reference to our Window.
        Gtk.Window Window { get; set; }

        // This property saves a reference to our calling plugin.
        SimplePlugin Plugin { get; set; }
    }



    // This class implements a little application for aquiring spectra and showing them
    // in the diagram of the QuickStep application.
    // This application is started by clicking on the menu item  'Active, if at least a device is selected and device state is "Idle"'.
    // Then appears a window with two buttons, one for acquiring the spectra and one for closing the application.
    public class AtLeastOneDevice
    {
        public AtLeastOneDevice(SimplePlugin plugin)
        {
            // Saving a reference of our plugin, because we need some properties of
            // our plugin (Host for acquiring the spectra)
            this.Plugin = plugin;

            // Creating the first button for acquiring the spectra with 
            // the corresponding click event
            Gtk.Button button1 = new Gtk.Button();
            button1.Label = Catalog.GetString(Catalog.GetString("Acquire spectra"));
            button1.Clicked += this.OnAcquireSpectraClicked;

            // Creating the second button for closing this application with
            // the corresponding click event
            Gtk.Button button2 = new Gtk.Button();
            button2.Label = Catalog.GetString(Catalog.GetString("Close"));
            button2.Clicked += this.OnDelete;

            // Now we need a container for our two created buttons.
            // This container type is a horizontal box, that means the widgets (buttons) are placed
            // in line horizontally.
            Gtk.HBox hbox = new Gtk.HBox();
            hbox.PackStart(button1);
            hbox.PackStart(button2);

            // Now we create the displaying window for our application.
            // The window gets some properties set like border width.
            // A delete event for closing this application is added.
            // Finally tthe created widgtes are conntected with the window and shown.
            this.Window = new Gtk.Window(Catalog.GetString("Acquire at least one spectrum"));
            this.Window.BorderWidth = 20;
            this.Window.Modal = true;
            this.Window.WindowPosition = Gtk.WindowPosition.Center;
            this.Window.DeleteEvent += this.OnDelete;
            this.Window.Child = hbox;
            this.Window.ShowAll();
        }

        // Mehtod for closing this application
        private void OnDelete(object sender, EventArgs e)
        {
            this.Window.Destroy();
        }

        // Method for acquiring and showing the spectra
        private void OnAcquireSpectraClicked(object sender, EventArgs e)
        {
            // The spectrum data will be acquired and shown for each active device.
            // We find this information inPlugin. Host.ActiveDevices.Idle.
            // (Host is an interface, which is implemented in the QuickStep application, so we have access to these members)
            foreach (IDevice dev in this.Plugin.Host.ActiveDevices.Idle)
            {
                // First we acquire the spectrum from the device.
                Spectrum spectrum = dev.Single();

                // Second the spectrum is added to the diagram surface of the QuickStep application.
                this.Plugin.Host.AddToCurrentPlot(spectrum);
            }
        }

        // This property saves a reference to our Window.
        Gtk.Window Window { get; set; }

        // This property saves a reference to our calling plugin
        SimplePlugin Plugin { get; set; }
    }




    // This class implements a little application for aquiring one spectrum and showing it
    // in the diagram of the QuickStep application.
    // This application is started by clicking on the menu item  'Active, if excatly one device is selected and device state is "Idle"'.
    // Then appears a window with two buttons, one for acquiring the spectrum and one for closing the application.
    public class ExactlyOneDevice
    {
        public ExactlyOneDevice(SimplePlugin plugin)
        {
            // Saving a reference of our plugin, because we need some properties of
            // our plugin (Host for acquiring the spectrum)
            this.Plugin = plugin;

            // Creating the first button for acquiring the spectra with 
            // the corresponding click event
            Gtk.Button button1 = new Gtk.Button();
            button1.Label = Catalog.GetString(Catalog.GetString("Acquire one spectrum"));
            button1.Clicked += this.OnAcquireSpectrumClicked;

            // Creating the second button for closing this application with
            // the corresponding click event
            Gtk.Button button2 = new Gtk.Button();
            button2.Label = Catalog.GetString(Catalog.GetString("Close"));
            button2.Clicked += this.OnDelete;

            // Now we need a container for our two created buttons.
            // This container type is a horizontal box, that means the widgets (buttons) are placed
            // in line horizontally.
            Gtk.HBox hbox = new Gtk.HBox();
            hbox.PackStart(button1);
            hbox.PackStart(button2);

            // Now we create the displaying window for our application.
            // The window gets some properties set like border width.
            // A delete event for closing this application is added.
            // Finally tthe created widgtes are conntected with the window and shown.
            this.Window = new Gtk.Window(Catalog.GetString("Acquire exactly one spectrum"));
            this.Window.BorderWidth = 20;
            this.Window.Modal = true;
            this.Window.WindowPosition = Gtk.WindowPosition.Center;
            this.Window.DeleteEvent += this.OnDelete;
            this.Window.Child = hbox;
            this.Window.ShowAll();
        }

        // Mehtod for closing this application
        private void OnDelete(object sender, EventArgs e)
        {
            this.Window.Destroy();
        }

        // Method for acquiring and showing the spectrum
        private void OnAcquireSpectrumClicked(object sender, EventArgs e)
        {
            // The spectrum data will be acquired and shown for each active device.
            // We find this information inPlugin. Host.ActiveDevices.Idle.
            // (Host is an interface, which is implemented in the QuickStep application, so we have access to these members)
            foreach (IDevice dev in this.Plugin.Host.ActiveDevices.Idle)
            {
                // First we acquire the spectrum from the device.
                Spectrum spectrum = dev.Single();

                // Second the spectrum is added to the diagram surface of the QuickStep application.
                this.Plugin.Host.AddToCurrentPlot(spectrum);
            }
        }

        // This property saves a reference to our Window.
        Gtk.Window Window { get; set; }

        // This property saves a reference to our calling plugin.
        SimplePlugin Plugin { get; set; }
    }
}

