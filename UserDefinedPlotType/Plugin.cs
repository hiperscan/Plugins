// Plugin.cs created with MonoDevelop
// User: klose at 15:27 10.01.2011
// CVS release: $Id: UserDefinedPlotDialog.cs,v 1.2 2011-04-19 12:49:21 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Reflection;

using Hiperscan.Unix;


namespace Hiperscan.Extensions.UserDefinedPlotType
{

    public class Plugin : Hiperscan.Extensions.IPlugin
    {
        public static readonly string name        = Catalog.GetString("User Defined Plot Types");
        public static readonly string version     = "1.0.0";
        public static readonly string vendor      = "Hiperscan GmbH";
        public static readonly string description = Catalog.GetString("Provides user defined plot types");
        public static readonly Guid guid          = new Guid("24ee5140-1ca3-11e0-ac64-0800200c9a66");

        public static readonly string PlotDefinitionFile = "user_defined_plot_type.xml";
        
        internal static List<UserDefinedPlotType> plot_type_infos = new List<UserDefinedPlotType>();
        
        internal static IHost host;

        public Plugin()
        {
            MenuInfo menu_info = new MenuInfo(Catalog.GetString("User-defined plot types"));
            menu_info.Invoked += (sender, e) => new UserDefinedPlotDialog();
            
            this.MenuInfos = new MenuInfo[] { menu_info };
        }

        public void ReplaceGui()
        {
        }

        public void Initialize(IHost host)
        {
            Plugin.host = host;

            UserDefinedPlotDialog.InitPlotTypeInfos();
        }

        public MenuInfo[] MenuInfos { get; }

        public bool Active { get; set; } = true;

        public ToolbarInfo[] ToolbarInfos   => null;
        public PlotTypeInfo[] PlotTypeInfos => Plugin.plot_type_infos.ToArray();
        public FileTypeInfo[] FileTypeInfos => null;
        public string Name                  => Plugin.name;
        public string Version               => Plugin.version;
        public string Vendor                => Plugin.vendor;
        public Assembly Assembly            => Assembly.GetExecutingAssembly();
        public string Description           => Plugin.description;
        public Guid Guid                    => Plugin.guid;
        public bool CanReplaceGui           => false;
    }
}