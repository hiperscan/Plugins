// UserDefinedPlotDialog.cs created with MonoDevelop
// User: klose at 15:27 10.01.2011
// CVS release: $Id: UserDefinedPlotDialog.cs,v 1.2 2011-04-19 12:49:21 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Gtk;


namespace Hiperscan.Extensions.UserDefinedPlotType
{

    public class PlotTypeTreeView : Gtk.TreeView
    {
        public delegate void F2PressedHandler(TreeView tv);
        public event         F2PressedHandler F2Pressed;
        
        public PlotTypeTreeView(ListStore store) : base(store) 
        {
        }

        protected override bool OnKeyPressEvent(Gdk.EventKey evnt)
        {
            if ((evnt.Key == Gdk.Key.F2) &&  (this.F2Pressed != null))
                this.F2Pressed(this);
            
            return base.OnKeyPressEvent(evnt);
        }
    }
}