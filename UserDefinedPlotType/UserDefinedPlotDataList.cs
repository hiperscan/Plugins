// UserDefinedPlotType.cs created with MonoDevelop
// User: klose at 15:27 10.01.2011
// CVS release: $Id: UserDefinedPlotDialog.cs,v 1.2 2011-04-19 12:49:21 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.IO;
using System.Xml.Serialization;


namespace Hiperscan.Extensions.UserDefinedPlotType
{

    [Serializable()]
    public class UserDefinedPlotDataList : System.Collections.Generic.List<UserDefinedPlotData>
    {

        public static UserDefinedPlotDataList ReadFromFile(string fname)
        {
            using (Stream stream = new FileStream(fname, FileMode.Open))
            using (StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(UserDefinedPlotDataList));
                UserDefinedPlotDataList list = (UserDefinedPlotDataList)serializer.Deserialize(reader);

                if (list == null)
                    throw new Exception("Error while deserialization.");

                return list;
            }
        }

        public void WriteToFile(string fname)
        {
            using (Stream stream = new FileStream(fname, FileMode.Create))
            using (StreamWriter writer = new StreamWriter(stream, System.Text.Encoding.UTF8))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(UserDefinedPlotDataList));
                serializer.Serialize(writer, this);
            }
        }
    }
}