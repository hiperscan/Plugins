// UserDefinedPlotType.cs created with MonoDevelop
// User: klose at 15:27 10.01.2011
// CVS release: $Id: UserDefinedPlotDialog.cs,v 1.2 2011-04-19 12:49:21 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;


namespace Hiperscan.Extensions.UserDefinedPlotType
{

    public enum DefinitionType : int
    {
        Simple,
        Advanced
    }
    
    public enum SpectrumType : int
    {
        Intensity,
        Absorbance,
        Reflectivity,
        Background,
        RawIntensity
    }
    
    public enum Spectrum0Type : int
    {
        First,
        Last,
        LeastValue,
        GreatestValue,
        LeastMean,
        GreatestMean,
        LeastMedian,
        GreatestMedian,
        MeanSpectrum
    }
    
    [Serializable()]
    public class UserDefinedPlotData
    {
        public string Name { get; set; }
        public string XLabel { get; set; }
        public string YLabel { get; set; }
        public double LowerLimit { get; set; }
        public double UpperLimit { get; set; }
        public double StandardMin { get; set; }
        public double StandardMax { get; set; }
        public bool Absorbance { get; set; }
        public DefinitionType DefinitionType { get; set; }
        public SpectrumType SpectrumType { get; set; }
        public Spectrum0Type Spectrum0Type { get; set; }
        public string Function { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
    }
}