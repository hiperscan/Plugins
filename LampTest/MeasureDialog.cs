using System;
using Gtk;
using Glade;
using System.IO;
using System.Text;


namespace Hiperscan.Extensions.LampTest
{
    public class MeasureDialog
    {
        [Widget] Gtk.Window measure_window = null;
        
        [Widget] Gtk.Entry TL_10_u_entry = null;
        [Widget] Gtk.Entry TL_10_i_entry = null;
        [Widget] Gtk.Entry RL_10_u_entry = null;
        [Widget] Gtk.Entry RL_10_i_entry = null;
        
        [Widget] Gtk.CheckButton TL_10_1_checkbutton = null;
        [Widget] Gtk.CheckButton TL_10_2_checkbutton = null;
        [Widget] Gtk.CheckButton TL_10_3_checkbutton = null;
        [Widget] Gtk.CheckButton TL_10_4_checkbutton = null;
        [Widget] Gtk.CheckButton TL_10_5_checkbutton = null;
        [Widget] Gtk.CheckButton TL_10_6_checkbutton = null;
        [Widget] Gtk.CheckButton TL_10_7_checkbutton = null;
        [Widget] Gtk.CheckButton TL_10_8_checkbutton = null;
        [Widget] Gtk.CheckButton RL_10_checkbutton = null;
        [Widget] public Gtk.TextView LT_10_textview = null;
        
        [Widget] Gtk.Entry TL_12_u_entry = null;
        [Widget] Gtk.Entry TL_12_i_entry = null;
        [Widget] Gtk.Entry RL_12_u_entry = null;
        [Widget] Gtk.Entry RL_12_i_entry = null;
        
        [Widget] Gtk.CheckButton TL_12_1_checkbutton = null;
        [Widget] Gtk.CheckButton TL_12_2_checkbutton = null;
        [Widget] Gtk.CheckButton TL_12_3_checkbutton = null;
        [Widget] Gtk.CheckButton TL_12_4_checkbutton = null;
        [Widget] Gtk.CheckButton TL_12_5_checkbutton = null;
        [Widget] Gtk.CheckButton TL_12_6_checkbutton = null;
        [Widget] Gtk.CheckButton TL_12_7_checkbutton = null;
        [Widget] Gtk.CheckButton TL_12_8_checkbutton = null;
        [Widget] Gtk.CheckButton RL_12_checkbutton = null;
        [Widget] public Gtk.TextView LT_12_textview = null;

        
        private LampTestProcedure Parent {get;set;}
        
        public MeasureDialog (LampTestProcedure parent)
        {
            this.Parent = parent;
            this.BuildGUI();
        }
        
        public void Show()
        {
            this.measure_window.ShowAll();
        }
        
        private void BuildGUI()
        {
            Glade.XML gui = new Glade.XML(null, "LampTest.lamptest.glade", "measure_window", "Hiperscan");
            gui.Autoconnect(this);
            this.measure_window.WindowPosition = WindowPosition.CenterAlways;
            this.measure_window.Title = "Lampentest - Messinfos";
            this.measure_window.Modal = true;
            this.measure_window.DeleteEvent += (sender, e) =>
            {
                this.SaveMeasureProtokoll();
                if (this.Counter != 3)
                {
                    e.RetVal = true;
                    this.measure_window.Hide();
                }
                this.Parent.IsRunning = false;
            };
            
            this.TL_10_1_checkbutton.Label = LampTestProcedure.Lamps_10V[0] + " ist OK.";
            this.TL_10_1_checkbutton.Active = true;
            this.TL_10_2_checkbutton.Label = LampTestProcedure.Lamps_10V[1] + " ist OK.";
            this.TL_10_2_checkbutton.Active = true;
            this.TL_10_3_checkbutton.Label = LampTestProcedure.Lamps_10V[2] + " ist OK.";
            this.TL_10_3_checkbutton.Active = true;
            this.TL_10_4_checkbutton.Label = LampTestProcedure.Lamps_10V[3] + " ist OK.";
            this.TL_10_4_checkbutton.Active = true;
            this.TL_10_5_checkbutton.Label = LampTestProcedure.Lamps_10V[4] + " ist OK.";
            this.TL_10_5_checkbutton.Active = true;
            this.TL_10_6_checkbutton.Label = LampTestProcedure.Lamps_10V[5] + " ist OK.";
            this.TL_10_6_checkbutton.Active = true;
            this.TL_10_7_checkbutton.Label = LampTestProcedure.Lamps_10V[6] + " ist OK.";
            this.TL_10_7_checkbutton.Active = true;
            this.TL_10_8_checkbutton.Label = LampTestProcedure.Lamps_10V[7] + " ist OK.";
            this.TL_10_8_checkbutton.Active = true;
            
            this.RL_10_checkbutton.Label = LampTestProcedure.Lamps_10V[8] + " ist OK.";
            this.RL_10_checkbutton.Active = true;
            
            this.TL_12_1_checkbutton.Label = LampTestProcedure.Lamps_12V[0] + " ist OK.";
            this.TL_12_1_checkbutton.Active = true;
            this.TL_12_2_checkbutton.Label = LampTestProcedure.Lamps_12V[1] + " ist OK.";
            this.TL_12_2_checkbutton.Active = true;
            this.TL_12_3_checkbutton.Label = LampTestProcedure.Lamps_12V[2] + " ist OK.";
            this.TL_12_3_checkbutton.Active = true;
            this.TL_12_4_checkbutton.Label = LampTestProcedure.Lamps_12V[3] + " ist OK.";
            this.TL_12_4_checkbutton.Active = true;
            this.TL_12_5_checkbutton.Label = LampTestProcedure.Lamps_12V[4] + " ist OK.";
            this.TL_12_5_checkbutton.Active = true;
            this.TL_12_6_checkbutton.Label = LampTestProcedure.Lamps_12V[5] + " ist OK.";
            this.TL_12_6_checkbutton.Active = true;
            this.TL_12_7_checkbutton.Label = LampTestProcedure.Lamps_12V[6] + " ist OK.";
            this.TL_12_7_checkbutton.Active = true;
            this.TL_12_8_checkbutton.Label = LampTestProcedure.Lamps_12V[7] + " ist OK.";
            this.TL_12_8_checkbutton.Active = true;
            
            this.RL_12_checkbutton.Label = LampTestProcedure.Lamps_12V[8] + " ist OK.";
            this.RL_12_checkbutton.Active = true;
        }
        
        private void SaveMeasureProtokoll()
        {
            string file = Path.Combine(LampTestProcedure.MonthlyMeasureFolder, "Messprotokoll.txt");
            if (Directory.Exists(LampTestProcedure.MonthlyMeasureFolder) == false)
                Directory.CreateDirectory(LampTestProcedure.MonthlyMeasureFolder);
            
            using(StreamWriter sw = new StreamWriter(file, false, Encoding.UTF8))
            {
                sw.WriteLine("Messprotokoll vom " + DateTime.Now.ToString("dd.MMM.yyyy"));
                sw.WriteLine("---------------------------------------------------------------------------------");
                sw.WriteLine();
                sw.WriteLine("Messreihe 10V:");
                sw.WriteLine("------------------");
                sw.WriteLine("Testlampen:");
                sw.WriteLine("    Spannung/V: " + this.TL_10_u_entry.Text);
                sw.WriteLine("    Strom/A   : " + this.TL_10_i_entry.Text);
                sw.WriteLine();
                sw.WriteLine("Referenzlampe:");
                sw.WriteLine("    Spannung/V: " + this.RL_10_u_entry.Text);
                sw.WriteLine("    Strom/A   : " + this.RL_10_i_entry.Text);
                sw.WriteLine();
                sw.WriteLine("Lampenzustand:");
                
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[0]] = TL_10_1_checkbutton.Active;
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[1]] = TL_10_2_checkbutton.Active;
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[2]] = TL_10_3_checkbutton.Active;
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[3]] = TL_10_4_checkbutton.Active;
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[4]] = TL_10_5_checkbutton.Active;
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[5]] = TL_10_6_checkbutton.Active;
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[6]] = TL_10_7_checkbutton.Active;
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[7]] = TL_10_8_checkbutton.Active;
                this.Parent.LampsOK10V[LampTestProcedure.Lamps_10V[8]] = RL_10_checkbutton.Active;
                
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[0] + (TL_10_1_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[1] + (TL_10_2_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[2] + (TL_10_3_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[3] + (TL_10_4_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[4] + (TL_10_5_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[5] + (TL_10_6_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[6] + (TL_10_7_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[7] + (TL_10_8_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_10V[8] + (RL_10_checkbutton.Active ? ": OK" : ": Defekt"));
                
                sw.WriteLine();
                sw.WriteLine("Kommentar:");
                sw.WriteLine(LT_10_textview.Buffer.Text);

                
                sw.WriteLine();
                sw.WriteLine();
                sw.WriteLine();
                sw.WriteLine("Messreihe 12V:");
                sw.WriteLine("------------------");
                sw.WriteLine("Testlampen:");
                sw.WriteLine("    Spannung/V: " + this.TL_12_u_entry.Text);
                sw.WriteLine("    Strom/mA  : " + this.TL_12_i_entry.Text);
                sw.WriteLine();
                sw.WriteLine("Referenzlampe:");
                sw.WriteLine("    Spannung/V: " + this.RL_12_u_entry.Text);
                sw.WriteLine("    Strom/mA  : " + this.RL_12_i_entry.Text);
                sw.WriteLine();
                sw.WriteLine("Lampenzustand:");
                
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[0]] = TL_12_1_checkbutton.Active;
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[1]] = TL_12_2_checkbutton.Active;
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[2]] = TL_12_3_checkbutton.Active;
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[3]] = TL_12_4_checkbutton.Active;
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[4]] = TL_12_5_checkbutton.Active;
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[5]] = TL_12_6_checkbutton.Active;
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[6]] = TL_12_7_checkbutton.Active;
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[7]] = TL_12_8_checkbutton.Active;
                this.Parent.LampsOK12V[LampTestProcedure.Lamps_12V[8]] = RL_12_checkbutton.Active;
                
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[0] + (TL_12_1_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[1] + (TL_12_2_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[2] + (TL_12_3_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[3] + (TL_12_4_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[4] + (TL_12_5_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[5] + (TL_12_6_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[6] + (TL_12_7_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[7] + (TL_12_8_checkbutton.Active ? ": OK" : ": Defekt"));
                sw.WriteLine("Lampe " + LampTestProcedure.Lamps_12V[8] + (RL_12_checkbutton.Active ? ": OK" : ": Defekt"));
                
                sw.WriteLine();
                sw.WriteLine("Kommentar:");
                sw.WriteLine(LT_12_textview.Buffer.Text);
            }
        }
        
        public int Counter {get;set;}
    }
}

