using System;
using System.Collections.Generic;
using System.IO;

using Glade;
using Gtk;

using Hiperscan.Spectroscopy.IO;
using Hiperscan.SGS;
using Hiperscan.SGS.Benchtop;
using Hiperscan.Spectroscopy;


namespace Hiperscan.Extensions.LampTest
{

    public class LampTestProcedure
    {
        [Widget] Gtk.Window main_window     = null;
        [Widget] Gtk.Label text_label         = null;
        [Widget] Gtk.Button start_button     = null;
        
        private static string RootFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Lampentest");
        public static string MonthlyMeasureFolder = Path.Combine(RootFolder, "Lampentest_" + DateTime.Now.ToString("yyyy-MM-dd"));
        private static string Measure10VFolder = Path.Combine(MonthlyMeasureFolder, "Lampentest_10V");
        private static string Measure12VFolder = Path.Combine(MonthlyMeasureFolder, "Lampentest_12V");
        private static int TimeWarmUp = 5;
        
        public static readonly string[] Lamps_10V = new string[]
        {
            "99910-17",
            "99910-16",
            "99910-15",
            "99910-14",
            "99910-13",
            "99910-12",
            "99910-11",
            "99910-10",
            "99910-09",
        };
        
        public static readonly string[] Lamps_12V = new string[]
        {
            "99910-08",
            "99910-07",
            "99910-06",
            "99910-05",
            "99910-04",
            "99910-03",
            "99910-02",
            "99910-01",
            "99910-00",
        };
        
        
        public LampTestProcedure()
        {
            this.Host = Plugin.Host;
            this.BuildGUI();
            this.main_window.ShowAll();
            
            this.InitDevice();
            this.Host.DeviceList.DeviceAdded += (ListStore, added) =>
            {
                if (this.Finder == null)
                    this.InitDevice();
            };
            
            this.LampsOK10V = new Dictionary<string, bool>();
            for(int i = 0; i < LampTestProcedure.Lamps_10V.Length; i++)
                this.LampsOK10V.Add(LampTestProcedure.Lamps_10V[i], true);
            
            this.LampsOK12V = new Dictionary<string, bool>();
            for(int i = 0; i < LampTestProcedure.Lamps_12V.Length; i++)
                this.LampsOK12V.Add(LampTestProcedure.Lamps_12V[i], true);
            
            this.Spectra10V = new Dictionary<string, Spectrum>();
            this.Spectra12V = new Dictionary<string, Spectrum>();
            
            this.MDialog = new MeasureDialog(this);
        }
        
        private void BuildGUI()
        {
            Glade.XML gui = new Glade.XML(null, "LampTest.lamptest.glade", "main_window", "Hiperscan");
            gui.Autoconnect(this);
            this.main_window.DefaultWidth = 600;
            this.main_window.WindowPosition = WindowPosition.CenterAlways;
            this.main_window.Title = "Quickstep - Lampentest";
            this.main_window.DeleteEvent += (sender, e) =>
            {
                if (Plugin.ReplacedGui)
                    this.Host.Quit();
            };
            
            this.text_label.Text = "Dieses Programm führt Sie durch den Messablauf für den Lampentest. ";
            this.text_label.Text += String.Format(" Das Ergebnis der heutigen Messung wird dann im Ordner '{0}' abgelegt werden.\n\n",
                                                  LampTestProcedure.MonthlyMeasureFolder);
            this.text_label.Text += "Noch einige Anmerkungen:\n";
            this.text_label.Text += "  - Strom- und Spannungsmessung für Referenzlampen nur über externe Versorgung.\n";
            this.text_label.Text += "  - Aufwärmphasen für die Referenzlampen müssen eingehalten werden.\n";
            this.text_label.Text += "  - Die Testlampen müssen bereits 10 min angescaltet sein.\n";
            this.text_label.Text += "  - Die Referenzlampen müssen ausgeschaltet sein.\n";
            this.start_button.Label = "";
            this.start_button.Sensitive = false;
            this.MeasureEnd = false;
            this.start_button.Clicked += (sender, e) =>
            {
                if (this.MeasureEnd == false)
                {
                    this.start_button.Sensitive = false;
                    this.start_button.Label = "Messprozedur läuft ...";
                    this.text_label.Text = "";
                    this.text_label.Xalign = 0.5f;
                    this.text_label.Yalign = 0.5f;
                    this.text_label.UseMarkup = true;
                    
                    if (this.CheckDeviceTemperature() == false)
                        return;
                    this.ShowMeasureDialog(1);
                    this.MeasureDarkIntensity10V();
                }
                else
                {
                    this.main_window.Destroy();
                    if (Plugin.ReplacedGui)
                        this.Host.Quit();
                }
            };
        }
        
        private IDevice Device {get;set;}
        private Finder Finder  {get;set;}
        
        private void InitDevice()
        {
            this.Device = null;
            this.Finder = null;
            this.start_button.Sensitive = false;
            this.start_button.Label     = "Initialisiere Gerät ... ";
            
            lock (this.Host.DeviceList)
            {
                foreach (IDevice dev in this.Host.DeviceList.Values)
                {
                    bool is_disconnected = false;
                    if (dev.IsConnected == false)
                    {
                        try
                        {
                            dev.Open();
                            is_disconnected = true;
                        }
                        catch(Exception)
                        { 
                            continue; 
                        }
                    }
                
                    try
                    {
                        dev.WaitForIdle(15000);
                    }
                    catch (TimeoutException) {}
                    
                    if (dev.IsFinder)
                    {
                        this.Device = dev;
                        break;
                    }
                
                    if (is_disconnected)
                    {
                        try
                        {
                            dev.Close();
                        }
                        catch(Exception) {}
                    }
                }
            }
            
            if (this.Device == null)
                return;


            this.Finder = new Finder(this.Device, ReferenceWheelVersion.Auto, ExternalWhiteReferenceType.Unknown, "Lamp-Test 1.0.0")
            {
                CorrectionMask = CorrectionMask.All,
                IdleTaskHandler = this.Host.ProcessEvents,
                AutoSaveReferences = true
            };

            this.Device.Removed += delegate(IDevice device) 
            {
                this.start_button.Label = "Gerät nicht angeschlossen";
                this.start_button.Sensitive = false;
                this.Finder = null;
                this.Device = null;
            };
            
            try
            {
                if (this.Device.IsConnected == false)
                    this.Device.Open();
                this.Host.ProcessEvents();
                this.Device.WaitForIdle(10000);
                if (this.Finder.HardwareFeatures.HasFlag(HardwareFeatures.StartButton))
                    this.Device.SwitchFinderStatusLED(false);
                this.Device.CurrentConfig.Average = 500;
                this.Device.SetFinderLightSource(false, Host.ProcessEvents);
            }
            catch (Exception)
            {
                this.start_button.Label = "Fehler Initialisierung Gerät";
                this.Finder = null;
                this.Device = null;
                return;
            }
            this.Finder.ResetReferences();
    
            this.start_button.Label = "Start Mess-Prozedur";
            this.start_button.Sensitive = true;
        }
        
        private bool CheckDeviceTemperature()
        {
            if (this.Finder.Device.ProtocolVersion.Major != 0 ||  this.Finder.Device.ProtocolVersion.Minor >= 13)
            {
                if (this.Finder.Device.ReadStatus() <= 30)
                {
                    string message = "Finder hat noch nicht die nötige Betriebstemperatur." +
                         " Versuchen Sie es in 5 min wieder. Applikation wird geschlossen.";
                    this.SetInfoText(String.Empty);
                    this.ShowDialog(message);
                    
                    this.main_window.Destroy();
                    if (Plugin.ReplacedGui)
                        this.Host.Quit();
                    return false;
                }
            }
            return true;
        }
        
        private Spectrum MeasureSpectraWhite(string path)
        {
            Spectrum spectrum = null;
            this.Finder.AutoSaveDirectory = path;
            int counter = 0;
            try
            {
                this.Finder.RecalibrateInternal(false, this.DarkSpectrum10V);
                spectrum = this.Finder.CurrentInternalWhiteSpectrum.Clone();
            }
            catch(Exception)
            {
                counter++;
                if (counter == 3)
                {
                    return null;
                }
            }

            System.Threading.Thread.Sleep(1000);
            return spectrum;
        }
        
        private bool MeasureSpectraDark(string path, bool is_10V)
        {
            Spectrum dark;

            this.Finder.Device.SetFinderWheel(FinderWheelPosition.White, null);
            dark = this.Finder.Device.Single(false);
            if (is_10V == true)
                this.DarkSpectrum10V = dark;
            else
                this.DarkSpectrum12V = dark;
            
            System.Threading.Thread.Sleep(1000);
            
            return (dark.RawIntensity.YMax() < 0.7);
        }
        
        private void CalcAbsorbance(bool is_10V, Spectrum reference)
        {
            Dictionary<string, Spectrum> spectra = null;
            Spectrum dark_spectrum = null;
            if (is_10V == true)
            {
                dark_spectrum = this.DarkSpectrum10V;
                spectra = this.Spectra10V;
            }
            else
            {
                dark_spectrum = this.DarkSpectrum12V;
                spectra = this.Spectra12V;
            }
            
            Spectrum reference_clone = reference.Clone();
            reference_clone.ApplyCorrectionsToRawData(CorrectionMask.All);
            
            foreach (string key in spectra.Keys)
            {
                spectra[key].ApplyCorrectionsToRawData(CorrectionMask.All);
                spectra[key].Reference = reference_clone.Clone();
                string label = String.Empty;
                if (is_10V == true)
                     label = Path.GetFileNameWithoutExtension(key).Replace("_", " 10V ");
                else
                    label = Path.GetFileNameWithoutExtension(key).Replace("_", " 12V ");
                
                label += " " + DateTime.Now.ToString("dd.MM.yyyy");
                spectra[key].Label = label;
                CSV.Write(spectra[key], key);
            }
        }

        
        
        private MeasureDialog MDialog {get;set;}
        public bool IsRunning {get;set;}
        private void ShowMeasureDialog(int counter)
        {
            this.SetInfoText(String.Empty);
            this.IsRunning = true;
            this.MDialog.Counter = counter;
            this.MDialog.Show();
            while (this.IsRunning == true)
            {
                this.Host.ProcessEvents();
                System.Threading.Thread.Sleep(200);
            }
        }
        
        private void MeasureDarkIntensity10V()
        {
            do
            {
                this.SetInfoText(String.Empty);
                string message = "Dunkel-Referenz für 10V-Messreihe aufnehmen. (Finder ohne Lampe)";
                this.ShowDialog(message);
                
                this.SetInfoText("Dunkelreferenz 10V wird augenommen.");
                
                if (Directory.Exists(LampTestProcedure.Measure10VFolder) == false)
                    Directory.CreateDirectory(LampTestProcedure.Measure10VFolder);
            } while (this.MeasureSpectraDark(LampTestProcedure.Measure10VFolder, true) == false);
            
            this.MeasureLamps10V();
        }
        
        private void MeasureLamps10V()
        {
            string dialog_message = String.Empty;
            bool error = false;
            for( int i = 0; i < LampTestProcedure.Lamps_10V.Length - 1; i++)
            {
                this.SetInfoText(String.Empty);
                if (this.LampsOK10V[LampTestProcedure.Lamps_10V[i]] == false)
                    continue;
                    
                dialog_message = String.Format("Bitte eingeschaltete 10V-Lampe mit der Kennung '{0}' in den Finder einbauen.", 
                                                      LampTestProcedure.Lamps_10V[i]);
                this.ShowDialog(dialog_message);
                
                string info_message = String.Format("10V-Lampe '{0}' wird gemessen.",
                                                    LampTestProcedure.Lamps_10V[i]);
                this.SetInfoText(info_message);
                
                string path = Path.Combine(LampTestProcedure.Measure10VFolder, "Lampe_" +  LampTestProcedure.Lamps_10V[i]);
                if (Directory.Exists(path) == false)
                    Directory.CreateDirectory(path);
                
                Spectrum spectrum = this.MeasureSpectraWhite(path);
                if (spectrum == null)
                {
                    error = true;
                    break;
                }
                string file = Path.Combine(path,"Lampe_" + LampTestProcedure.Lamps_10V[i] + ".csv");
                this.Spectra10V.Add(file, spectrum);
            }
            
            if (error == true)
            {
                string message = "Versuch wird abgebrochen, da das Gerät nicht korrekt arbeitet. Bitte den " +
                        "ordner '" + LampTestProcedure.MonthlyMeasureFolder + "' samt Inhalt löschen.";
                this.SetInfoText(String.Empty);
                this.ShowDialog(message);
                this.main_window.Destroy();
                if (Plugin.ReplacedGui)
                    this.Host.Quit();
            }
            else
            {
                this.SetInfoText(String.Empty);
                dialog_message = "Alle 10V-Testlampen ausschalten.";
                this.ShowDialog(dialog_message);
                this.StartTimer10V();
            }
        }
        
        private void StartTimer10V()
        {
            this.SetInfoText(String.Empty);
            string dialog_message = String.Format("Bitte 10V-Referenzlampe mit der Kennung '{0}' einschalten und {1} min aufwärmen lassen. " + 
                                                  "Lampe noch nicht in den Finder einbauen!!!!", 
                                                      LampTestProcedure.Lamps_10V[LampTestProcedure.Lamps_10V.Length - 1],
                                                      LampTestProcedure.TimeWarmUp);
            this.ShowDialog(dialog_message);
            this.WarmUp10VStart = DateTime.Now;
            this.MeasureDarkIntensity12V();
        }
        
        private void MeasureLamp10VReference()
        {
            string info_message = String.Empty;
            DateTime end = this.WarmUp10VStart.Add(TimeSpan.FromMinutes(LampTestProcedure.TimeWarmUp));
            this.SetInfoText(String.Empty);
            for(;;)
            {
                if (DateTime.Now > end)
                    break;
                
                TimeSpan difference_span  = end - DateTime.Now;
                info_message = String.Format("Aufwärmphase 10V-Referenzlampe " +
                                                    "{0:D2}:{1:D2} min", 
                                                    difference_span.Minutes,
                                                    difference_span.Seconds % 60);
                this.SetInfoText(info_message);
                System.Threading.Thread.Sleep(500);
            }
            
            this.SetInfoText(String.Empty);
            string dialog_message = String.Format("Bitte eingeschaltete 10V-Referenzlampe mit der Kennung '{0}' in den Finder einbauen.", 
                                                      LampTestProcedure.Lamps_10V[LampTestProcedure.Lamps_10V.Length - 1]);
            this.ShowDialog(dialog_message);
            
            info_message = String.Format("10V-Referenzlampe '{0}' wird gemessen.",
                                                    LampTestProcedure.Lamps_10V[LampTestProcedure.Lamps_10V.Length - 1]);
            this.SetInfoText(info_message);
            
                
            string path = Path.Combine(LampTestProcedure.Measure10VFolder, "Lampe_" +  
                                        LampTestProcedure.Lamps_10V[LampTestProcedure.Lamps_10V.Length - 1]);
            
            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
                
                
            Spectrum spectrum = this.MeasureSpectraWhite(path);
            spectrum.DarkIntensity = new DarkIntensity(this.Finder.CurrentInternalDarkSpectrum);
            if (spectrum == null)
            {
                string message = "Versuch wird abgebrochen, da das Gerät nicht korrekt arbeitet. Bitte den " +
                        "ordner '" + LampTestProcedure.MonthlyMeasureFolder + "' samt Inhalt löschen.";
                this.SetInfoText(String.Empty);
                this.ShowDialog(message);
                this.main_window.Destroy();
                if (Plugin.ReplacedGui)
                    this.Host.Quit();
            }
            else
            {
                this.CalcAbsorbance(true, spectrum);
                spectrum.Label = "Referenzlampe 10V " + LampTestProcedure.Lamps_10V[LampTestProcedure.Lamps_10V.Length - 1] +
                                 " " + DateTime.Now.ToString("dd.MM.yyyy");
                spectrum.Comment = this.MDialog.LT_10_textview.Buffer.Text;
                string file = Path.Combine(path, "Referenz_lampe_10V_white.csv");
                CSV.Write(spectrum, Path.Combine(path, file));
                
                this.ShowMeasureDialog(2);
                this.StartTimer12V();
            }
        }

        
        
        
        
        
        
        private void MeasureDarkIntensity12V()
        {
            do
            {
                this.SetInfoText(String.Empty);
                string message = "Dunkel-Referenz für 12V-Messreihe aufnehmen. (Finder ohne Lampe)";
                this.ShowDialog(message);
                
                this.SetInfoText("Dunkelreferenz 12V wird aufgenommen.");
                
                if (Directory.Exists(LampTestProcedure.Measure12VFolder) == false)
                    Directory.CreateDirectory(LampTestProcedure.Measure12VFolder);
            }
            while (this.MeasureSpectraDark(LampTestProcedure.Measure12VFolder, false) == false);
            
            this.MeasureLamps12V();
        }
        
        private void MeasureLamps12V()
        {
            string dialog_message = String.Empty;
            bool error = false;
            for( int i = 0; i < LampTestProcedure.Lamps_12V.Length - 1; i++)
            {
                this.SetInfoText(String.Empty);
                if (this.LampsOK12V[LampTestProcedure.Lamps_12V[i]] == false)
                    continue;
                
                dialog_message = String.Format("Bitte eingeschaltete 12V-Lampe mit der Kennung '{0}' in den Finder einbauen.", 
                                                      LampTestProcedure.Lamps_12V[i]);
                this.ShowDialog(dialog_message);
                
                string info_message = String.Format("12V-Lampe '{0}' wird gemessen.",
                                                    LampTestProcedure.Lamps_12V[i]);
                this.SetInfoText(info_message);
                
                string path = Path.Combine(LampTestProcedure.Measure12VFolder, "Lampe_" +  LampTestProcedure.Lamps_12V[i]);
                if (Directory.Exists(path) == false)
                    Directory.CreateDirectory(path);
                
                Spectrum spectrum = this.MeasureSpectraWhite(path);
                if (spectrum == null)
                {
                    error = true;
                    break;
                }
                string file = Path.Combine(path,"Lampe_" + LampTestProcedure.Lamps_12V[i] + ".csv");
                this.Spectra12V.Add(file, spectrum);
            }
            
            if (error == true)
            {
                string message = "Versuch wird abgebrochen, da das Gerät nicht korrekt arbeitet. Bitte den " +
                        "ordner '" + LampTestProcedure.MonthlyMeasureFolder + "' samt Inhalt löschen.";
                this.SetInfoText(String.Empty);
                this.ShowDialog(message);
                this.main_window.Destroy();
                if (Plugin.ReplacedGui)
                    this.Host.Quit();
            }
            else
            {
            
                this.SetInfoText(String.Empty);
                dialog_message = "Alle 12V-Testlampen ausschalten.";
                this.ShowDialog(dialog_message);
                this.MeasureLamp10VReference();
            }
        }
        
        private void StartTimer12V()
        {
            this.SetInfoText(String.Empty);
            
            string dialog_message = String.Format("Bitte 12V-Referenzlampe mit der Kennung '{0}' einschalten und {1} min aufwärmen lassen. " + 
                                                  "Lampe noch nicht in den Finder einbauen!!!!", 
                                                      LampTestProcedure.Lamps_12V[LampTestProcedure.Lamps_12V.Length - 1],
                                                         LampTestProcedure.TimeWarmUp);
            this.ShowDialog(dialog_message);
            this.WarmUp12VStart = DateTime.Now;
            this.MeasureLamp12VReference();
        }
        
        
        private void MeasureLamp12VReference()
        {
            string info_message = String.Empty;
            DateTime end = this.WarmUp12VStart.Add(TimeSpan.FromMinutes(LampTestProcedure.TimeWarmUp));
            this.SetInfoText(String.Empty);
            for(;;)
            {
                if (DateTime.Now > end)
                    break;
                
                TimeSpan difference_span  = end - DateTime.Now;
                info_message = String.Format("Aufwärmphase 12V-Referenzlampe\n" +
                                                    "{0:D2}:{1:D2} min", 
                                                    difference_span.Minutes,
                                                    difference_span.Seconds % 60);
                this.SetInfoText(info_message);
                System.Threading.Thread.Sleep(500);
            }
            
            this.SetInfoText(String.Empty);
            string dialog_message = String.Format("Bitte eingeschaltete 12V-Referenzlampe mit der Kennung '{0}' in den Finder einbauen.", 
                                                      LampTestProcedure.Lamps_12V[LampTestProcedure.Lamps_12V.Length - 1]);
            this.ShowDialog(dialog_message);
            
            info_message = String.Format("12V-Referenzlampe '{0}' wird gemessen.",
                                                    LampTestProcedure.Lamps_12V[LampTestProcedure.Lamps_12V.Length - 1]);
            this.SetInfoText(info_message);
            
                
            string path = Path.Combine(LampTestProcedure.Measure12VFolder, "Lampe_" +  
                                        LampTestProcedure.Lamps_12V[LampTestProcedure.Lamps_12V.Length - 1]);
            
            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
                
                
            Spectrum spectrum = this.MeasureSpectraWhite(path);
            spectrum.DarkIntensity = new DarkIntensity(this.Finder.CurrentInternalDarkSpectrum);
            if (spectrum == null)
            {
                string message = "Versuch wird abgebrochen, da das Gerät nicht korrekt arbeitet. Bitte den " +
                        "ordner '" + LampTestProcedure.MonthlyMeasureFolder + "' samt Inhalt löschen.";
                this.SetInfoText(String.Empty);
                this.ShowDialog(message);
                this.main_window.Destroy();
                if (Plugin.ReplacedGui)
                    this.Host.Quit();
            }
            else
            {
                this.CalcAbsorbance(false, spectrum);
                string file = Path.Combine(path, "Referenz_lampe_12V_white.csv");
                spectrum.Label = "Referenzlampe 12V " + LampTestProcedure.Lamps_12V[LampTestProcedure.Lamps_12V.Length - 1] +
                                 " " + DateTime.Now.ToString("dd.MM.yyyy");
                spectrum.Comment = this.MDialog.LT_12_textview.Buffer.Text;
                CSV.Write(spectrum, Path.Combine(path, file));
                
                this.ShowMeasureDialog(3);
                
                this.SetInfoText("Messung beendet. Alle Lampen wieder einschalten, Referenzlampen ausschalten.");
                this.MeasureEnd = true;
                this.start_button.Label = "Anwendung beenden";
                this.start_button.Sensitive = true;
            }
        }
        
        
        

        
        private void ShowDialog(string message)
        {
            Gtk.MessageDialog mdlg = new Gtk.MessageDialog(this.main_window,
                                                           Gtk.DialogFlags.Modal,
                                                           Gtk.MessageType.Info,
                                                           Gtk.ButtonsType.Ok, 
                                                           message);
            
            mdlg.Run();
            mdlg.Destroy();
        }
        
        private void SetInfoText(string text)
        {
            string begin = "<span size=\"large\" weight=\"bold\">";
            string end   = "</span>";
            this.text_label.Markup = begin + text + end;
            this.Host.ProcessEvents();
        }
        
        
        
        public IHost Host {get;set;}
        private DateTime WarmUp10VStart {get;set;}
        private DateTime WarmUp12VStart {get;set;}
        private bool MeasureEnd {get;set;}
        
        public Dictionary<string, bool> LampsOK10V {get;set;}
        public Dictionary<string, bool> LampsOK12V {get;set;}
        
        private Dictionary<string, Spectrum> Spectra10V {get;set;}
        private Dictionary<string, Spectrum> Spectra12V {get;set;}
        
        private Spectrum DarkSpectrum10V {get;set;}
        private Spectrum DarkSpectrum12V {get;set;}
    }
}

