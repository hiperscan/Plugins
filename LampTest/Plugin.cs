using System;
using System.Reflection;
using Hiperscan.Extensions;


namespace Hiperscan.Extensions.LampTest
{
    public class Plugin : IPlugin
    {
        public static IHost Host { get; private set; }
        public static bool ReplacedGui { get; private set; }
        
        public Plugin ()
        {
            MenuInfo menu_info1 = new MenuInfo("Lampentest");
            menu_info1.Invoked += delegate(object sender, EventArgs e) 
            {
                new LampTestProcedure();
            };
            
            this.MenuInfos = new MenuInfo[]{menu_info1};
        }
    

        #region IPlugin implementation
        public void Initialize (IHost host)
        {
            Plugin.Host = host;
        }

        public void ReplaceGui ()
        {
            Plugin.ReplacedGui = true;
            new LampTestProcedure();
        }

        public MenuInfo[] MenuInfos 
        {
            get;
            set;
        }

        public ToolbarInfo[] ToolbarInfos 
        {
            get 
            {
                return null;
            }
        }

        public PlotTypeInfo[] PlotTypeInfos 
        {
            get 
            {
                return null;
            }
        }

        public FileTypeInfo[] FileTypeInfos 
        {
            get 
            {
                return null;
            }
        }

        public string Name 
        {
            get 
            {
                return "Lamp-Test";
            }
        }

        public string Version 
        {
            get 
            {
                return "1.0.0";
            }
        }

        public string Vendor 
        {
            get 
            {
                return "Hiperscan GmbH";
            }
        }

        public Assembly Assembly 
        {
            get 
            {
                return Assembly.GetExecutingAssembly();
            }
        }

        public string Description 
        {
            get 
            {
                return "Plugin Für Langzeittest von Lampen";
            }
        }

        public Guid Guid 
        {
            get 
            {
                return new Guid("e3641b6d-6d7e-4588-b2d0-a1bdc86b05d5");
            }
        }

        private bool active = true;
        public bool Active 
        {
            get 
            {
                return this.active;
            }
            set 
            {
                this.active = value;
            }
        }

        public bool CanReplaceGui 
        {
            get 
            {
                return true;
            }
        }
        #endregion
}
}

