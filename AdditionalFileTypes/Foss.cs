// Foss.cs created with MonoDevelop
// User: klose at 17:14 05.11.2012
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

using Hiperscan.Spectroscopy;
using Hiperscan.Spectroscopy.IO;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public class FossContainerFileTypeInfo : FileTypeInfo
    {
        public FossContainerFileTypeInfo() : base("FOSS (txt)", new string[] {"txt"})
        {
            this.is_container = true;
        }
        
        public override Spectrum[] ReadContainer(string fname, out List<MetaData> meta_data)
        {
            meta_data = null;
            return Foss.ReadContainer(fname);
        }
        
        public override void WriteContainer(List<Spectrum> spectra, List<MetaData> meta_data, string fname, CultureInfo culture)
        {
            throw new NotImplementedException(Catalog.GetString("Output of FOSS txt format is not implemented yet."));
        }
    }
    
    public static class Foss
    {
        private static CultureInfo culture = CultureInfo.InvariantCulture;
        
        public static Spectrum[] ReadContainer(string fname)
        {
            using (Stream stream = new FileStream(fname, FileMode.Open))
            {
                using (TextReader reader = new StreamReader(stream))
                {
                    string line;
                    List<Spectrum> spectra = new List<Spectrum>();
                    
                    while (true)
                    {
                        string product = string.Empty;
                        string sample  = string.Empty;
                        string date    = string.Empty;
                        string time    = string.Empty;
                        string comment = string.Empty;
                        
                        List<double> wavelength  = new List<double>();
                        List<double> reflectance = new List<double>();
                        
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line.StartsWith("##END", StringComparison.InvariantCulture))
                            {
                                break;
                            }
                            
                            if (line.StartsWith("##PRODUCT:", StringComparison.InvariantCulture))
                            {
                                product = Foss.GetFieldValue(line);
                                continue;
                            }
                            
                            if (line.StartsWith("##SAMPLE NAME:", StringComparison.InvariantCulture))
                            {
                                sample = Foss.GetFieldValue(line);
                                continue;
                            }
                            
                            if (line.StartsWith("##DATE:", StringComparison.InvariantCulture))
                            {
                                date = Foss.GetFieldValue(line);
                                continue;
                            }
                            
                            if (line.StartsWith("##TIME:", StringComparison.InvariantCulture))
                            {
                                date = Foss.GetFieldValue(line);
                                continue;
                            }
                            
                            if (line.StartsWith("##", StringComparison.InvariantCulture))
                            {
                                if (line.StartsWith("##SPECTRUM:", StringComparison.InvariantCulture))
                                    continue;
                                
                                comment += line + "\n";
                                continue;
                            }
                            
                            if (line.Trim() == string.Empty)
                                continue;
                            
                            string[] s = line.Split(new char[] {','});
                            if (s.Length != 2)
                                throw new FormatException(Catalog.GetString("Cannot read FOSS txt file: Missformed data section."));
                            
                            wavelength.Add(double.Parse(s[0].Trim(), Foss.culture));
                            reflectance.Add(double.Parse(s[1].Trim(), Foss.culture));
                        }
                        
                        if (line == null)
                            break;
                        
                        DataSet reflectance_ds = new DataSet(wavelength, reflectance);
                        DataSet reference_ds   = new DataSet(wavelength, 1.0);
                        DataSet intensity_ds   = reference_ds / DataSet.Pow(10.0, reflectance_ds);

                        Spectrum spectrum   = new Spectrum(intensity_ds, new WavelengthLimits(intensity_ds));
                        Spectrum background = new Spectrum(reference_ds, new WavelengthLimits(reference_ds));

                        spectrum.DarkIntensity = new DarkIntensity(double.Epsilon, 0.0);
                        spectrum.Reference = background;
                        spectrum.Label = $"{sample} ({product})";
                        spectrum.Comment = comment;
                        spectrum.Timestamp = DateTime.Parse($"{time} {date}", Foss.culture);
                        
                        spectra.Add(spectrum);
                    }
                    
                    return spectra.ToArray();
                }
            }
        }
        
        public static string GetFieldValue(string line)
        {
            int ix = line.IndexOf(':');
            if (line.Length < ix + 3)
                return string.Empty;
            
            return line.Substring(ix+2);
        }
    }
}