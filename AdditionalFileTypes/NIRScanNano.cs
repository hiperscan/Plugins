// NIRScanNano.cs created with MonoDevelop
// User: klose at 16:01 11.03.2016
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2016  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

using Hiperscan.Spectroscopy.IO;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public class NIRScanNanoFileTypeInfo : FileTypeInfo
    {
        public NIRScanNanoFileTypeInfo() : base("NIRscan Nano (csv)", new string[] {"csv"})
        {
            this.is_container = false;
        }

        public override Spectrum Read(string fname)
        {
            try
            {
                return NIRScanNano.Read(fname);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw ex;
            }
        }

        public override void WriteContainer(List<Spectrum> spectra, List<MetaData> meta_data, string fname, CultureInfo culture)
        {
            throw new NotImplementedException(Catalog.GetString("Output of NIRScanNano txt format is not implemented yet."));
        }
    }

    public static class NIRScanNano
    {
        private static CultureInfo culture = CultureInfo.InvariantCulture;
        
        public static Spectrum Read(string fname)
        {
            using (Stream stream = new FileStream(fname, FileMode.Open))
            {
                using (TextReader reader = new StreamReader(stream))
                {
                    string line;

                    string method   = string.Empty;
                    DateTime time   = DateTime.MinValue;
                    string version  = string.Empty;
                    double temp     = 0.0;
                    double humidity = 0.0;
                    string lamp_pd  = string.Empty;
                    string shift_coeff = string.Empty;
                    string pixel_coeff = string.Empty;
                    string serial      = string.Empty;
                    string config_name = string.Empty;
                    string config_type = string.Empty;
                    string pattern_width = string.Empty;
                    int average = 0;
                    string pga_gain = string.Empty;
                    string measurement_time = string.Empty;
                    double start_wavelength = double.MinValue;
                    double end_wavelength = double.MaxValue;

                    List<double> wavelength = new List<double>();
                    List<double> absorbance = new List<double>();
                    List<double> intensity  = new List<double>();
                    
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.StartsWith("Method:", StringComparison.InvariantCulture))
                        {
                            method = NIRScanNano.GetFieldValue(line);
                            continue;
                        }
                        
                        if (line.StartsWith("Host Date-Time:", StringComparison.InvariantCulture))
                        {
                            string s = NIRScanNano.GetFieldValue(line);
                            time = DateTime.ParseExact(s, "%d/%M/%y @ %H:%m:%s", NIRScanNano.culture);
                            continue;
                        }
                        
                        if (line.StartsWith("Header Version:", StringComparison.InvariantCulture))
                        {
                            version = NIRScanNano.GetFieldValue(line);
                            continue;
                        }
                        
                        if (line.StartsWith("System Temp hundredths:", StringComparison.InvariantCulture))
                        {
                            temp = double.Parse(NIRScanNano.GetFieldValue(line), NIRScanNano.culture) * 100.0;
                            continue;
                        }

                        if (line.StartsWith("Detector Temp hundredths:", StringComparison.InvariantCulture))
                            continue;

                        if (line.StartsWith("Humidity hundredths:", StringComparison.InvariantCulture))
                        {
                            humidity = double.Parse(NIRScanNano.GetFieldValue(line), NIRScanNano.culture) * 100.0;
                            continue;
                        }

                        if (line.StartsWith("Lamp PD:", StringComparison.InvariantCulture))
                        {
                            lamp_pd = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Shift Vector Coefficients:", StringComparison.InvariantCulture))
                        {
                            shift_coeff = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Pixel to Wavelength Coefficients:", StringComparison.InvariantCulture))
                        {
                            pixel_coeff = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Serial Number:", StringComparison.InvariantCulture))
                        {
                            serial = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Scan Config Name:", StringComparison.InvariantCulture))
                        {
                            config_name = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Scan Config Type:", StringComparison.InvariantCulture))
                        {
                            config_type = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Start wavelength in nm:", StringComparison.InvariantCulture))
                        {
                            start_wavelength = double.Parse(NIRScanNano.GetFieldValue(line), NIRScanNano.culture);
                            continue;
                        }

                        if (line.StartsWith("End wavelength in nm:", StringComparison.InvariantCulture))
                        {
                            end_wavelength = double.Parse(NIRScanNano.GetFieldValue(line), NIRScanNano.culture);
                            continue;
                        }

                        if (line.StartsWith("Pattern Pixel Width:", StringComparison.InvariantCulture))
                        {
                            pattern_width = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Number of Measurement points:", StringComparison.InvariantCulture))
                            continue;

                        if (line.StartsWith("Num Repeats:", StringComparison.InvariantCulture))
                        {
                            average = int.Parse(NIRScanNano.GetFieldValue(line));
                            continue;
                        }

                        if (line.StartsWith("PGA Gain:", StringComparison.InvariantCulture))
                        {
                            pga_gain = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Total Measurement Time (s):", StringComparison.InvariantCulture))
                        {
                            measurement_time = NIRScanNano.GetFieldValue(line);
                            continue;
                        }

                        if (line.StartsWith("Wavelength (nm)", StringComparison.InvariantCulture))
                            continue;
                        
                        if (line.Trim() == string.Empty)
                            continue;

                        if (line.Contains("nan") || line.Contains("inf"))
                            continue;

                        Console.WriteLine(line);

                        string[] ss = line.Split(new char[] {','});
                        if (ss.Length != 4)
                            throw new FormatException(Catalog.GetString("Cannot read NIRscan Nano file: Misformed data section."));

                        wavelength.Add(double.Parse(ss[0].Trim(), NIRScanNano.culture));
                        absorbance.Add(double.Parse(ss[1].Trim(), NIRScanNano.culture));
                        intensity.Add(double.Parse(ss[3].Trim(), NIRScanNano.culture));
                    }

                    Spectrum spectrum = new Spectrum(new WavelengthLimits(start_wavelength, end_wavelength))
                    {
                        CultureInfo  = NIRScanNano.culture,
                        RawIntensity = new DataSet(wavelength, intensity)
                    };

                    double di = double.Epsilon;
                    spectrum.DarkIntensity      = new DarkIntensity(di, double.Epsilon);
                    DataSet raw_absorbance_ds  = new DataSet(wavelength, absorbance);
                    DataSet raw_intensity_di_ds = spectrum.RawIntensity - di;

                    // negative intensity is not defined, zero intensity would be infinite absorbance
                    for (int ix=0; ix < raw_intensity_di_ds.Count; ++ix)
                    {
                        if (raw_intensity_di_ds.Y[ix] < 1e-10)
                            raw_intensity_di_ds.Y[ix] = 1e-10;
                    }

                    DataSet raw_reference_di_ds = DataSet.InverseAbsorbance(raw_intensity_di_ds, raw_absorbance_ds);
                    DataSet raw_reference_ds    = raw_reference_di_ds + di;

                    Spectrum background = new Spectrum(raw_reference_ds, spectrum.WavelengthLimits)
                    {
                        WavelengthCorrection = null,
                        AdcCorrection = null
                    };
                    spectrum.Reference = background;

                    spectrum.Label = Path.GetFileName(fname);
                    spectrum.AverageCount = average;
                    spectrum.Timestamp = time;
                    spectrum.Serial = serial;

                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("Method: {0}\n", method);
                    sb.AppendFormat("Header version: {0}\n", version);
                    sb.AppendFormat("Temperature: {0}\n", temp);
                    sb.AppendFormat("Humidity: {0}\n", humidity);
                    sb.AppendFormat("Lamp PD: {0}\n", lamp_pd);
                    sb.AppendFormat("Shift Coefficient: {0}\n", shift_coeff);
                    sb.AppendFormat("Pixel Coefficient: {0}\n", pixel_coeff);
                    sb.AppendFormat("Configuration Name: {0}\n", config_name);
                    sb.AppendFormat("Configuration Type: {0}\n", config_type);
                    sb.AppendFormat("Pattern Width: {0}\n", pattern_width);

                    return spectrum;
                }
            }
        }
        
        public static string GetFieldValue(string line)
        {
            string[] s = line.Split(',');
            if (s.Length < 2)
                return string.Empty;
            
            return s[1];
        }
    }
}