// AdditionalFileTypes.cs created with MonoDevelop
// User: klose at 17:14 05.11.2012
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

using Hiperscan.Spectroscopy;
using Hiperscan.Spectroscopy.IO;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public class SpcFileTypeInfo : FileTypeInfo
    {
        public SpcFileTypeInfo() : base(Catalog.GetString("THERMO Galactic GRAMS (spc)"), new string[] {"spc"})
        {
            this.rw_type = FileRwType.WriteOnly;
        }
        
        public override void Write(Spectrum spectrum, string fname, CultureInfo culture)
        {
            if (Path.GetExtension(fname).ToLower() != ".spc")
                fname = Path.ChangeExtension(fname, ".spc");
            
            if (spectrum.HasAbsorbance == false)
                throw new NotSupportedException(Catalog.GetString("This implementation of SPC file format supports absorbance spectra only."));
            
            SPC.Write(new List<Spectrum>() { spectrum }, fname);
        }
    }
    
    public class SpcContainerFileTypeInfo : FileTypeInfo
    {
        public SpcContainerFileTypeInfo() : base(Catalog.GetString("THERMO Galactic GRAMS Container (spc)"), new string[] {"spc"})
        {
            this.is_container = true;
            this.rw_type = FileRwType.ReadWrite;
        }
        
        public override Spectrum[] ReadContainer(string fname, out List<MetaData> meta_data)
        {
            Spectrum[] spectra = SPC.Read(fname).ToArray();
            
            FileInfo file = new FileInfo(fname);
            for (int ix=0; ix < spectra.Length; ++ix)
            {
                spectra[ix].Id = "0_" + Path.GetDirectoryName(file.FullName) + 
                                        Path.DirectorySeparatorChar + 
                                        Path.GetFileNameWithoutExtension(file.FullName) + 
                                        ix.ToString();
                spectra[ix].Label = Path.GetFileName(file.FullName) + "#" + ix.ToString();
            }
            
            meta_data = null;
            return spectra;
        }
        
        public override void WriteContainer(List<Spectrum> spectra, List<MetaData> meta_data, string fname, CultureInfo culture)
        {
            if (Path.GetExtension(fname).ToLower() != ".spc")
                fname = Path.ChangeExtension(fname, ".spc");

            foreach (Spectrum spectrum in spectra)
            {
                if (!spectrum.HasAbsorbance)
                    throw new NotSupportedException(Catalog.GetString("This implementation of SPC file format supports absorbance spectra only."));
            }
            SPC.Write(spectra, fname);
        }
    }
}