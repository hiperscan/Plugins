// SPCHeader.cs created with VisualStudio
// User: rudolph at 17:14 05.03.2009
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Matthias Rudolph, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;


namespace Hiperscan.Extensions.AdditionalFileTypes
{
    #region enums
    [Flags]
    public enum SpcSubHeaderFlags
    {
        SUBCHGD = 1, /* Subflgs bit if subfile changed */
        SUBNOPT = 8, /* Subflgs bit if peak table file should not be used */
        SUBMODF = 128, /* Subflgs bit if subfile modified by arithmetic */
    }

    /* for fx-, fz-, fw-axis*/
    public enum SpcAxisTypes
    {
        XARB = 0, /* Arbitrary */
        XWAVEN = 1, /* Wavenumber (cm-=1,) */
        XUMETR = 2, /* Micrometers (um) */
        XNMETR = 3, /* Nanometers (nm) */
        XSECS = 4, /* Seconds */
        XMINUTS = 5, /* Minutes */
        XHERTZ = 6, /* Hertz (Hz) */
        XKHERTZ = 7, /* Kilohertz (KHz) */
        XMHERTZ = 8, /* Megahertz (MHz) */
        XMUNITS = 9, /* Mass (M/z) */
        XPPM = 10, /* Parts per million (PPM) */
        XDAYS = 11, /* Days */
        XYEARS = 12, /* Years */
        XRAMANS = 13, /* Raman Shift (cm-=1,) */
        XEV = 14, /* eV */
        ZTEXTL = 15, /* Custom XYZ label text in fcatxt (=0,x=4,D format only) */
        XDIODE = 16, /* Diode Number */
        XCHANL = 17, /* Channel */
        XDEGRS = 18, /* Degrees */
        XDEGRF = 19, /* Temperature (F) */
        XDEGRC = 20, /* Temperature (C) */
        XDEGRK = 21, /* Temperature (K) */
        XPOINT = 22, /* Data Points */
        XMSEC = 23, /* Milliseconds (mSec) */
        XUSEC = 24, /* Microseconds (uSec) */
        XNSEC = 25, /* Nanoseconds (nSec) */
        XGHERTZ = 26, /* Gigahertz (GHz) */
        XCM = 27, /* Centimeters (cm) */
        XMETERS = 28, /* Meters (m) */
        XMMETR = 29, /* Millimeters (mm) */
        XHOURS = 30, /* Hours */
        XANGST = 31, /* Angstroms (A) */
                     /* 32-254 Not defined */
        XDBLIGM = 255, /* Double interferogram (no label display) */
    }

    public enum SpcYAxisTypes
    {
        YARB = 0, /* Arbitrary Intensity */
        YIGRAM = 1, /* Interferogram */
        YABSRB = 2, /* Absorbance */
        YKMONK = 3, /* Kubelka-Monk */
        YCOUNT = 4, /* Counts */
        YVOLTS = 5, /* Volts */
        YDEGRS = 6, /* Degrees */
        YAMPS = 7, /* Milliamps */
        YMETERS = 8, /* Millimeters */
        YMVOLTS = 9, /* Millivolts */
        YLOGDR = 10, /* Log(=1,/R) */
        YPERCNT = 11, /* Percent */
        YINTENS = 12, /* Intensity */
        YRELINT = 13, /* Relative Intensity */
        YENERGY = 14, /* Energy */
                      /* 15 Reserved - do not use */
        YDECBL = 16, /* Decibel */
        YABUND = 17, /* Abundance */
        YRELABN = 18, /* Relative Abundance */
        YDEGRF = 19, /* Temperature (F) */
        YDEGRC = 20, /* Temperature (C) */
        YDEGRK = 21, /* Temperature (K) */
        YINDRF = 22, /* Index of Refraction [N] */
        YEXTCF = 23, /* Extinction Coeff. [K] */
        YREAL = 24, /* Real */
        YIMAG = 25, /* Imaginary */
        YCMPLX = 26, /* Complex */
        YMGRAM = 27, /* Milligrams */
        YGRAM = 28, /* Grams */
        YKGRAM = 29, /* Kilograms */
        YSROT = 30, /* Specific Rotation (new) */
                    /* 31-127 Not defined */
        YTRANS = 128, /* Transmission (>==128, MUST HAVE VALLEY PEAKS!) */
        YREFLEC = 129, /* Reflectance */
        YVALLEY = 130, /* Single Beam */
        YEMISN = 131, /* Emission */
    }

    /******************************************************************************
    * Possible bit flags settings for FTFLGS flag byte.
    * Note that TRANDM and TORDRD are mutually exclusive.
    * Code depends on TXVALS being the sign bit. TXYXYS must be 0 if TXVALS=0.
    * In 'old' format files (fexper not suported), TCGRAM specifies a chromatogram.
    ******************************************************************************/
    [Flags]
    public enum SpcFTFLGS
    {
        SinglePrecY = 1, /* Single precision (16 bit) Y data if set. */
        EnableFexperOld = 2, /* Enables fexper in older software (CGM if fexper=0) */
        MultiTrace = 4, /* Multiple traces format (set if more than one subfile) */
        TRANDM = 8, /* If TMULTI and TRANDM=1 then arbitrary time (Z) values */
        TORDRD = 16, /* If TMULTI abd TORDRD=1 then ordered but uneven subtimes */
        UseLabels = 32, /* Set if should use fcatxt axis labels, not fxtype etc. */
        MultiFile = 64, /* If TXVALS and multifile, then each subfile has own X's */
        NotEvenlySpacedX = 128, /* Floating X value array preceeds Y's (New format only) */
    }

    public enum SpcInstrumentTechnique
    {
        GeneralSPC = 0, /* General SPC (could be anything) */
        GasChromatogram = 1, /* Gas Chromatogram */
        GeneralChromatogram = 2, /* General Chromatogram (same as SPCGEN with TCGRAM) */
        HPLCChromatogram = 3, /* HPLC Chromatogram */
        FTIRSpectrum = 4, /* FT-IR, FT-NIR, FT-Raman Spectrum or Igram (Can also be used for scanning IR.) */
        NIRSpectrum = 5, /* NIR Spectrum (Usually multi-spectral data sets for calibration.) */
                         /* 6 Reserved - do not use */
        UVVISSpectrum = 7, /* UV-VIS Spectrum (Can be used for single scanning UV-VIS-NIR.) */
        XRaySpectrum = 8, /* X-ray Spectrum (XRD) */
        MassSpectrum = 9, /* Mass Spectrum (Single Continuum/Centroid, GC-MS, LC-MS or TOF.) */
        NMRSpectrum = 10, /* NMR Spectrum or FID */
        RamanSpectrum = 11, /* Raman Spectrum (Usually Diode Array, CCD, etc. use SPCFTIR for FT-Raman.) */
        FluorescenceSpectrum = 12, /* Fluorescence Spectrum */
        AtomicSpectrum = 13, /* Atomic Spectrum */
        ChromatographyDiodeArraySpectra = 14, /* Chromatography Diode Array Spectra */
        ThermalAnalysis = 15, /* Thermal Analysis (TGA, DSC, etc.) */
        CircularDichroism = 16, /* Circular Dichroism */
    }

    //Conventions used for FMODS data modifications flags:
    [Flags]
    public enum SpcDataModFlags
    {
        A = 0x00000002, /* Averaging (from multiple source traces)*/
        B = 0x00000004, /* Baseline correction or offset */
        C = 0x00000008, /* Compute interferogram to spectrum */
        D = 0x00000010, /* Derivative or integral */
        E = 0x00000040, /* Resolution enhancement (i.e. deconvolution) */
        I = 0x00000200, /* Interpolation or deresolution */
        N = 0x00004000, /* Noise reduction smoothing */
        O = 0x00008000, /* Other math functions (add, subtract, noise, etc.) */
        S = 0x00080000, /* Spectral subtraction */
        T = 0x00100000, /* Truncation (portion of original X range remains) */
        W = 0x00800000, /* When collected date/time has been modified */
        X = 0x01000000, /* X unit conversion or X shifting */
        Y = 0x02000000, /* Y unit conversion (transmission->absorbance, etc.) */
        Z = 0x04000000, /* Zap (data features removed or modified) */
    }

    //Headerversion flags
    public enum SpcHeaderVersion
    {
        oldHeader = 0x4D, /* 0x4D=> old format */
        newHeaderLSB = 0x4B, /* 0x4B=> new LSB 1st */
        newHeaderMSB = 0x4C, /* 0x4C=> new MSB 1st */

    }
    #endregion

    /// <summary>
    /// Implements the new SPC file header struct
    /// </summary>
    public class SpcHeader
    {
        public SpcHeader()
        {
            //init the array fields with its standard values
            fresolution = new char[9];
            fsource = new char[9];
            fspare = new float[8];
            fcomment = new char[130];
            fcatxt = new char[30];
            fmethod = new char[48];
            freserv = new byte[187];
        }

        public SpcFTFLGS ftflags; /* Flag bits defined above */
        public SpcHeaderVersion fversion; /* 0x4B=> new LSB 1st, 0x4C=> new MSB 1st, 0x4D=> old format */
        public SpcInstrumentTechnique fexper; /* Instrument technique code (see above) */
        public byte fexponent; /* Fraction scaling exponent integer (80h=>float) */
        public uint fnpoints; /* Integer number of points (or TXYXYS directory position) */
        public double ffirst; /* Floating X coordinate of first point */
        public double flast; /* Floating X coordinate of last point */
        public uint fnsub; /* Integer number of subfiles (1 if not TMULTI) */
        public SpcAxisTypes fxtype; /* Type of X axis units (see definitions above) */
        public SpcYAxisTypes fytype; /* Type of Y axis units (see definitions above) */
        public SpcAxisTypes fztype; /* Type of Z axis units (see definitions above) */
        public byte fpost; /* Posting disposition (see GRAMSDDE.H) */
        public uint fdate; /* Date/Time LSB: min=6b,hour=5b,day=5b,month=4b,year=12b */
        public char[] fresolution; /* Resolution description text (null terminated) */
        public char[] fsource; /* Source instrument description text (null terminated) */
        public ushort fpeakpoint; /* Peak point number for interferograms (0=not known) */
        public float[] fspare; /* Used for Array Basic storage */
        public char[] fcomment; /* Null terminated comment ASCII text string */
        public char[] fcatxt; /* X,Y,Z axis label strings if ftflgs=TALABS */
        public uint flogoff; /* File offset to log block or 0 (see above) */
        public SpcDataModFlags fmods; /* File Modification Flags (see above: 1=A,2=B,4=C,8=D..) */
        public byte fprocs; /* Processing code (see GRAMSDDE.H) */
        public byte flevel; /* Calibration level plus one (1 = not calibration data) */
        public ushort fsampin; /* Sub-method sample injection number (1 = first or only ) */
        public float ffactor; /* Floating data multiplier concentration factor (IEEE-32) */
        public char[] fmethod; /* Method/program/data filename w/extensions comma list */
        public float fzinc; /* Z subfile increment (0 = use 1st subnext-subfirst) */
        public uint fwplanes; /* Number of planes for 4D with W dimension (0=normal) */
        public float fwinc; /* W plane increment (only if fwplanes is not 0) */
        public SpcAxisTypes fwtype; /* Type of W axis units (see definitions above) */
        public byte[] freserv; /* Reserved (must be set to zero) */
    }

    /// <summary>
    /// Implements the SPC subfile header struct
    /// </summary>
    public class SpcSubHeader
    {
        public SpcSubHeader()
        {
            subresv = new byte[4];
        }

        public SpcSubHeaderFlags subflgs; /* Flags as defined above */
        public byte subexp; /* Exponent for sub-file's Y values (80h=>float) */
        public ushort subindx; /* Integer index number of trace subfile (0=first) */
        public float subtime; /* Floating time for trace (Z axis coordinate) */
        public float subnext; /* Floating time for next trace (May be same as subtime) */
        public float subnois; /* Floating peak pick noise level if high byte nonzero */
        public uint subnpts; /* Integer number of subfile points for TXYXYS type */
        public uint subscan; /* Integer number of co-added scans or 0 (for collect) */
        public float subwlevel; /* Floating W axis value (if fwplanes non-zero) */
        public byte[] subresv; /* Reserved area (must be set to zero) */
    }

    public struct SSFSTC /* ftflgs=TXYXYS file directory block format */
    {
        public uint ssfposn; /* disk file position of beginning of subfile (subhdr)*/
        public uint ssfsize; /* byte size of subfile (subhdr+X+Y) */
        public float ssftime; /* floating Z time of subfile (subtime) */
    }
}
