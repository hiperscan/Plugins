// SPC.cs created with VisualStudio
// User: rudolph at 17:14 05.03.2009
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Matthias Rudolph, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

using Hiperscan.Spectroscopy;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public static class SPC
    {
        private enum WriteType
        {
            RawIntensity,
            Absorbance
        }

        private static ushort subfile_number;
        private static DateTime start_time;

        #region Reading
        public static List<Spectrum> Read(String fileName)
        {
            List<Spectrum> result = new List<Spectrum>();

            SpcFile file = new SpcFile();

            file.ReadFile(fileName);

            uint date = file.SpcHeader.fdate;

            int year = (int)(date >> 20) & 0xFFF; //12 bit
            int month = (int)(date >> 16) & 0xF; //4 bit
            int day = (int)(date >> 11) & 0x1F; //5 bit
            int hour = (int)(date >> 6) & 0x1F; //5 bit
            int minute = (int)(date) & 0x3F; //6 bit

            DateTime startTime = new DateTime(year, month, day, hour, minute, 0);

            for (int jdx = 0; jdx < file.SpcHeader.fwplanes; jdx++)
            {
                result.Add(new Spectrum(WavelengthLimits.DefaultWavelengthLimits));
                result[jdx].RawIntensity = file.SubFiles[jdx].Data;
                result[jdx]._SetAbsorbance(file.SubFiles[jdx + (int)file.SpcHeader.fwplanes].Data);

                float span = file.SubFiles[jdx].SpcSubHeader.subtime;

                result[jdx].Timestamp = startTime;
                result[jdx].Timestamp.AddSeconds(span);
            }

            return result;
        }
        #endregion

        #region Writing
        public static void Write(List<Spectrum> specs, String fileName)
        {
            SpcFile file = new SpcFile();
            CreateSPCHeader(file, specs);
            subfile_number = 0;

            start_time = new DateTime(specs[0].Timestamp.Year, specs[0].Timestamp.Month, specs[0].Timestamp.Day, specs[0].Timestamp.Hour, specs[0].Timestamp.Minute, 0);

            foreach (Spectrum spec in specs)
            {
                SPC.CreateSpcSubFile(file, spec, WriteType.RawIntensity);
            }

            foreach (Spectrum spec in specs)
            {
                SPC.CreateSpcSubFile(file, spec, WriteType.Absorbance);
            }

            //set the subfile number
            file.SpcHeader.fnsub = (uint)file.SubFiles.Count;

            file.WriteFile(fileName);
        }

        private static void CreateSpcSubFile(SpcFile file, Spectrum spec, WriteType writeType)
        {
            SPCSubFile subFile = new SPCSubFile();
            SpcSubHeader subHeader = new SpcSubHeader
            {

                //Set the Subfile flags
                subflgs = 0,
                //Data Interpretation
                subexp = 0x80, //data stored as float
                               //Subfile Index
                subindx = subfile_number
            };
            subfile_number++;

            //time of messuring for the z index
            subHeader.subtime = (spec.Timestamp - start_time).Seconds;

            //end time of the messuring
            subHeader.subnext = (spec.Timestamp - start_time).Seconds;

            //internal use
            subHeader.subnois = 0;

            //datapoints of the subfile
            uint datapoints = 0;
            if (writeType == WriteType.Absorbance)
                datapoints = (uint)spec.Absorbance.Count;
            else if (writeType == WriteType.RawIntensity)
                datapoints = (uint)spec.RawIntensity.Count;

            subHeader.subnpts = datapoints;

            //W-axis value
            if (writeType == WriteType.Absorbance)
                subHeader.subwlevel = 0;
            else if (writeType == WriteType.RawIntensity)
                subHeader.subwlevel = 1;

            subFile.SpcSubHeader = subHeader;

            if (writeType == WriteType.Absorbance)
                subFile.Data = spec.Absorbance;
            else if (writeType == WriteType.RawIntensity)
                subFile.Data = spec.RawIntensity;

            file.SubFiles.Add(subFile);
        }

        private static void CreateSPCHeader(SpcFile file, List<Spectrum> specs)
        {
            //Create new Header
            SpcHeader header = new SpcHeader();

            header.ftflags = SpcFTFLGS.MultiFile | SpcFTFLGS.NotEvenlySpacedX | SpcFTFLGS.TORDRD;
            header.fversion = SpcHeaderVersion.newHeaderLSB;
            header.fexper = SpcInstrumentTechnique.NIRSpectrum;
            header.fexponent = 0x80; //Use floating point for writing the y values
            header.fnpoints = 0; //specified by the subfile header
            header.ffirst = 0; //ignored, cause set in the subfile header
            header.flast = 0; //ignored, cause set in the subfile header

            //number of subfiles
            //count multiplied with count of different displayed value types (e.g. absorbance, raw intensity)
            //setting this after creating all subfiles

            //axis labels
            header.fxtype = SpcAxisTypes.XNMETR;
            header.fytype = SpcYAxisTypes.YABSRB;
            header.fztype = SpcAxisTypes.XMINUTS;

            //Post Disposing code
            header.fpost = 0;

            //create SPC formated date
            uint date = 0;
            date += (uint)specs[0].Timestamp.Year;
            date <<= 4;
            date += (uint)specs[0].Timestamp.Month;
            date <<= 5;
            date += (uint)specs[0].Timestamp.Day;
            date <<= 5;
            date += (uint)specs[0].Timestamp.Hour;
            date <<= 6;
            date += (uint)specs[0].Timestamp.Minute;

            header.fdate = date;

            //text with the datapoint resolution
            //up to 8 chars + terminating zero
            header.fresolution = "TestRes".ToCharArray();

            //Interferogram peak point number
            //it should be set to null, sure setting a WORD to null... of cause
            header.fpeakpoint = 0;

            //Array basic value storage
            //header.fspare //leave it null, it's obsolate

            //Comment text, limited to 130 chars (including terminating zero
            if (specs[0].Comment.Length > 129)
                header.fcomment = specs[0].Comment.Substring(0, 129).ToCharArray();
            else
                header.fcomment = specs[0].Comment.ToCharArray();

            //Custom Axis Unit Label Text
            //the spec does not say how to use this
            header.fcatxt = "Value".ToCharArray();

            //File Offset to LOGSTC Structure
            header.flogoff = 0;

            //File Modifier
            header.fmods = 0;

            //Post processing code
            header.fprocs = 0;

            //internal use only
            header.flevel = 0;
            header.fsampin = 0;
            header.ffactor = 0;
            //header.fmethod; //array already set to 0

            //Spacing between files in z order
            header.fzinc = 0;

            //Multifile 4D data number w planes
            //when not 0, groups of subfiles will be interpreted as volume of data
            header.fwplanes = (uint)specs.Count;

            //w planes increment
            header.fwinc = 0;

            //w axis type
            header.fwtype = SpcAxisTypes.XARB;

            file.SpcHeader = header;
        }
        #endregion
    }
}