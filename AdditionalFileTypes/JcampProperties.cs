// JCAMPProperties.cs created with MonoDevelop
// User: klose at 16:38 18.08.2014
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011-2014  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

using Gtk;

using Hiperscan.Spectroscopy;
using Hiperscan.Spectroscopy.IO;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public static class JCAMPProperties
    {
        public static void Assign()
        {
            var dialog = new Dialog(Catalog.GetString("Assign JCAMP Properties"),
                                    Plugin.host.MainWindow, 
                                    DialogFlags.Modal,
                                    Stock.Cancel,
                                    ResponseType.Cancel, 
                                    Stock.Ok,
                                    ResponseType.Ok);

            Table table = new Table(2, 2, false)
            {
                RowSpacing    = 5,
                ColumnSpacing = 10
            };

            Alignment align1 = new Alignment(0f, 0.5f, 0f, 0f)
            {
                new Label(Catalog.GetString("Choose JCAMP directory:"))
            };
            table.Attach(align1, 0, 1, 0, 1, AttachOptions.Fill, AttachOptions.Fill, 0, 0);

            FileChooserButton chooser1 = new FileChooserButton("Choose JCAMP Directory", FileChooserAction.SelectFolder);
            Alignment align2 = new Alignment(0f, 0.5f, 1f, 1f)
            {
                chooser1
            };
            table.Attach(align2, 1, 2, 0, 1, AttachOptions.Fill, AttachOptions.Fill, 0, 0);

            Alignment align3 = new Alignment(0f, 0.5f, 0f, 0f)
            {
                new Label(Catalog.GetString("Choose properties file:"))
            };
            table.Attach(align3, 0, 1, 1, 2, AttachOptions.Fill, AttachOptions.Fill, 0, 0);

            Alignment align4 = new Alignment(0f, 0.5f, 1f, 1f);
            FileChooserButton chooser2 = new FileChooserButton("Choose Properties File", FileChooserAction.Open)
            {
                Filter = new FileFilter()
            };
            chooser2.Filter.AddPattern("*.xlsx");
            chooser2.Filter.AddPattern("*.csv");
            chooser2.Filter.Name = Catalog.GetString("Properties file (XLSX, CSV)");
            align4.Add(chooser2);
            table.Attach(align4, 1, 2, 1, 2, AttachOptions.Fill, AttachOptions.Fill, 0, 0);

            Alignment align5 = new Alignment(0.5f, 0.5f, 0, 0)
            {
                TopPadding    = 10,
                BottomPadding = 10,
                LeftPadding   = 5,
                RightPadding  = 5
            };
            align5.Add(table);

            dialog.VBox.Add(align5);
            dialog.ShowAll();

            ResponseType response = (ResponseType)dialog.Run();
            string dirname = chooser1.Filename;
            string filename = chooser2.Filename;
            dialog.Destroy();

            if (response != ResponseType.Ok)
                return;

            if (string.IsNullOrEmpty(dirname) || Directory.Exists(dirname) == false)
                throw new FileNotFoundException(Catalog.GetString("Invalid JCAMP directory."));

            if (string.IsNullOrEmpty(filename) || File.Exists(filename) == false)
                throw new FileNotFoundException(Catalog.GetString("Invalid property file path."));

            string[] files = Directory.GetFiles(dirname, "*.dx");

            if (files.Length == 0)
                throw new FileNotFoundException(Catalog.GetString("No JCAMP files found in specified directory."));

            List<Property> properties;
            if (Path.GetExtension(filename).ToLower() == ".csv")
                properties = JCAMPProperties.ReadPropertiesFileCsv(filename);
            else
                throw new NotImplementedException(Catalog.GetString("Xlsx support is not implemented (yet)."));
//                properties = JCAMPProperties.ReadPropertiesFileXlsx(filename);

            dialog = new Dialog(Catalog.GetString("Assign Properties to JCAMP Files..."),
                                Plugin.host.MainWindow,
                                DialogFlags.Modal);
            ProgressBar progress = new ProgressBar();
            Alignment align6 = new Alignment(0.5f, 0.5f, 0, 0)
            {
                TopPadding    = 5,
                BottomPadding = 5,
                LeftPadding   = 5,
                RightPadding  = 5
            };
            align6.Add(progress);

            dialog.VBox.Add(align6);

            dialog.ShowAll();
            Plugin.host.ProcessEvents();

            for (int ix=0; ix < files.Length; ++ix)
            {
                progress.Fraction = (double)ix/(double)files.Length;
                Plugin.host.ProcessEvents();

                string file = files[ix];
                string series_title;
                Spectrum[] spectra;

                try
                {
                    spectra = Jcamp.Read(file, out series_title);
                }
                catch (Exception ex)
                {
                    throw new Exception(Catalog.GetString("Cannot read JCAMP file:") + " " + ex.Message, ex);
                }

                List<MetaData> meta_data = new List<MetaData>
                {
                    new MetaData("Title", MetaData.SerializeString(series_title))
                };
                Jcamp.Write(new List<Spectrum>(spectra), meta_data, properties, file);
            }

            dialog.Destroy();
        }

        public static List<Property> ReadPropertiesFileCsv(string fname)
        {
            List<Property> properties = new List<Property>();

            List<string> names = new List<string>();
            List<string> units = new List<string>();

            using (StreamReader reader = new StreamReader(fname))
            {
                char[] separator = null;
                CultureInfo culture = CultureInfo.InvariantCulture;
                while (reader.EndOfStream == false)
                {
                    string line = reader.ReadLine();

                    if (separator == null)
                    {
                        separator = new char[] {line[0]};

                        switch (separator[0])
                        {

                        case ',':
                            culture = new CultureInfo("en-US");
                            break;

                        case ';':
                            culture = new CultureInfo("de-DE");
                            break;

                        default:
                            throw new FormatException(Catalog.GetString("Cannot read properties file: Invalid format or separator character."));

                        }
                    }

                    string[] s;

                    if (names.Count == 0)
                    {
                        s = line.Split(separator);
                        if (s.Length < 2)
                            throw new FormatException(Catalog.GetString("Cannot read properties file: Invalid format (less than 2 rows in table)."));

                        for (int ix=1; ix < s.Length; ++ix)
                        {
                            names.Add(s[ix].Trim(new char[]{'\"'}).Replace(",", "_"));
                        }
                        continue;
                    }

                    s = line.Split(separator);

                    if (s.Length != names.Count+1)
                        throw new FormatException(Catalog.GetString("Cannot read properties file: Invalid format (inconsistent row count)."));

                    if (units.Count == 0 && properties.Count == 0 && string.IsNullOrEmpty(s[0]))
                    {
                        for (int ix=1; ix < s.Length; ++ix)
                        {
                            units.Add(s[ix].Trim(new char[]{'\"', '[', ']'}));
                        }
                        continue;
                    }

                    for (int ix=0; ix < names.Count; ++ix)
                    {
                        if (ix >= units.Count)
                            break;
                        names[ix] += $" ({units[ix]})";
                    }

                    for (int ix=0; ix < names.Count; ++ix)
                    {
                        List<int> name_duplicates = new List<int>();
                        for (int jx=0; jx < names.Count; ++jx)
                        {
                            if (names[ix] == names[jx])
                                name_duplicates.Add(jx);
                        }

                        if (name_duplicates.Count == 1)
                            continue;

                        List<int> unit_duplicates = new List<int>();
                        for (int jx=0; jx < name_duplicates.Count; ++jx)
                        {
                            for (int kx=0; kx < name_duplicates.Count; ++kx)
                            {
                                if (units[jx] == units[kx])
                                    unit_duplicates.Add(kx);
                            }
                        }

                        if (unit_duplicates.Count == name_duplicates.Count)
                        {
                            foreach (int jx in name_duplicates)
                            {
                                names[jx] = $"{names[jx]} ({units[jx]})";
                            }
                        }
                        else
                        {
                            int suffix = 0;
                            foreach (int jx in name_duplicates)
                            {
                                names[jx] = $"{names[jx]} ({(++suffix).ToString()})";
                            }
                        }
                    }

                    string id = s[0];

                    if (string.IsNullOrEmpty(id))
                        throw new FormatException(Catalog.GetString("Cannot read properties file: Missing entry in ID column."));

                    if (properties.Count == 0)
                    {
                        for (int ix=0; ix < names.Count; ++ix)
                        {
                            if (units.Count == 0)
                                properties.Add(new Property(names[ix]));
                            else
                                properties.Add(new Property(names[ix], units[ix]));
                        }
                    }

                    for (int ix=1; ix < s.Length; ++ix)
                    {
                        string _s = s[ix].Trim(new char[]{'\"'});
                        if (string.IsNullOrEmpty(_s))
                            properties[ix-1].Add(id, 0.0);
                        else
                            properties[ix-1].Add(id, double.Parse(_s, culture));
                    }
                }
            }

            List<Property> _properties = new List<Property>();
            foreach (Property property in properties)
            {
                if (string.IsNullOrEmpty(property.Name.Trim()))
                    continue;
                _properties.Add(property);
            }

            return _properties;
        }

        private static string[] RowToArray(System.Data.DataRow row)
        {
            List<string> s = new List<string>();

            for (int ix=0; ix < row.Table.Columns.Count; ++ix)
            {
                s.Add(row[ix].ToString());
            }

            return s.ToArray();
        }
    }
}