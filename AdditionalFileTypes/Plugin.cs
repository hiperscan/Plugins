using System;
using System.Reflection;

using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public class Plugin : Hiperscan.Extensions.IPlugin
    {
        public static readonly string name        = Catalog.GetString("Additional File Types");
        public static readonly string version     = "1.0.0";
        public static readonly string vendor      = "Hiperscan GmbH";
        public static readonly string description = Catalog.GetString("Provides support for additional file types");
        public static readonly string guid        = "0c4a862b-990f-46fc-b2d8-3d9439f1f9d9";
        
        internal static IHost host;

        public Plugin ()
        {
            this.FileTypeInfos = new FileTypeInfo[]
            {
                new SpcFileTypeInfo(), 
                new SpcContainerFileTypeInfo(), 
                new FossContainerFileTypeInfo(),
                new DptFileTypeInfo(),
                new JcampFileTypeInfo(),
                new JcampContainerFileTypeInfo(),
                new NIRScanNanoFileTypeInfo()
            };

            MenuInfo menu_info1 = new MenuInfo(Catalog.GetString("Assign JCAMP Properties..."));
            menu_info1.Invoked += (sender, e) => JCAMPProperties.Assign();
            this.MenuInfos = new MenuInfo[] { menu_info1 };
        }

        public void ReplaceGui()
        {
        }

        public void Initialize(IHost host)
        {
            Plugin.host = host;
        }

        public MenuInfo[]     MenuInfos     { get; }
        public FileTypeInfo[] FileTypeInfos { get; }

        public bool Active { get; set; } = true;

        public ToolbarInfo[] ToolbarInfos   => null;
        public PlotTypeInfo[] PlotTypeInfos => null;
        public string Name                  => Plugin.name;
        public string Version               => Plugin.version;
        public string Vendor                => Plugin.vendor;
        public Assembly Assembly            => Assembly.GetExecutingAssembly();
        public string Description           => Plugin.description;
        public Guid Guid                    => new Guid(Plugin.guid);
        public bool CanReplaceGui           => false;
    }
}