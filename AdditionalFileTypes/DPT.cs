// DPT.cs created with MonoDevelop
// User: klose at 13:51 16.12.2013
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2013  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

using Hiperscan.Spectroscopy;
using Hiperscan.Spectroscopy.IO;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public class DptFileTypeInfo : FileTypeInfo
    {
        public DptFileTypeInfo() : base("Bruker Opus (dpt)", new string[] {"dpt"})
        {
            this.is_container = false;
        }
        
        public override Spectrum Read(string fname)
        {
            return DPT.Read(fname);
        }
        
        public override void WriteContainer(List<Spectrum> spectra, List<MetaData> meta_date, string fname, CultureInfo culture)
        {
            throw new NotImplementedException(Catalog.GetString("Output of DTP format is not implemented yet."));
        }
    }
    
    public static class DPT
    {
        private static CultureInfo culture = CultureInfo.InvariantCulture;
        
        public static Spectrum Read(string fname)
        {
            using (Stream stream = new FileStream(fname, FileMode.Open))
            {
                using (TextReader reader = new StreamReader(stream))
                {
                    string line;

                    List<double> wavelength = new List<double>();
                    List<double> extinction = new List<double>();

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Trim() == string.Empty)
                            continue;

                        string[] s = line.Split(new char[] {',', ' '});
                        if (s.Length != 2)
                            throw new FormatException(Catalog.GetString("Cannot read DPT file: Misformed data section."));

                        wavelength.Add(DPT.WavecountToWavelength(double.Parse(s[0].Trim(), DPT.culture)));
                        extinction.Add(double.Parse(s[1].Trim(), DPT.culture));
                    }

                    DataSet extinction_ds = new DataSet(wavelength, extinction);
                    DataSet reference_ds  = new DataSet(wavelength, 1.0);
                    DataSet intensity_ds  = reference_ds * (1 - extinction_ds);
                    Spectrum spectrum  = new Spectrum(intensity_ds, new WavelengthLimits(intensity_ds));
                    Spectrum reference = new Spectrum(reference_ds, new WavelengthLimits(reference_ds));

                    spectrum.DarkIntensity = new DarkIntensity(double.Epsilon, 0.0);
                    spectrum.Reference = reference;
                    spectrum.Label = Path.GetFileName(fname);
                    FileInfo file_info = new FileInfo(fname);
                    spectrum.Timestamp = file_info.CreationTime;

                    return spectrum;
                }
            }
        }

        internal static double WavecountToWavelength(double wl)
        {
            return 1e7 / wl;
        }
    }
}