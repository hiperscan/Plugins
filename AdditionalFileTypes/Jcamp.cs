// JCAMP.cs created with MonoDevelop
// User: klose at 17:14 05.11.2012
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2011  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// User: hoepfner at 06.03.2020
// FitDateFormat, FitTimeFormat, FitXUnits, FitYUnits // #34117
// add LONGDATE at writing and reading of DX files

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

using Hiperscan.Spectroscopy;
using Hiperscan.Spectroscopy.IO;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public class JcampFileTypeInfo : FileTypeInfo
    {
        public JcampFileTypeInfo() : base("JCAMP (dx)", new string[] {"dx", "jdx"})
        {
            this.is_container = false;
            this.rw_type = FileRwType.WriteOnly;
        }
        
        public override void Write(Spectrum spectrum, string fname, CultureInfo culture)
        {
            Jcamp.Write(new List<Spectrum>() { spectrum }, null, fname);
        }
    }

    public class JcampContainerFileTypeInfo : FileTypeInfo
    {
        public JcampContainerFileTypeInfo() : base("JCAMP (dx)", new string[] {"dx", "jdx"})
        {
            this.is_container = true;
        }
        
        public override Spectrum[] ReadContainer(string fname, out List<MetaData> meta_data)
        {
            meta_data = null;
            return Jcamp.Read(fname);
        }
        
        public override void WriteContainer(List<Spectrum> spectra, List<MetaData> meta_data, string fname, CultureInfo culture)
        {
            if (Path.GetExtension(fname).ToLower() != ".dx")
                fname = Path.ChangeExtension(fname, ".dx");

            Jcamp.Write(spectra, meta_data, fname);
        }
    }

    public class Property : System.Collections.Generic.Dictionary<string,double>
    {
        public string Name { get; private set; }
        public string Unit { get; private set; }


        public Property(string name, string unit) : base()
        {
            this.Name = name;
            this.Unit = unit;
        }

        public Property(string name) : this(name, "?")
        {
        }
    }

    public static class Jcamp
    {
        private static CultureInfo culture = CultureInfo.InvariantCulture;


        public static Spectrum[] Read(string fname)
        {
            return Jcamp.Read(fname, out string title);
        }

        public static Spectrum[] Read(string fname, out string series_title)
        {
            string content;
            using (Stream stream = new FileStream(fname, FileMode.Open))
            {
                using (TextReader reader = new StreamReader(stream))
                {
                    content = reader.ReadToEnd();
                }
            }

            string[] fields = Regex.Split(content, @"^\#\#", RegexOptions.Multiline);

            series_title = string.Empty;
            uint series_count = 1;

            string title = string.Empty;
            string data_type = string.Empty;
            string origin = string.Empty;
            string date = string.Empty;
            string time = string.Empty;
            string description = string.Empty;
            string spectrometer = string.Empty;
            string xunits = string.Empty;
            string yunits = string.Empty;
            double xfactor = 1.0;
            double yfactor = 1.0;
            uint npoints = 0;
            string concentrations = string.Empty;
            double deltax = double.NaN;
            string comment = string.Empty;
            List<double> wavelengths = new List<double>();
            List<double> absorbance = new List<double>();

            int depth = 0;
            List<Spectrum> spectra = new List<Spectrum>();

            foreach (string field in fields)
            {
                if (string.IsNullOrEmpty(field))
                    continue;

                string[] s = field.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);

                if (s.Length != 2)
                    throw new FormatException(Catalog.GetString("Invalid JCAMP data field."));

                string field_name = s[0].Trim();
                string field_data = s[1].Trim(new char[] { ' ', '\t', '\n', '\r' });

                switch (field_name)
                {

                case "TITLE":
                    if (depth != 0 && data_type == "LINK")
                        series_title = title;
                    title = field_data;
                    ++depth;
                    break;

                case "DATA TYPE":
                    data_type = field_data;
                    break;

                case "BLOCKS":
                    series_count = uint.Parse(field_data, culture);
                    break;

                case "ORIGIN":
                    origin = field_data;
                    break;

                case "DATE":
                    date = Jcamp.FitDateFormat(field_data);
                    break;
                case "LONGDATE":
                    date = Jcamp.FitDateFormat(field_data);
                    break;

                case "TIME":
                    time = Jcamp.FitTimeFormat(field_data);
                    break;

                case "SAMPLE DESCRIPTION":
                    description = field_data;
                    break;

                case "SPECTROMETER/DATA SYSTEM":
                    spectrometer = field_data;
                    break;

                case "XUNITS":
                    //if (field_data != "1/CM" && field_data != "NANOMETERS")
                    //     throw new NotSupportedException(string.Format(Catalog.GetString("JCAMP format ##XUNITS={0} is not supported yet."), field_data));
                    xunits = Jcamp.FitXUnits(field_data);
                    break;

                case "YUNITS":
                    //if (field_data != "REFLECTANCE" && field_data != "ABSORBANCE")
                    //     throw new NotSupportedException(string.Format(Catalog.GetString("JCAMP format ##YUNITS={0} is not supported yet."), field_data));
                    yunits = Jcamp.FitYUnits(field_data);
                    break;

                case "XFACTOR":
                    xfactor = double.Parse(field_data, Jcamp.culture);
                    break;

                case "YFACTOR":
                    yfactor = double.Parse(field_data, Jcamp.culture);
                    break;

                case "NPOINTS":
                    npoints = uint.Parse(field_data, Jcamp.culture);
                    break;

                case "CONCENTRATIONS":
                    concentrations = field_data;
                    break;

                case "DELTAX":
                    deltax = double.Parse(field_data, Jcamp.culture);
                    break;

                case "XYDATA":
                    string[] xy_lines = field_data.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string _xy_line in xy_lines)
                    {
                        string xy_line = _xy_line.Trim(new char[] { ' ', '\t', '\r' });

                        if (xy_line == "(X++(Y..Y))")
                            continue;

                        if (string.IsNullOrEmpty(xunits))
                            throw new FormatException(Catalog.GetString("Missing ##XUNITS definition."));

                        string[] values = xy_line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                        if (values.Length < 2)
                            throw new FormatException(Catalog.GetString("Invalid ##XYDATA format."));

                        bool is_absorbance = yunits == "ABSORBANCE";

                        if (xunits == "NANOMETERS")
                        {
                            double current_scaled_wavelength = double.Parse(values[0], Jcamp.culture);
                            wavelengths.Add(current_scaled_wavelength * xfactor);

                            if (is_absorbance)
                                absorbance.Add(double.Parse(values[1], Jcamp.culture) * yfactor);
                            else
                                absorbance.Add(Math.Log10(1.0 / (double.Parse(values[1], Jcamp.culture) * yfactor)));

                            for (int ix = 2; ix < values.Length; ++ix)
                            {
                                wavelengths.Add((current_scaled_wavelength + (double)(ix - 1) * deltax) * xfactor);

                                if (is_absorbance)
                                    absorbance.Add(double.Parse(values[ix], culture) * yfactor);
                                else
                                    absorbance.Add(Math.Log10(1.0 / (double.Parse(values[ix], culture) * yfactor)));
                            }
                        }
                        else
                        {
                            double current_scaled_wavenumber = double.Parse(values[0], Jcamp.culture);
                            wavelengths.Add(1e7 / current_scaled_wavenumber * xfactor);

                            if (is_absorbance)
                                absorbance.Add(double.Parse(values[1], Jcamp.culture) * yfactor);
                            else
                                absorbance.Add(Math.Log10(1.0 / (double.Parse(values[1], Jcamp.culture) * yfactor)));

                            for (int ix = 2; ix < values.Length; ++ix)
                            {
                                wavelengths.Add(1e7 / ((current_scaled_wavenumber + (double)(ix - 1) * deltax) * xfactor));

                                if (is_absorbance)
                                    absorbance.Add(double.Parse(values[ix], Jcamp.culture) * yfactor);
                                else
                                    absorbance.Add(Math.Log10(1.0 / (double.Parse(values[ix], Jcamp.culture) * yfactor)));
                            }
                        }
                    }
                    break;

                case "END":
                    if (depth == 0)
                        throw new FormatException(Catalog.GetString("Unexpected ##END field."));
                    if (absorbance.Count > 0)
                    {
                        if (absorbance.Count != npoints)
                            throw new FormatException(Catalog.GetString("Inconsistent ##XYDATA count."));

                        DataSet absorbance_ds = new DataSet(wavelengths, absorbance);
                        DataSet reference_ds  = new DataSet(wavelengths, 1.0);
                        DataSet intensity_ds  = reference_ds / DataSet.Pow(10.0, absorbance_ds);

                        intensity_ds.Sort();
                        reference_ds.Sort();

                        Spectrum reference = new Spectrum(reference_ds, new WavelengthLimits(reference_ds));

                        Spectrum spectrum = new Spectrum(intensity_ds, new WavelengthLimits(intensity_ds))
                        {
                            DarkIntensity = new DarkIntensity(double.Epsilon, 0.0),
                            Reference = reference,
                            Label = Regex.Replace(title, @"\$\$.*", string.Empty).Trim(),
                            Comment = string.Format("SAMPLE DESCRIPTION: {0}\nCONCENTRATIONS: {1}\nSPECTROMETER: {2}\nORIGIN: {3}\nCOMMENT: {4}",
                                                    description, concentrations, spectrometer, origin, comment),
                            Timestamp = DateTime.ParseExact(FitTimeFormat(time) + " " + FitDateFormat(date), "HH:mm:ss yyyy/MM/dd", Jcamp.culture)
                        };
                        spectra.Add(spectrum);

                        wavelengths.Clear();
                        absorbance.Clear();
                        title = string.Empty;
                        origin = string.Empty;
                        date = string.Empty;
                        time = string.Empty;
                        description = string.Empty;
                        spectrometer = string.Empty;
                        xunits = string.Empty;
                        yunits = string.Empty;
                        xfactor = 1.0;
                        yfactor = 1.0;
                        npoints = 0;
                    }
                    --depth;
                    break;
                }
            }

            if (depth > 0)
                throw new FormatException(Catalog.GetString("Missing ##END field."));

            if (spectra.Count != series_count)
                throw new FormatException(Catalog.GetString("Inconsistent BLOCKS count."));

            return spectra.ToArray();
        }

        public static void Write(List<Spectrum> spectra, List<MetaData> meta_data, string fname)
        {
            Jcamp.Write(spectra, meta_data, null, fname);
        }

        public static void Write(List<Spectrum> spectra, List<MetaData> meta_data, List<Property> properties, string fname)
        {
            using (TextWriter writer = new StreamWriter(fname))
            {
                Jcamp.Write(spectra, meta_data, properties, writer);
            }
        }

        public static void Write(List<Spectrum> spectra, List<MetaData> meta_data, List<Property> properties, TextWriter writer)
        {
            bool multi_mode = (meta_data != null && spectra.Count > 1);

            int spectrum_count = 0;
            foreach (Spectrum spectrum in spectra)
            {
                if (spectrum.HasAbsorbance)
                    ++spectrum_count;
            }

            if (spectrum_count < 1)
                throw new ArgumentException(Catalog.GetString("At least one absorbance spectrum is needed to write a JCAMP file."));

            string series_name = Catalog.GetString("Unknown Name");
            if (meta_data != null)
            {
                foreach (MetaData md in meta_data)
                {
                    if (md.Type != "Title")
                        continue;
                    series_name = MetaData.DeserializeString(md.Data);
                }
            }

            string origin = string.Format("{0} {1}",
                                          Assembly.GetEntryAssembly().GetName().Name.Replace(".GUI", string.Empty),
                                          Assembly.GetEntryAssembly().GetName().Version.ToString());

            if (multi_mode)
            {
                writer.WriteLine("##TITLE= {0}", series_name);
                writer.WriteLine("##JCAMP-DX= 4.24");
                writer.WriteLine("##DATA TYPE= LINK");
                writer.WriteLine("##BLOCKS= {0}", spectra.Count);
                writer.WriteLine("##ORIGIN= {0}", origin);
                writer.WriteLine("##OWNER= ");
                // #34117
                writer.WriteLine("##DATE= {0}", DateTime.Now.ToString("yy/MM/dd", Jcamp.culture));
                writer.WriteLine("##LONGDATE= {0}", DateTime.Now.ToString("yyyy/MM/dd", Jcamp.culture));
                writer.WriteLine("##TIME= {0}", DateTime.Now.ToString("HH:mm:ss", Jcamp.culture));
            }

            bool properties_written = false;

            foreach (Spectrum s in spectra)
            {
                if (s.HasAbsorbance == false)
                    continue;

                Spectrum spectrum = s.Clone();
                spectrum.ApplyCorrectionsToRawData(CorrectionMask.All);
                DataSet absorbance = spectrum.Absorbance;
                double deltax = absorbance.X[1] - absorbance.X[0];

                string sample_id = series_name;
                if (s.Label.Contains("_"))
                    sample_id = s.Label.Split(new char[] { '_' })[0];

                writer.WriteLine("##TITLE= {0}", spectrum.Label);
                writer.WriteLine("##JCAMP-DX= 4.24");
                writer.WriteLine("##DATA TYPE= INFRARED SPECTRUM");
                writer.WriteLine("##ORIGIN= {0}", origin);
                writer.WriteLine("##OWNER= ");
                // #34117
                writer.WriteLine("##DATE= {0}", spectrum.Timestamp.ToString("yy/MM/dd", Jcamp.culture));
                writer.WriteLine("##LONGDATE= {0}", spectrum.Timestamp.ToString("yyyy/MM/dd", Jcamp.culture));
                writer.WriteLine("##TIME= {0}", spectrum.Timestamp.ToString("HH:mm:ss", Jcamp.culture));
                writer.WriteLine("##SAMPLE DESCRIPTION= {0}", sample_id);
                writer.WriteLine("##SPECTROMETER/DATA SYSTEM= Hiperscan SGS/Hiperscan Class Library");
                writer.WriteLine("##XUNITS= NANOMETERS");
                writer.WriteLine("##YUNITS= ABSORBANCE");
                writer.WriteLine("##XFACTOR= 1");
                writer.WriteLine("##YFACTOR= 0.000000100");
                writer.WriteLine("##FIRSTX= {0}", absorbance.X[0].ToString("f0", Jcamp.culture));
                writer.WriteLine("##LASTX= {0}", absorbance.X[absorbance.Count - 1].ToString("f0", Jcamp.culture));
                writer.WriteLine("##FIRSTY= {0}", absorbance.Y[0].ToString("f6", Jcamp.culture));
                writer.WriteLine("##MINY= {0}", absorbance.YMin().ToString("f6", Jcamp.culture));
                writer.WriteLine("##MAXY= {0}", absorbance.YMax().ToString("f6", Jcamp.culture));
                writer.WriteLine("##NPOINTS= {0}", absorbance.Count);
                writer.WriteLine("##CONCENTRATIONS= (NCU)");

                if (properties != null && properties_written == false)
                {
                    foreach (Property property in properties)
                    {
                        string id = spectrum.Label.Split(new char[] { '_' })[0];
                        if (property.ContainsKey(id) == false)
                        {
                            string msg = string.Format("Cannot write JCAMP file: No properties (responses) found for sample ID {0}.", id);
                            throw new KeyNotFoundException(Catalog.GetString(msg));
                        }

                        if (double.IsNaN(property[id]))
                            continue;

                        writer.WriteLine("(<{0}>, {1,10}, {2})", 
                                         property.Name, property[id].ToString("f6", CultureInfo.InvariantCulture), 
                                         property.Unit);
                    }

                    properties_written = true;
                }

                writer.WriteLine("##DELTAX= {0}", deltax.ToString("f0", Jcamp.culture));
                writer.WriteLine("##XYDATA= (X++(Y..Y))");

                for (int ix = 0; ix < absorbance.Count; ++ix)
                {
                    if (ix % 6 == 0)
                        writer.Write("{0,4}", absorbance.X[ix].ToString("f0", Jcamp.culture));

                    writer.Write(" {0,10}", (absorbance.Y[ix] * 1e7).ToString("f0", Jcamp.culture));

                    if (ix % 6 == 5)
                        writer.WriteLine();
                }

                if (absorbance.Count % 6 != 0)
                    writer.WriteLine();

                writer.WriteLine("##END=");
            }

            if (multi_mode)
                writer.WriteLine("##END=");

            writer.Flush();
        }

        private static string FitDateFormat(string value)
        {
            // #34117
            String dt = value;
            // Debug.WriteLine(dt);
            DateTime ts;
            try
            {
                switch (value.Length)
                {
                    case 8:
                        int yr = int.Parse(value.Substring(0, 2));
                        if (yr > 70)
                            dt = "19" + value;
                        else
                            dt = "20" + value;
                        ts = DateTime.ParseExact(dt, "yyyy/MM/dd", Jcamp.culture);
                        break;
                    case 10:
                        if (dt.IndexOf('/') == 2)
                            ts = DateTime.ParseExact(dt, "dd/MM/yyyy", Jcamp.culture);
                        else
                            ts = DateTime.ParseExact(dt, "yyyy/MM/dd", Jcamp.culture);
                        break;
                    default:
                        ts = DateTime.Parse(dt);
                        break;
                }
                dt = ts.ToString("yyyy/MM/dd", Jcamp.culture);
            }
            catch
            {
                //Debug.WriteLine(time + " " + dt);
                dt = "1970/01/01";
            }

            return dt;
        }

        private static string FitTimeFormat(string value)
        {

            // #34117
            string dt = value;
            // Debug.WriteLine(dt);
            DateTime ts;
            try
            {
                switch (value.Length)
                {
                    case 8:
                        ts = DateTime.ParseExact(dt, "HH:mm:ss", Jcamp.culture);
                        break;
                    default:
                        ts = DateTime.Parse(dt, Jcamp.culture);
                        dt = ts.ToString("HH:mm:ss", Jcamp.culture);
                        break;
                }
            }
            catch
            {
                dt = "00:00:00";
            }
            return dt;

        }

        private static string FitXUnits(string value)
        {
            // #30794
            string fxd = value;
            switch (fxd)
            {
                case "1/CM":
                    break;
                case "NANOMETERS":
                case "NANOMETER":
                case "Wavelength":
                    fxd = "NANOMETERS";
                    break;
                default:
                    throw new NotSupportedException(
                      string.Format(Catalog.GetString("JCAMP format ##XUNITS={0} is not supported yet."), value));
            }
            return fxd;
        }

        private static string FitYUnits(string value)
        {
            // #30794
            string fyd = value;
            switch (fyd)
            {
                case "Tansmittance":
                case "TRANSMITTANCE":
                    fyd = "TRANSFLECTANCE";
                    break;

                case "REFLECTANCE":
                case "ABSORBANCE":
                    break;
                case "Absorbance":
                    fyd = "ABSORBANCE";
                    break;
                case "Reflectance":
                    fyd = "REFLECTANCE";
                    break;
                default:
                    fyd = "REFLECTANCE";
                    break;
            }
            return fyd;
        }
    }
}