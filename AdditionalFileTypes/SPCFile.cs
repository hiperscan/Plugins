// SPCFile.cs created with VisualStudio
// User: rudolph at 17:14 05.03.2009
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2009  Matthias Rudolph, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.IO;

using Hiperscan.Spectroscopy;


namespace Hiperscan.Extensions.AdditionalFileTypes
{

    public class SpcFile
    {
        public SpcFile()
        {
            this.SubFiles = new List<SPCSubFile>();
        }

        #region Writing
        public void WriteFile(string fileName)
        {
            FileStream fs = File.Open(fileName, FileMode.Create);

            //Write Header
            Write(fs, (byte)SpcHeader.ftflags);
            Write(fs, (byte)SpcHeader.fversion);
            Write(fs, (byte)SpcHeader.fexper);
            Write(fs, SpcHeader.fexponent);
            Write(fs, SpcHeader.fnpoints);
            Write(fs, SpcHeader.ffirst);
            Write(fs, SpcHeader.flast);
            Write(fs, SpcHeader.fnsub);
            Write(fs, (byte)SpcHeader.fxtype);
            Write(fs, (byte)SpcHeader.fytype);
            Write(fs, (byte)SpcHeader.fztype);
            Write(fs, SpcHeader.fpost);
            Write(fs, SpcHeader.fdate);
            Write(fs, SpcHeader.fresolution, 9);
            Write(fs, SpcHeader.fsource, 9);
            Write(fs, SpcHeader.fpeakpoint);
            Write(fs, SpcHeader.fspare, 8);
            Write(fs, SpcHeader.fcomment, 130);
            Write(fs, SpcHeader.fcatxt, 30);
            Write(fs, SpcHeader.flogoff);
            Write(fs, (uint)SpcHeader.fmods);
            Write(fs, SpcHeader.fprocs);
            Write(fs, SpcHeader.flevel);
            Write(fs, SpcHeader.fsampin);
            Write(fs, SpcHeader.ffactor);
            Write(fs, SpcHeader.fmethod, 48);
            Write(fs, SpcHeader.fzinc);
            Write(fs, SpcHeader.fwplanes);
            Write(fs, SpcHeader.fwinc);
            Write(fs, (byte)SpcHeader.fwtype);
            Write(fs, SpcHeader.freserv, 187);


            foreach (SPCSubFile subFile in SubFiles)
            {
                //Write Subheader
                Write(fs, (byte)subFile.SpcSubHeader.subflgs);
                Write(fs, subFile.SpcSubHeader.subexp);
                Write(fs, subFile.SpcSubHeader.subindx);
                Write(fs, subFile.SpcSubHeader.subtime);
                Write(fs, subFile.SpcSubHeader.subnext);
                Write(fs, subFile.SpcSubHeader.subnois);
                Write(fs, subFile.SpcSubHeader.subnpts);
                Write(fs, subFile.SpcSubHeader.subscan);
                Write(fs, subFile.SpcSubHeader.subwlevel);
                Write(fs, subFile.SpcSubHeader.subresv, 4);

                //Write X-Data
                foreach (Double d in subFile.Data.X)
                {
                    float f = Convert.ToSingle(d);
                    Write(fs, f);
                }

                //Write Y-Data
                foreach (Double d in subFile.Data.Y)
                {
                    float f = Convert.ToSingle(d);
                    Write(fs, f);
                }
            }

            fs.Close();
        }

        private void Write(FileStream fs, byte[] p, int c)
        {
            for (int idx = 0; idx < c; idx++)
            {
                if (idx < p.Length)
                    Write(fs, p[idx]);
                else
                    Write(fs, ((byte)0));
            }
        }

        private void Write(FileStream fs, float[] p, int c)
        {
            for (int idx = 0; idx < c; idx++)
            {
                if (idx < p.Length)
                    Write(fs, p[idx]);
                else
                    Write(fs, ((float)0));
            }
        }

        private void Write(FileStream fs, char[] p, int c)
        {
            for (int idx = 0; idx < c; idx++)
            {
                if (idx < p.Length)
                    Write(fs, p[idx]);
                else
                    Write(fs, ((char)'\0'));
            }
        }

        private unsafe void Write(FileStream fs, float p)
        {
            byte[] b = new byte[4];
            float* ptrFloat = &p;

            fixed (byte* ptrByte = b)
            {
                float* write = (float*)ptrByte;
                *write = *ptrFloat;
            }

            fs.WriteByte(b[0]);
            fs.WriteByte(b[1]);
            fs.WriteByte(b[2]);
            fs.WriteByte(b[3]);
        }

        private unsafe void Write(FileStream fs, double p)
        {
            byte[] b = new byte[8];
            double* ptrDouble = &p;

            fixed (byte* ptrByte = b)
            {
                double* write = (double*)ptrByte;
                *write = *ptrDouble;
            }

            fs.WriteByte(b[0]);
            fs.WriteByte(b[1]);
            fs.WriteByte(b[2]);
            fs.WriteByte(b[3]);
            fs.WriteByte(b[4]);
            fs.WriteByte(b[5]);
            fs.WriteByte(b[6]);
            fs.WriteByte(b[7]);
        }

        private void Write(FileStream fs, uint p)
        {
            fs.WriteByte((byte)p);
            fs.WriteByte((byte)(p >> 8));
            fs.WriteByte((byte)(p >> 16));
            fs.WriteByte((byte)(p >> 24));
        }

        private void Write(FileStream fs, ushort p)
        {
            fs.WriteByte((byte)p);
            fs.WriteByte((byte)(p >> 8));
        }

        private void Write(FileStream fs, char p)
        {
            fs.WriteByte((byte)p);
        }

        private void Write(FileStream fs, byte p)
        {
            fs.WriteByte(p);
        }
        #endregion

        public void ReadFile(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open);

            SpcHeader = new SpcHeader();

            SpcHeader.ftflags = (SpcFTFLGS)ReadByte(fs);
            SpcHeader.fversion = (SpcHeaderVersion)ReadByte(fs);
            SpcHeader.fexper = (SpcInstrumentTechnique)ReadByte(fs);
            SpcHeader.fexponent = ReadByte(fs);
            SpcHeader.fnpoints = ReadUint(fs);
            SpcHeader.ffirst = ReadDouble(fs);
            SpcHeader.flast = ReadDouble(fs);
            SpcHeader.fnsub = ReadUint(fs);
            SpcHeader.fxtype = (SpcAxisTypes)ReadByte(fs);
            SpcHeader.fytype = (SpcYAxisTypes)ReadByte(fs);
            SpcHeader.fztype = (SpcAxisTypes)ReadByte(fs);
            SpcHeader.fpost = ReadByte(fs);
            SpcHeader.fdate = ReadUint(fs);
            SpcHeader.fresolution = (char[])ReadArray(fs, 9, typeof(char));
            SpcHeader.fsource = (char[])ReadArray(fs, 9, typeof(char));
            SpcHeader.fpeakpoint = ReadUshort(fs);
            SpcHeader.fspare = (float[])ReadArray(fs, 8, typeof(float));
            SpcHeader.fcomment = (char[])ReadArray(fs, 130, typeof(char));
            SpcHeader.fcatxt = (char[])ReadArray(fs, 30, typeof(char));
            SpcHeader.flogoff = ReadUint(fs);
            SpcHeader.fmods = (SpcDataModFlags)ReadUint(fs);
            SpcHeader.fprocs = ReadByte(fs);
            SpcHeader.flevel = ReadByte(fs);
            SpcHeader.fsampin = ReadUshort(fs);
            SpcHeader.ffactor = ReadFloat(fs);
            SpcHeader.fmethod = (char[])ReadArray(fs, 48, typeof(char));
            SpcHeader.fzinc = ReadFloat(fs);
            SpcHeader.fwplanes = ReadUint(fs);
            SpcHeader.fwinc = ReadFloat(fs);
            SpcHeader.fwtype = (SpcAxisTypes)ReadByte(fs);
            SpcHeader.freserv = (byte[])ReadArray(fs, 187, typeof(byte));

            for (int idx = 0; idx < SpcHeader.fnsub; idx++)
            {
                SPCSubFile subFile = new SPCSubFile();

                subFile.SpcSubHeader = new SpcSubHeader();

                subFile.SpcSubHeader.subflgs = (SpcSubHeaderFlags)ReadByte(fs);
                subFile.SpcSubHeader.subexp = ReadByte(fs);
                subFile.SpcSubHeader.subindx = ReadUshort(fs);
                subFile.SpcSubHeader.subtime = ReadFloat(fs);
                subFile.SpcSubHeader.subnext = ReadFloat(fs);
                subFile.SpcSubHeader.subnois = ReadFloat(fs);
                subFile.SpcSubHeader.subnpts = ReadUint(fs);
                subFile.SpcSubHeader.subscan = ReadUint(fs);
                subFile.SpcSubHeader.subwlevel = ReadFloat(fs);
                subFile.SpcSubHeader.subresv = (byte[])ReadArray(fs, 4, typeof(byte));

                double[] x = new double[subFile.SpcSubHeader.subnpts];

                for (int jdx = 0; jdx < subFile.SpcSubHeader.subnpts; jdx++)
                {
                    x[jdx] = ReadFloat(fs);
                }

                double[] y = new double[subFile.SpcSubHeader.subnpts];

                for (int jdx = 0; jdx < subFile.SpcSubHeader.subnpts; jdx++)
                {
                    y[jdx] = ReadFloat(fs);
                }

                if (x.Length > 1)
                    subFile.Data = new DataSet(x, y);

                SubFiles.Add(subFile);
            }
            fs.Close();
        }

        private object ReadArray(FileStream fs, int size, Type type)
        {
            object res = null;
            if (type == typeof(char))
            {
                res = new char[size];
            }
            else if (type == typeof(float))
            {
                res = new float[size];
            }
            else if (type == typeof(byte))
            {
                res = new byte[size];
            }

            for (int idx = 0; idx < size; idx++)
            {
                if (type == typeof(char))
                {
                    ((char[])res)[idx] = (char)ReadByte(fs);
                }
                else if (type == typeof(float))
                {
                    ((float[])res)[idx] = ReadFloat(fs);
                }
                else if (type == typeof(byte))
                {
                    ((byte[])res)[idx] = ReadByte(fs);
                }
            }

            return res;
        }

        private byte ReadByte(FileStream fs)
        {
            return (byte)fs.ReadByte();
        }

        private uint ReadUint(FileStream fs)
        {
            uint res;

            res = (uint)fs.ReadByte();
            res += (((uint)fs.ReadByte()) << 8);
            res += (((uint)fs.ReadByte()) << 16);
            res += (((uint)fs.ReadByte()) << 24);

            return res;
        }

        private ushort ReadUshort(FileStream fs)
        {
            ushort res;

            res = (ushort)fs.ReadByte();
            res += (ushort)(((ushort)fs.ReadByte()) << 8);

            return res;
        }

        private unsafe float ReadFloat(FileStream fs)
        {
            float res;
            byte[] b = new byte[4];

            b[0] = (byte)fs.ReadByte();
            b[1] = (byte)fs.ReadByte();
            b[2] = (byte)fs.ReadByte();
            b[3] = (byte)fs.ReadByte();


            float* ptrFloat = &res;
            fixed (byte* ptrByte = b)
            {
                float* write = ptrFloat;
                write = (float*)ptrByte;
                res = *write;
            }

            return res;
        }

        private unsafe double ReadDouble(FileStream fs)
        {
            double res;
            byte[] b = new byte[8];

            b[0] = (byte)fs.ReadByte();
            b[1] = (byte)fs.ReadByte();
            b[2] = (byte)fs.ReadByte();
            b[3] = (byte)fs.ReadByte();
            b[4] = (byte)fs.ReadByte();
            b[5] = (byte)fs.ReadByte();
            b[6] = (byte)fs.ReadByte();
            b[7] = (byte)fs.ReadByte();

            double* ptrDouble = &res;
            fixed (byte* ptrByte = b)
            {
                double* write = ptrDouble;
                write = (double*)ptrByte;
                res = *write;
            }

            return res;
        }

        public SpcHeader SpcHeader       { get; set; }
        public List<SPCSubFile> SubFiles { get; set; }
    }

    public class SPCSubFile
    {
        public SpcSubHeader SpcSubHeader { get; set; }
        public DataSet Data { get; set; }
    }
}