//
//  LocationChooser.cs
//
// QuickStep: Acquire, view and identify spectra 
// Copyright (C) 2010-2017  Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

using Glade;

using Gtk;

using Mono.Unix;


namespace Hiperscan.Extensions.Acquisition
{

    public class LocationChooser
    {
        [Widget] Gtk.Window location_chooser_window = null;
        [Widget] Gtk.FileChooserButton filechooser_button = null;
        [Widget] Gtk.Entry filename_entry = null;
        [Widget] Gtk.Button cancel_button = null;
        [Widget] Gtk.Button ok_button     = null;
        
        
        public LocationChooser()
        {
            Glade.XML gui = new Glade.XML(null, "Acquisition.Acquisition.glade", "location_chooser_window", "Hiperscan");
            gui.Autoconnect(this);
            
            if (string.IsNullOrEmpty(Settings.Current.TargetDirectory) == false)
                this.filechooser_button.SetCurrentFolder(Settings.Current.TargetDirectory);
            else
                this.filechooser_button.SetCurrentFolder(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
                                      
            int ix = this.filename_entry.Text.LastIndexOf(".", StringComparison.InvariantCulture);
            this.location_chooser_window.Focus = this.filename_entry;
            
            if (ix > 0)
                this.filename_entry.SelectRegion(0, ix);
            
            this.location_chooser_window.AddAccelGroup(gui.EnsureAccel());

            this.cancel_button.AddAccelerator("clicked",
                                              gui.EnsureAccel(), 
                                              new AccelKey(Gdk.Key.Escape, Gdk.ModifierType.None, AccelFlags.Locked));
            
            this.ok_button.AddAccelerator("clicked",
                                          gui.EnsureAccel(), 
                                          new AccelKey(Gdk.Key.Return, Gdk.ModifierType.None, AccelFlags.Locked));
            
            this.location_chooser_window.ShowAll();
        }
        
        internal void OnCancelButtonClicked(object o, EventArgs e)
        {
            this.location_chooser_window.Destroy();
        }
        
        internal void OnOkButtonClicked(object o, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.filename_entry.Text))
                return;
                
            string fname = Path.Combine(this.filechooser_button.Filename, this.filename_entry.Text);
            
            if (Handlers.Running)
            {
                Handlers.AutoSaveFirstFilename = fname;
            }
            else
            {
                try
                {
                    Plugin.host.SaveAll(Plugin.host.CurrentSpectra, fname, true);
                }
                catch (Exception ex)
                {
                    Plugin.host.ShowInfoMessage(Plugin.host.MainWindow,
                                                Catalog.GetString("Cannot save spectra: {0}"), ex.Message);
                }
            }
            Settings.Current.TargetDirectory = this.filechooser_button.Filename;
            this.location_chooser_window.Destroy();
        }
    }
}