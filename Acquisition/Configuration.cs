// Configuration.cs created with MonoDevelop
// User: klose at 15:28 02.08.2010
// CVS release: $Id: Configuration.cs,v 1.2 2011-04-19 12:43:09 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//

using System.Configuration;


namespace Hiperscan.Extensions.Acquisition
{

    internal class PluginSettings : ConfigurationSection
    {
        
        public PluginSettings() {}
        
        [ConfigurationProperty("SectionExists")]
        public bool SectionExists
        {
            get { return (bool)this["SectionExists"]; }
            set { this["SectionExists"] = value;      }
        }
        
        [ConfigurationProperty("AcquisitionCount")]
        public int AcquisitionCount
        {
            get { return (int)this["AcquisitionCount"]; }
            set { this["AcquisitionCount"] = value;     }
        }
        
        [ConfigurationProperty("ReferenceAverage")]
        public ushort ReferenceAverage
        {
            get 
            { 
                if ((ushort)this["ReferenceAverage"] < 50)
                    return 2000;
                else
                    return (ushort)this["ReferenceAverage"]; 
            }
            set { this["ReferenceAverage"] = value; }
        }
        
        [ConfigurationProperty("AcquisitionAverage")]
        public ushort AcquisitionAverage
        {
            get 
            {
                if ((ushort)this["AcquisitionAverage"] < 50)
                    return 500;
                else
                    return (ushort)this["AcquisitionAverage"]; 
            }
            set { this["AcquisitionAverage"] = value; }
        }
        
        [ConfigurationProperty("TargetDirectory")]
        public string TargetDirectory
        {
            get { return (string)this["TargetDirectory"]; }
            set { this["TargetDirectory"] = value;        }
        }
        
        [ConfigurationProperty("ProbeSerial")]
        public string ProbeSerial
        {
            get { return (string)this["ProbeSerial"]; }
            set { this["ProbeSerial"] = value;        }
        }
        
        [ConfigurationProperty("WhiteReferenceSerial")]
        public string WhiteReferenceSerial
        {
            get { return (string)this["WhiteReferenceSerial"]; }
            set { this["WhiteReferenceSerial"] = value;        }
        }
        
        [ConfigurationProperty("BlackReferenceSerial")]
        public string BlackReferenceSerial
        {
            get { return (string)this["BlackReferenceSerial"]; }
            set { this["BlackReferenceSerial"] = value;        }
        }
    }

    internal static class Settings
    {
        public static void Init()
        {
            Plugin.host.InitConfigurationSection(Plugin.guid, new PluginSettings());
        }

        public static PluginSettings Current => Plugin.host.GetConfigurationSection(Plugin.guid) as PluginSettings;
    }
}