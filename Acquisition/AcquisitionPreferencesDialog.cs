//
//  AcquisitionPreferencesDialog.cs
//
// QuickStep: Acquire, view and identify spectra 
// Copyright (C) 2010-2017  Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using Glade;

using Gtk;


namespace Hiperscan.Extensions.Acquisition
{

    [Obsolete("This implementation was just a prototype for " +
              "Hiperscan.SGS.Benchtop.AsyncStabilizedFinder. " +
              "It is outdated und must be reimplemented!")]
    public class AcquisitionSettingsDialog
    {
        [Widget] Gtk.Dialog acquisition_settings = null;
        [Widget] Gtk.SpinButton reference_spin   = null;
        [Widget] Gtk.SpinButton acquisition_spin = null;
        [Widget] Gtk.Button ok_button2           = null;

        
        public AcquisitionSettingsDialog()
        {
            Glade.XML gui = new Glade.XML(null, "Acquisition.Acquisition.glade", "acquisition_settings", "Hiperscan");
            gui.Autoconnect(this);
            
            if (Settings.Current.ReferenceAverage > 0)
                this.reference_spin.Value = Settings.Current.ReferenceAverage;
            
            if (Settings.Current.AcquisitionAverage > 0)
                this.acquisition_spin.Value = Settings.Current.AcquisitionAverage;
            
            this.acquisition_settings.DeleteEvent += (o, args) =>
            {
                args.RetVal = true;
                this.OnOkButtonClicked(o, args);
            };
            
            this.ok_button2.CanDefault = true;
            this.acquisition_settings.Default = this.ok_button2;
            
            this.acquisition_spin.ActivatesDefault = true;
            this.reference_spin.ActivatesDefault   = true;
            
            this.acquisition_settings.ShowAll();
        }
        
        internal void OnOkButtonClicked(object o, EventArgs e)
        {
            Settings.Current.ReferenceAverage   = (ushort)this.reference_spin.Value;
            Settings.Current.AcquisitionAverage = (ushort)this.acquisition_spin.Value;
            this.acquisition_settings.Destroy();
        }
    }
}