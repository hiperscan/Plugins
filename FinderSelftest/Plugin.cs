//
//  Plugin.cs
//
// QuickStep: Acquire, view and identify spectra 
// Copyright (C) 2010-2017  Thomas Klose & Torsten Schache, Hiperscan GmbH Dresden, Germany
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Reflection;

using Gtk;

using Hiperscan.SGS;
using Hiperscan.Unix;


namespace Hiperscan.Extensions.FinderSelftest
{

    public class Plugin : Hiperscan.Extensions.IPlugin
    {
        public static readonly string name        = Catalog.GetString("Finder Selftest");
        public static readonly string version     = "1.0.0";
        public static readonly string vendor      = "Hiperscan GmbH";
        public static readonly string description = Catalog.GetString("Perform self tests for Finder hardware");
        public static readonly Guid guid          = new Guid("c9e31bbe-6ecb-4ac5-a934-54d61f25b4eb");

        public static Gdk.Pixbuf[] Icons;

        internal static IHost host;


        public Plugin()
        {
            string finder_selftest_logo_fname = "FinderSelftest.Resources.checkmark128x128.png";
            Plugin.Icons = new Gdk.Pixbuf[5];
            
            Plugin.Icons[0] = new Gdk.Pixbuf(null, finder_selftest_logo_fname, 16, 16);
            Plugin.Icons[1] = new Gdk.Pixbuf(null, finder_selftest_logo_fname, 32, 32);
            Plugin.Icons[2] = new Gdk.Pixbuf(null, finder_selftest_logo_fname, 48, 48);
            Plugin.Icons[3] = new Gdk.Pixbuf(null, finder_selftest_logo_fname, 64, 64);
            Plugin.Icons[4] = new Gdk.Pixbuf(null, finder_selftest_logo_fname, 128, 128);

            IconSet icon_set = new IconSet();
            foreach (Gdk.Pixbuf pixbuf in Plugin.Icons)
            {
                IconSource icon_source = new IconSource
                {
                    Pixbuf = pixbuf
                };
                icon_set.AddSource(icon_source);
            }

            IconFactory icon_factory = new IconFactory();
            icon_factory.Add("hiperscan.finder_selftest", icon_set);
            icon_factory.AddDefault();

            MenuInfo menu_info1 = new MenuInfo(Catalog.GetString("Start Finder Selftest ..."))
            {
                WatchCursor = true,
                NeedIdleDevice = true,
                NeedSingleDevice = true
            };
            menu_info1.Invoked += (sender, e) =>
            {
                foreach (IDevice dev in Plugin.host.ActiveDevices.Idle)
                {
                    if (dev.IsFinder)
                        FinderSelftest.Start(dev);
                }
            };

            this.MenuInfos = new MenuInfo[] {menu_info1};

            ToolButton b = new ToolButton("hiperscan.finder_selftest");
            ToolbarInfo toolbar_info1 = new ToolbarInfo(b, Catalog.GetString("Test"), Catalog.GetString("Start Finder Selftest"))
            {
                WatchCursor = true,
                NeedIdleDevice = true,
                NeedSingleDevice = true
            };
            toolbar_info1.Invoked += (sender, e) =>
            {
                foreach (IDevice dev in Plugin.host.ActiveDevices.Idle)
                {
                    if (dev.IsFinder)
                        FinderSelftest.Start(dev);
                }
            };

            this.ToolbarInfos = new ToolbarInfo[] { toolbar_info1 };
        }
        
        public void ReplaceGui()
        {
        }

        public void Initialize(IHost host)
        {
            Plugin.host = host;
        }
        
        public string Name
        {
            get { return Plugin.name; }
        }

        public MenuInfo[]    MenuInfos    { get; }
        public ToolbarInfo[] ToolbarInfos { get; }

        public bool Active { get; set; } = true;

        public PlotTypeInfo[] PlotTypeInfos => null;
        public FileTypeInfo[] FileTypeInfos => null;
        public string Version               => Plugin.version;
        public string Vendor                => Plugin.vendor;
        public Assembly Assembly            => Assembly.GetExecutingAssembly();
        public string Description           => Plugin.description;
        public Guid Guid                    => Plugin.guid;
        public bool CanReplaceGui           => false;
    }
}