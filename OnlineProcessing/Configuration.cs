// Configuration.cs created with MonoDevelop
// User: klose at 15:28 02.08.2010
// CVS release: $Id: Configuration.cs,v 1.2 2011-04-19 12:43:09 klose Exp $
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2010  Thomas Klose, Hiperscan GmbH Dresden, Germany
//

using System.Configuration;


namespace Hiperscan.Extensions.OnlineProcessing
{

    internal class PluginSettings : System.Configuration.ConfigurationSection
    {

        public PluginSettings() {}
        
        [ConfigurationProperty("SectionExists")]
        public bool SectionExists
        {
            get { return (bool)this["SectionExists"]; }
            set { this["SectionExists"] = value;      }
        }
        
        [ConfigurationProperty("AcquisitionCount")]
        public int AcquisitionCount
        {
            get { return (int)this["AcquisitionCount"]; }
            set { this["AcquisitionCount"] = value;     }
        }
    }

    internal static class Settings
    {
        public static void Init()
        {
            Plugin.host.InitConfigurationSection(Plugin.guid, new PluginSettings());
        }

        public static PluginSettings Current => Plugin.host.GetConfigurationSection(Plugin.guid) as PluginSettings;
    }
}